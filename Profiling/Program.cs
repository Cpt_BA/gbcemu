﻿using GBCZ80;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Profiling
{
    class Program
    {
        static void Main(string[] args)
        {
            long runCount = 100000000;
            Stopwatch timer = new Stopwatch();
            Z80CPU cpu = new Z80CPU();
            cpu.SetupBlankCart();

            timer.Restart();
            for (long i = 0; i < runCount; i++)
            {
                cpu.ram.ReadOverheadTest((byte)(i % 0xFF));
            }
            timer.Stop();
            PrintTimes("Read Overhead Test", timer, runCount);

            timer.Restart();
            for (long i = 0; i < runCount; i++)
            {
                cpu.ram.WriteOverheadTest((byte)(i % 0xFF), 0xAA);
            }
            timer.Stop();
            PrintTimes("Write Overhead Test", timer, runCount);

            timer.Restart();
            for (long i = 0; i < runCount; i++)
            {
                cpu.ram.ReadByte((byte)(i % 0xFF));
            }
            timer.Stop();
            PrintTimes("Read Speed", timer, runCount);

            timer.Restart();
            for (long i = 0; i < runCount; i++)
            {
                cpu.ram.WriteByte((byte)(i % 0xFF), 0xAA);
            }
            timer.Stop();
            PrintTimes("Write Speed", timer, runCount);


            Console.ReadLine();
        }

        static void PrintTimes(string title, Stopwatch timer, long runCount)
        {
            Console.WriteLine("Results for {0}", title);
            Console.WriteLine("{0:#,##0} Cycles int {1}ms, {2:#,##0}/second", runCount, timer.ElapsedMilliseconds, (runCount / timer.ElapsedMilliseconds) * 1000);
        }
    }
}
