﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBCZ80.Decoding
{
    partial class Decoder
    {
        private static byte[] BIT_N_D_COSTS = new byte[] { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 };

        static void RegisterCB()
        {
            Console.WriteLine("-----------Registering CB Opcodes--------------");
            RegisterCBOpcode("RdC D", RDC_D, new byte[] { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 }, "0000", OpcodeTypes.Direction, OpcodeTypes.Destination);
            RegisterCBOpcode("Rd D", RD_D, new byte[] { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 }, "0001", OpcodeTypes.Direction, OpcodeTypes.Destination);
            RegisterCBOpcode("SdA D", SDA_D, new byte[] { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 }, "0010", OpcodeTypes.Direction, OpcodeTypes.Destination);
            RegisterCBOpcode("BIT N,D", BIT_N_D, BIT_N_D_COSTS, "01", OpcodeTypes.Bit, OpcodeTypes.Destination);
            RegisterCBOpcode("RES N,D", RES_N_D, BIT_N_D_COSTS, "10", OpcodeTypes.Bit, OpcodeTypes.Destination);
            RegisterCBOpcode("SET N,D", SET_N_D, BIT_N_D_COSTS, "11", OpcodeTypes.Bit, OpcodeTypes.Destination);
            RegisterCBOpcode("SRL n", SRL_N, new byte[] { 4, 4, 4, 4, 4, 4, 4, 4 }, "00111", OpcodeTypes.Destination);
            RegisterCBOpcode("SWAP n", SWAP_N, new byte[] { 4, 4, 4, 4, 4, 4, 4, 4 }, "00110", OpcodeTypes.Destination);
        }

        private static void RDC_D(int paramValue, params object[] parameters)
        {
            Opcode.Destination dest = (Opcode.Destination)parameters[1];
            byte src = GetRegister8Bit(dest);
            switch ((Opcode.Direction)parameters[0])
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
        }

        private static void RD_D(int paramValue, params object[] parameters)
        {
            Opcode.Destination dest = (Opcode.Destination)parameters[1];
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch ((Opcode.Direction)parameters[0])
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
        }

        private static void SDA_D(int paramValue, params object[] parameters)
        {
            Opcode.Destination dest = (Opcode.Destination)parameters[1];
            byte src = GetRegister8Bit(dest);
            switch ((Opcode.Direction)parameters[0])
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
        }

        private static void BIT_N_D(int paramValue, params object[] parameters)
        {
            Opcode.Destination dest = (Opcode.Destination)parameters[1];
            Opcode.RegisterBit bit = (Opcode.RegisterBit)parameters[0];
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
        }

        private static void RES_N_D(int paramValue, params object[] parameters)
        {
            Opcode.Destination dest = (Opcode.Destination)parameters[1];
            Opcode.RegisterBit bit = (Opcode.RegisterBit)parameters[0];
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
        }

        private static void SET_N_D(int paramValue, params object[] parameters)
        {
            Opcode.Destination dest = (Opcode.Destination)parameters[1];
            Opcode.RegisterBit bit = (Opcode.RegisterBit)parameters[0];
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
        }

        private static void SRL_N(int paramValue, params object[] parameters)
        {
            Opcode.Destination dest = (Opcode.Destination)parameters[0];
            byte val = GetRegister8Bit(dest);
            cpu.state.Zero = (val >> 1) == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = (val & 0x01) == 1;
            val >>= 1;
            SetRegister8Bit(dest, val);
        }

        private static void SWAP_N(int paramValue, params object[] parameters)
        {
            Opcode.Destination dest = (Opcode.Destination)parameters[0];
            byte val = GetRegister8Bit(dest);
            val = (byte)(((val & 0xF0) >> 4) | ((val & 0x0F) << 4));

            cpu.state.Zero = val == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = false;
            
            SetRegister8Bit(dest, val);
        }
    }
}
