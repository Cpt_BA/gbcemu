﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GBCZ80;

namespace GBCZ80.Decoding
{
    public static class DecodeTester
    {
        public static void Test(Z80CPU cpu)
        {
            Z80CPU.ProcessorState state = new Z80CPU.ProcessorState();
            state.A = 0x01;
            state.BC = 0x0203;
            state.DE = 0x0405;
            state.HL = 0x0607;
            state.PC = 0x0001;
            state.SP = 0xFF7F;

            ExecuteCode(cpu, state, 0x06, 0xFF); Assert(cpu.state.B, (byte)0xFF);
            ExecuteCode(cpu, state, 0x0E, 0xFF); Assert(cpu.state.C, (byte)0xFF);
            ExecuteCode(cpu, state, 0x16, 0xFF); Assert(cpu.state.D, (byte)0xFF);
            ExecuteCode(cpu, state, 0x1E, 0xFF); Assert(cpu.state.E, (byte)0xFF);
            ExecuteCode(cpu, state, 0x26, 0xFF); Assert(cpu.state.H, (byte)0xFF);
            ExecuteCode(cpu, state, 0x2E, 0xFF); Assert(cpu.state.L, (byte)0xFF);

            cpu.ram[0x0607] = 0xAA;
            cpu.ram[0xFF80] = 0xA0;
            cpu.ram[0xFF81] = 0xA0;

            ExecuteCode(cpu, state, 0x7F); Assert(cpu.state.A, cpu.state.A);
            ExecuteCode(cpu, state, 0x78); Assert(cpu.state.A, cpu.state.B);
            ExecuteCode(cpu, state, 0x79); Assert(cpu.state.A, cpu.state.C);
            ExecuteCode(cpu, state, 0x7A); Assert(cpu.state.A, cpu.state.D);
            ExecuteCode(cpu, state, 0x7B); Assert(cpu.state.A, cpu.state.E);
            ExecuteCode(cpu, state, 0x7C); Assert(cpu.state.A, cpu.state.H);
            ExecuteCode(cpu, state, 0x7D); Assert(cpu.state.A, cpu.state.L);
            ExecuteCode(cpu, state, 0x7E); Assert(cpu.state.A, (byte)0xAA);

            ExecuteCode(cpu, state, 0x40); Assert(cpu.state.B, cpu.state.B);
            ExecuteCode(cpu, state, 0x41); Assert(cpu.state.B, cpu.state.C);
            ExecuteCode(cpu, state, 0x42); Assert(cpu.state.B, cpu.state.D);
            ExecuteCode(cpu, state, 0x43); Assert(cpu.state.B, cpu.state.E);
            ExecuteCode(cpu, state, 0x44); Assert(cpu.state.B, cpu.state.H);
            ExecuteCode(cpu, state, 0x45); Assert(cpu.state.B, cpu.state.L);
            ExecuteCode(cpu, state, 0x46); Assert(cpu.state.B, (byte)0xAA);

            ExecuteCode(cpu, state, 0x48); Assert(cpu.state.C, cpu.state.B);
            ExecuteCode(cpu, state, 0x49); Assert(cpu.state.C, cpu.state.C);
            ExecuteCode(cpu, state, 0x4A); Assert(cpu.state.C, cpu.state.D);
            ExecuteCode(cpu, state, 0x4B); Assert(cpu.state.C, cpu.state.E);
            ExecuteCode(cpu, state, 0x4C); Assert(cpu.state.C, cpu.state.H);
            ExecuteCode(cpu, state, 0x4D); Assert(cpu.state.C, cpu.state.L);
            ExecuteCode(cpu, state, 0x4E); Assert(cpu.state.C, (byte)0xAA);

            cpu.ram[0x1040] = 0xAA;

            ExecuteCode(cpu, state, 0x36, 0xAA); Assert(cpu.ram[state.HL], (byte)0xAA);
            ExecuteCode(cpu, state, 0xFA, 0x40, 0x10); Assert(cpu.state.A, cpu.ram[0x1040]);
            ExecuteCode(cpu, state, 0x3E, 0x3E); Assert(cpu.state.A, (byte)0x3E);
            ExecuteCode(cpu, state, 0x7E); Assert(cpu.state.A, cpu.ram[state.HL]);

            ExecuteCode(cpu, state, 0x3A); Assert(cpu.state.A, (byte)0xAA); Assert(cpu.state.HL, state.HL - 1);
            ExecuteCode(cpu, state, 0x2A); Assert(cpu.state.A, (byte)0xAA); Assert(cpu.state.HL, state.HL + 1);

            ExecuteCode(cpu, state, 0x32); Assert(cpu.ram[state.HL], state.A); Assert(cpu.state.HL, state.HL - 1);
            ExecuteCode(cpu, state, 0x22); Assert(cpu.ram[state.HL], state.A); Assert(cpu.state.HL, state.HL + 1);

            ExecuteCode(cpu, state, 0xE0, 0x80); Assert(cpu.ram[0xFF80], state.A);
            ExecuteCode(cpu, state, 0xF0, 0x81); Assert(cpu.state.A, (byte)0xA0);

            ExecuteCode(cpu, state, 0x01, 0xAA, 0xBB); Assert(cpu.state.BC, 0xBBAA);
            ExecuteCode(cpu, state, 0x11, 0xAA, 0xBB); Assert(cpu.state.DE, 0xBBAA);
            ExecuteCode(cpu, state, 0x21, 0xAA, 0xBB); Assert(cpu.state.HL, 0xBBAA);
            ExecuteCode(cpu, state, 0x31, 0xAA, 0xBB); Assert(cpu.state.SP, 0xBBAA);

            ExecuteCode(cpu, state, 0xF9); Assert(cpu.state.SP, cpu.state.HL);

            ExecuteCode(cpu, state, 0xF8, 0x10); Assert(cpu.state.HL, cpu.state.SP + 0x10); AssertFlags(cpu.state, false, false, null, null);

            ExecuteCode(cpu, state, 0x08, 0x00, 0xE0); Assert(cpu.ram[0xE001] << 8 | cpu.ram[0xE000], cpu.state.SP);

            ExecuteCode(cpu, state, 0xF5); Assert(cpu.ram[state.SP - 1], state.A); Assert(cpu.ram[state.SP - 2], state.F); Assert(cpu.state.SP, state.SP - 2);
            ExecuteCode(cpu, state, 0xC5); Assert(cpu.ram[state.SP - 1], state.B); Assert(cpu.ram[state.SP - 2], state.C); Assert(cpu.state.SP, state.SP - 2);
            ExecuteCode(cpu, state, 0xD5); Assert(cpu.ram[state.SP - 1], state.D); Assert(cpu.ram[state.SP - 2], state.E); Assert(cpu.state.SP, state.SP - 2);
            ExecuteCode(cpu, state, 0xE5); Assert(cpu.ram[state.SP - 1], state.H); Assert(cpu.ram[state.SP - 2], state.L); Assert(cpu.state.SP, state.SP - 2);

            cpu.ram[state.SP] = 0x02;
            cpu.ram[state.SP + 1] = 0x03;

            ExecuteCode(cpu, state, 0xF1); Assert(cpu.state.AF, 0x0302);
            ExecuteCode(cpu, state, 0xC1); Assert(cpu.state.BC, 0x0302);
            ExecuteCode(cpu, state, 0xD1); Assert(cpu.state.DE, 0x0302);
            ExecuteCode(cpu, state, 0xE1); Assert(cpu.state.HL, 0x0302);

            //ExecuteCode(cpu, state, 0xC6, 0xFF); Assert(cpu.state.A, (byte)0x00); AssertFlags(cpu.state, true, false, true, true);
            ExecuteCode(cpu, state, 0xC6, 0x0F); Assert(cpu.state.A, (byte)0x10); AssertFlags(cpu.state, false, false, true, false);

            ExecuteCode(cpu, state, 0x09); Assert(cpu.state.HL, (ushort)(state.HL + state.BC)); AssertFlags(cpu.state, null, false, false, false);
            ExecuteCode(cpu, state, 0x19); Assert(cpu.state.HL, (ushort)(state.HL + state.DE));
            ExecuteCode(cpu, state, 0x29); Assert(cpu.state.HL, (ushort)(state.HL + state.HL));
            ExecuteCode(cpu, state, 0x39); Assert(cpu.state.HL, (ushort)(state.HL + state.SP));

            ExecuteCode(cpu, state, 0xE8, 0x10); Assert(cpu.state.SP, (ushort)(state.SP + 0x10)); AssertFlags(cpu.state, false, false, null, null);

            ExecuteCode(cpu, state, 0x03); Assert(cpu.state.BC, state.BC + 1);
            ExecuteCode(cpu, state, 0x13); Assert(cpu.state.DE, state.DE + 1);
            ExecuteCode(cpu, state, 0x23); Assert(cpu.state.HL, state.HL + 1);
            ExecuteCode(cpu, state, 0x33); Assert(cpu.state.SP, state.SP + 1);

            ExecuteCode(cpu, state, 0x0B); Assert(cpu.state.BC, state.BC - 1);
            ExecuteCode(cpu, state, 0x1B); Assert(cpu.state.DE, state.DE - 1);
            ExecuteCode(cpu, state, 0x2B); Assert(cpu.state.HL, state.HL - 1);
            ExecuteCode(cpu, state, 0x3B); Assert(cpu.state.SP, state.SP - 1);

            cpu.ram[state.HL] = 0x01;

            ExecuteCode(cpu, state, 0xCB, 0x37); Assert(cpu.state.A, (byte)(((state.A & 0x0F) << 4) | ((state.A & 0xF0) >> 4))); AssertFlags(cpu.state, false, false, false, false);
            ExecuteCode(cpu, state, 0xCB, 0x36); Assert(cpu.ram[state.HL], (byte)0x10); AssertFlags(cpu.state, false, false, false, false);

            ExecuteCode(cpu, state, 0x2F); Assert(cpu.state.A, (byte)~state.A); AssertFlags(cpu.state, null, true, true, null);

            ExecuteCode(cpu, state, 0x37); AssertFlags(cpu.state, null, false, false, true);

            ExecuteCode(cpu, state, 0xC3, 0x40, 0x10); Assert(cpu.state.PC, (ushort)0x1040);

            ExecuteCode(cpu, state, 0xC2, 0x40, 0x10); Assert(cpu.state.PC, (ushort)0x1040);
            ExecuteCode(cpu, state, 0xCA, 0x40, 0x10); Assert(cpu.state.PC, (ushort)state.PC + 0x03);
            ExecuteCode(cpu, state, 0xD2, 0x40, 0x10); Assert(cpu.state.PC, (ushort)0x1040);
            ExecuteCode(cpu, state, 0xDA, 0x40, 0x10); Assert(cpu.state.PC, (ushort)state.PC + 0x03);

            ExecuteCode(cpu, state, 0xE9); Assert(cpu.state.PC, state.HL);

            ExecuteCode(cpu, state, 0x18, 0x10); Assert(cpu.state.PC, state.PC + 2 + 0x10);

            ExecuteCode(cpu, state, 0x20, 0x10); Assert(cpu.state.PC, state.PC + 2 + 0x10);
            ExecuteCode(cpu, state, 0x28, 0x10); Assert(cpu.state.PC, state.PC + 2);
            ExecuteCode(cpu, state, 0x30, 0x10); Assert(cpu.state.PC, state.PC + 2 + 0x10);
            ExecuteCode(cpu, state, 0x38, 0x10); Assert(cpu.state.PC, state.PC + 2);

            ExecuteCode(cpu, state, 0xCD, 0x10, 0x40); Assert(cpu.state.PC, (ushort)0x4010); Assert(cpu.state.SP, state.SP - 2);

            ExecuteCode(cpu, state, 0xC4, 0x10, 0x40); Assert(cpu.state.PC, (ushort)0x4010); Assert(cpu.state.SP, state.SP - 2);
            ExecuteCode(cpu, state, 0xCC, 0x10, 0x40); Assert(cpu.state.PC, state.PC + 0x03); Assert(cpu.state.SP, state.SP);
            ExecuteCode(cpu, state, 0xD4, 0x10, 0x40); Assert(cpu.state.PC, (ushort)0x4010); Assert(cpu.state.SP, state.SP - 2);
            ExecuteCode(cpu, state, 0xDC, 0x10, 0x40); Assert(cpu.state.PC, state.PC + 0x03); Assert(cpu.state.SP, state.SP);

            Decoder.OpcodeCounts = new int[256];
            Decoder.CBOpcodeCounts = new int[256];
        }

        static void ExecuteCode(Z80CPU cpu, Z80CPU.ProcessorState restore_state, params byte[] data)
        {
            cpu.state = restore_state;
            for (int i = 0; i < data.Length; i++)
                cpu.ram[cpu.state.PC + i] = data[i];
            cpu.RunSingle();
        }

        static void Assert(byte value, byte expected)
        {
            if (value != expected)
                throw new Exception(String.Format("Failed Assert: 0x{0:X} expected, 0x{1:X} actual", expected, value));
        }

        static void Assert(short value, short expected)
        {
            if (value != expected)
                throw new Exception(String.Format("Failed Assert: 0x{0:X} expected, 0x{1:X} actual", expected, value));
        }

        static void Assert(int value, int expected)
        {
            if (value != expected)
                throw new Exception(String.Format("Failed Assert: 0x{0:X} expected, 0x{1:X} actual", expected, value));
        }

        static void AssertFlags(Z80CPU.ProcessorState state, bool? Z, bool? N, bool? H, bool? C)
        {
            if (Z != null && state.Zero != Z)
                throw new Exception(String.Format("Failed Flag Assert, Z flag mismatch"));
            if (N != null && state.Op != N)
                throw new Exception(String.Format("Failed Flag Assert, N flag mismatch"));
            if (H != null && state.HalCar != H)
                throw new Exception(String.Format("Failed Flag Assert, H flag mismatch"));
            if (C != null && state.Carry != C)
                throw new Exception(String.Format("Failed Flag Assert, C flag mismatch"));
        }
    }
}
