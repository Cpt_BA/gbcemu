﻿#undef INCLUDE_HISTORY

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.CompilerServices;

namespace GBCZ80.Decoding
{
    public partial class Decoder
    {
        public class OCHistory
        {
            public byte opcode;
            public ushort param;
            public ushort location;
            public bool CB;

            public override string ToString()
            {
                if(CB)
                    return String.Format("0x{0:X4} -> 0x{1:X2} [{2}] (0x{3:X2}) [CB]", location, opcode, Decoder.CBOpcodes[opcode].name, param);
                else
                    return String.Format("0x{0:X4} -> 0x{1:X2} [{2}] (0x{3:X2})", location, opcode, Decoder.Opcodes[opcode].name, param);
            }
        }

        public static Opcode[] Opcodes = new Opcode[256];
        public static Opcode[] CBOpcodes = new Opcode[256];
        public static Queue<OCHistory> history = new Queue<OCHistory>();

        public static int[] OpcodeCounts = new int[256];
        public static int[] CBOpcodeCounts = new int[256];

        public static bool enableOutput = false;

        private Decoder() { }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int Execute(byte opcode, Z80CPU cpu)
        {
            //return ExecuteSwitch(opcode);

            if(cpu.state.PC - 1 == 0x0155)
            {

            }

            Opcode oc;
#if INCLUDE_HISTORY
            OCHistory hist = new OCHistory();
#endif
            if (opcode == 0xCB)
            {
                opcode = cpu.ram.ReadByte(cpu.state.PC++);
                oc = CBOpcodes[opcode];
#if INCLUDE_HISTORY
                hist.CB = true;
                hist.opcode = opcode;
#endif
                CBOpcodeCounts[opcode]++;
                if (oc == null) throw new Exception(String.Format("CB Opcode {0:X}/{0} is not defined", opcode));
            }
            else
            {
                oc = Opcodes[opcode];
                if (oc == null) throw new Exception(String.Format("Opcode {0:X}/{0} is not defined", opcode));
            }
            OpcodeCounts[opcode]++;
#if INCLUDE_HISTORY
            hist.opcode = opcode;
            hist.location = (ushort)(cpu.state.PC - 1);
#endif
            //cpu.Cycles += oc.cost;
#if INCLUDE_HISTORY
            if (history.Count == 10000)
            {
                history.Dequeue();
            }
#endif
            switch (oc.opcodeParam)
            {
                case Opcode.Params._NONE:
#if INCLUDE_HISTORY
                    if (enableOutput) Console.WriteLine("{0}", hist.ToString());
#endif
                    oc.Execute(); break;
                case Opcode.Params._8_BIT:
                    cpu.state.PC++;
                    int param_8 = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
#if INCLUDE_HISTORY
                    hist.param = (ushort)param_8;
                    if (enableOutput) Console.WriteLine("{0}", hist.ToString());
#endif
                    oc.Execute(param_8); break; //Read 1 byte ahead in RAM and pass to function break;
                case Opcode.Params._16_BIT:
                    cpu.state.PC += 2;
                    int param_16 = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
#if INCLUDE_HISTORY
                    hist.param = (ushort)param_16;
                    if (enableOutput) Console.WriteLine("{0}", hist.ToString());
#endif
                    oc.Execute(param_16); break; //Read 2 bytes ahead in RAM and pass to function
            }
#if INCLUDE_HISTORY
            history.Enqueue(hist);
#endif

            //cpu.soundSystem.SoundUpdate();

            return oc.cost;
        }

        private static void test(int opCode)
        {
            
        }

        private static void RegisterCBOpcode(string debug_name, Opcode.OpcodeDelegate functionToCall, byte[] costs, params object[] opcode_parts)
        {
            Register(CBOpcodes, true, debug_name, functionToCall, costs, opcode_parts);
        }

        private static void RegisterOpcode(string debug_name, Opcode.OpcodeDelegate functionToCall, byte[] costs, params object[] opcode_parts)
        {
            Register(Opcodes, false, debug_name, functionToCall, costs, opcode_parts);
        }

        private static void Register(Opcode[] toArray, bool isCB, string debug_name, Opcode.OpcodeDelegate functionToCall, byte[] costs, params object[] opcode_parts)
        {
            string opcode_map = "";
            Dictionary<int, OpcodeTypes> variableLocations = new Dictionary<int, OpcodeTypes>();
            Opcode.Params parameter_type = Opcode.Params._NONE;
            foreach (object o in opcode_parts)
            {
                if (o is string)
                    opcode_map += o.ToString();
                else if (o is OpcodeTypes)
                {
                    variableLocations.Add(opcode_map.Length, (OpcodeTypes)o);
                    opcode_map += ("".PadRight((o as OpcodeTypes).value, 'x'));
                }
                else if (o is Opcode.Params)
                {
                    parameter_type = (Opcode.Params)o;
                }
            }
            string base_opcode = opcode_map.Substring(0, 8);
            int vars = base_opcode.Count((c) => c == 'x');
            int amount = (int)Math.Pow(2, vars);
            for (int value = 0; value < amount; value++)
            {
                string current = base_opcode;
                for (int i = 0; i < vars; i++)
                {
                    int index = current.LastIndexOf('x');
                    current = current.Remove(index, 1).Insert(index, ((value & (1 << i)) != 0) ? "1" : "0");
                }
                int opcode = Convert.ToInt16(current, 2);
                List<object> Params = new List<object>();
                foreach (KeyValuePair<int, OpcodeTypes> pair in variableLocations)
                {
                    Params.Add(Opcode.GetOpcodeType(pair.Value));
                    Params.Add(Convert.ToInt16(current.Substring(pair.Key, pair.Value.value), 2));
                }
                if(toArray[opcode] != null)
                {
                    throw new Exception(String.Format("Overwriting opcode 0x{0:X2} ({1}) With {2}", opcode, toArray[opcode].name, debug_name));
                }
                toArray[opcode] = new Opcode(opcode, isCB, debug_name, functionToCall, parameter_type, costs[value], Params.ToArray());

            }
            //Console.WriteLine("{0} Opcodes added, {1}/256 Opcodes Registered", amount, toArray.Count((x) => x != null));
        }

        public static void SetRegister8Bit(Opcode.Destination reg, byte value)
        {
            switch (reg)
            {
                case Opcode.Destination.A: cpu.state.A = value; break;
                case Opcode.Destination.B: cpu.state.B = value; break;
                case Opcode.Destination.C: cpu.state.C = value; break;
                case Opcode.Destination.D: cpu.state.D = value; break;
                case Opcode.Destination.E: cpu.state.E = value; break;
                case Opcode.Destination.H: cpu.state.H = value; break;
                case Opcode.Destination.L: cpu.state.L = value; break;
                case Opcode.Destination.HL: cpu.ram.WriteByte(cpu.state.HL, value); break;
            }
        }

        public static byte GetRegister8Bit(Opcode.Destination reg)
        {
            switch (reg)
            {
                case Opcode.Destination.A: return cpu.state.A;
                case Opcode.Destination.B: return cpu.state.B;
                case Opcode.Destination.C: return cpu.state.C;
                case Opcode.Destination.D: return cpu.state.D;
                case Opcode.Destination.E: return cpu.state.E;
                case Opcode.Destination.H: return cpu.state.H;
                case Opcode.Destination.L: return cpu.state.L;
                case Opcode.Destination.HL: return cpu.ram.ReadByte(cpu.state.HL);
            }
            return 0;
        }

        public static void SetRegister16BitSP(Opcode.RegisterSP reg, ushort value)
        {
            switch (reg)
            {
                case Opcode.RegisterSP.BC: cpu.state.BC = value; break;
                case Opcode.RegisterSP.DE: cpu.state.DE = value; break;
                case Opcode.RegisterSP.HL: cpu.state.HL = value; break;
                case Opcode.RegisterSP.SP: cpu.state.SP = value; break;
            }
        }

        public static void SetRegister16BitAF(Opcode.RegisterAF reg, ushort value)
        {
            switch (reg)
            {
                case Opcode.RegisterAF.BC: cpu.state.BC = value; break;
                case Opcode.RegisterAF.DE: cpu.state.DE = value; break;
                case Opcode.RegisterAF.HL: cpu.state.HL = value; break;
                case Opcode.RegisterAF.AF: cpu.state.AF = value; break;
            }
        }

        public static ushort GetRegister16BitSP(Opcode.RegisterSP reg)
        {
            switch (reg)
            {
                case Opcode.RegisterSP.BC: return cpu.state.BC;
                case Opcode.RegisterSP.DE: return cpu.state.DE;
                case Opcode.RegisterSP.HL: return cpu.state.HL;
                case Opcode.RegisterSP.SP: return cpu.state.SP;
            }
            return 0;
        }

        public static ushort GetRegister16BitAF(Opcode.RegisterAF reg)
        {
            switch (reg)
            {
                case Opcode.RegisterAF.BC: return cpu.state.BC;
                case Opcode.RegisterAF.DE: return cpu.state.DE;
                case Opcode.RegisterAF.HL: return cpu.state.HL;
                case Opcode.RegisterAF.AF: return cpu.state.AF;
            }
            return 0;
        }

        public static ushort GetRegister16BitSing(Opcode.RegisterSingle reg)
        {
            switch (reg)
            {
                case Opcode.RegisterSingle.BC: return cpu.state.BC;
                case Opcode.RegisterSingle.DE: return cpu.state.DE;
            }
            return 0;
        }
        
        public static byte ALU_ADD(byte n)
        {
            byte a = cpu.state.A;
            int result = 0;

            result = (a + n);

            cpu.state.Op = false;
            cpu.state.Zero = (result & 0xFF) == 0;
            cpu.state.HalCar = ((a & 0x0F) + (n & 0x0F)) > 0x0F;
            cpu.state.Carry = (result > 0xFF);

            return (byte)(result & 0xFF);
        }

        public static byte ALU_ADC(byte n)
        {
            return ALU_ADD((byte)(n + (cpu.state.Carry ? 1 : 0)));
        }

        public static byte ALU8Bit(Opcode.Operation op, byte n)
        {
            switch (op)
            {
                //case Opcode.Operation.ADD: return ALU_ADD(n);
                case Opcode.Operation.ADC: return ALU_ADC(n);
            }

            byte a = cpu.state.A;

            int result = 0;
            switch (op)
            {
                case Opcode.Operation.ADD: result = (a + n); break;
                case Opcode.Operation.ADC: result = (a + (cpu.state.Carry ? 1 : 0) + n); break;
                case Opcode.Operation.SUB: result = (a - n); break;
                case Opcode.Operation.SBC: result = (a - (n + (cpu.state.Carry ? 1 : 0))); break;
                case Opcode.Operation.AND: result = (a & n); break;
                case Opcode.Operation.XOR: result = (a ^ n); break;
                case Opcode.Operation.OR: result = (a | n); break;
                case Opcode.Operation.CP: result = (a - n); break;
            }

            result &= 0x1FF;

            if (op == Opcode.Operation.ADD)
            {
                cpu.state.Op = false;
                cpu.state.HalCar = (a & 0x0F) > (result & 0x0F);
                cpu.state.Carry = result > 0xFF;
            }
            if (op == Opcode.Operation.ADC)
            {
                cpu.state.Op = false;
                cpu.state.HalCar = (((cpu.state.Carry ? 1 : 0) + (n & 0x0F) + (a & 0x0F)) > 0x0F);
                cpu.state.Carry = result > 0xFF;
            }
            if (op == Opcode.Operation.SUB)
            {
                cpu.state.Op = true;
                cpu.state.HalCar = !((a & 0x0F) < (result & 0x0F));
                cpu.state.Carry = !(a < result);
            }
            if (op == Opcode.Operation.SBC)
            {
                cpu.state.Op = true;
                cpu.state.HalCar = !((a & 0x0F) < (result & 0x0F));
                cpu.state.Carry = !(a < result);
            }
            if (op == Opcode.Operation.AND)
            {
                cpu.state.Op = false;
                cpu.state.HalCar = true;
                cpu.state.Carry = false;
            }
            if (op == Opcode.Operation.OR || op == Opcode.Operation.XOR)
            {
                cpu.state.Op = false;
                cpu.state.HalCar = false;
                cpu.state.Carry = false;
            }

            cpu.state.Zero = (result & 0xFF) == 0;

            if (op == Opcode.Operation.CP)
            {
                cpu.state.Op = true;
                cpu.state.HalCar = !((a & 0x0F) < (n & 0x0F));
                cpu.state.Carry = !(a < n);
                result = cpu.state.A;
            }
            return (byte)(result & 0xFF);
        }

        public static bool GetFlag(Opcode.Condition cond)
        {
            switch (cond)
            {
                case Opcode.Condition.NotZero: return !cpu.state.Zero;
                case Opcode.Condition.Zero: return cpu.state.Zero;
                case Opcode.Condition.NotCarry: return !cpu.state.Carry;
                case Opcode.Condition.Carry: return cpu.state.Carry;
            }
            return false;
        }
    }
}
