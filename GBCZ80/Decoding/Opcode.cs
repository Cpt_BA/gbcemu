﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GBCZ80.Decoding
{
    public class Opcode
    {
        public delegate void OpcodeDelegate(Int32 paramValue, params object[] Paramaters);

        OpcodeDelegate method;
        public Params opcodeParam;
        public object[] delegateParams;
        public string name;
        public byte cost;
        public int opcodeNum;
        public bool isCB;

        public Opcode(int opcodeNumber, bool IsCBOpcode, String Name, OpcodeDelegate opcodeDelegate, Params opcodeParams, byte opcodeCost, params object[] functionParams)
        {
            name = Name;
            method = opcodeDelegate;
            opcodeParam = opcodeParams;
            opcodeNum = opcodeNumber;
            isCB = IsCBOpcode;

            List<object> tempParams = new List<object>();
            for (int i = 0; i < functionParams.Length; i += 2)
            {
                Type paramType = (Type)functionParams[i];
                tempParams.Add(Enum.ToObject(paramType, functionParams[i + 1]));
            }
            delegateParams = tempParams.ToArray();
            cost = opcodeCost;
            //Execute();
        }

        public static Type GetOpcodeType(OpcodeTypes type)
        {
            if (type == OpcodeTypes.Operation) return typeof(Operation);
            if (type == OpcodeTypes.Destination) return typeof(Destination);
            if (type == OpcodeTypes.Bit) return typeof(RegisterBit);
            if (type == OpcodeTypes.ResetLocation) return typeof(ResetLocation);

            if (type == OpcodeTypes.RegisterAF) return typeof(RegisterAF);
            if (type == OpcodeTypes.RegisterSP) return typeof(RegisterSP);
            if (type == OpcodeTypes.Condition) return typeof(Condition);

            if (type == OpcodeTypes.RegisterSingle) return typeof(RegisterSingle);
            if (type == OpcodeTypes.Direction) return typeof(RegisterSingle);
            return null;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Execute()
        {
            method(0, delegateParams);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Execute(int paramValue)
        {
            method(paramValue, delegateParams);
        }

        public enum Params
        {
            _8_BIT,
            _16_BIT,
            _NONE
        }

        public enum RegisterSingle
        {
            BC = 0,
            DE = 1
        }

        public enum Direction
        {
            LEFT = 0,
            RIGHT = 1
        }

        public enum RegisterSP
        {
            BC = 0,
            DE = 1,
            HL = 2,
            SP = 3
        }

        public enum RegisterAF
        {
            BC = 0,
            DE = 1,
            HL = 2,
            AF = 3
        }

        public enum Destination
        {
            B = 0,
            C = 1,
            D = 2,
            E = 3,
            H = 4,
            L = 5,
            HL = 6,
            A = 7
        }

        public enum RegisterBit
        {
            BIT_0 = 0,
            BIT_1 = 1,
            BIT_2 = 2,
            BIT_3 = 3,
            BIT_4 = 4,
            BIT_5 = 5,
            BIT_6 = 6,
            BIT_7 = 7,
        }

        public enum Condition
        {
            NotZero = 0,
            Zero = 1,
            NotCarry = 2,
            Carry = 3
        }

        public enum Operation : int
        {
            ADD = 0,
            ADC = 1,
            SUB = 2 ,
            SBC = 3,
            AND = 4,
            XOR = 5,
            OR = 6,
            CP = 7
        }

        public enum ResetLocation : int
        {
            H00 = 0,
            H08 = 1,
            H10 = 2,
            H18 = 3,
            H20 = 4,
            H28 = 5,
            H30 = 6,
            H38 = 7
        }
    }

    public class OpcodeTypes
    {
        public int value = 0;

        public OpcodeTypes(int val) { value = val; }

        public static OpcodeTypes Destination = new OpcodeTypes(3);
        public static OpcodeTypes Operation = new OpcodeTypes(3);
        public static OpcodeTypes Bit = new OpcodeTypes(3);
        public static OpcodeTypes ResetLocation = new OpcodeTypes(3);

        public static OpcodeTypes RegisterAF = new OpcodeTypes(2);
        public static OpcodeTypes RegisterSP = new OpcodeTypes(2);
        public static OpcodeTypes Condition = new OpcodeTypes(2);

        public static OpcodeTypes RegisterSingle = new OpcodeTypes(1);
        public static OpcodeTypes Direction = new OpcodeTypes(1);

        public static implicit operator int(OpcodeTypes ot)
        {
            return ot.value;
        }
    }
}
