﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBCZ80.Decoding
{
    public partial class Decoder
    {
        private static byte[] LD_D_D_COSTS = new byte[] { /*0x40*/4, 4, 4, 4, 4, 4, 8, 4,
                                                          /*0x48*/4, 4, 4, 4, 4, 4, 8, 4,
                                                          /*0x50*/4, 4, 4, 4, 4, 4, 8, 4,
                                                          /*0x58*/4, 4, 4, 4, 4, 4, 8, 4,
                                                          /*0x60*/4, 4, 4, 4, 4, 4, 8, 4,
                                                          /*0x68*/4, 4, 4, 4, 4, 4, 8, 4,
                                                          /*0x70*/4, 4, 4, 4, 4, 4, 8, 4,
                                                          /*0x78*/4, 4, 4, 4, 4, 4, 8, 4 };
        private static byte[] ALU_COSTS =    new byte[] { /*0x80*/4, 4, 4, 4, 4, 4, 8, 4,
                                                          /*0x88*/4, 4, 4, 4, 4, 4, 8, 4,
                                                          /*0x90*/4, 4, 4, 4, 4, 4, 8, 4,
                                                          /*0x98*/4, 4, 4, 4, 4, 4, 8, 4,
                                                          /*0xA0*/4, 4, 4, 4, 4, 4, 8, 4,
                                                          /*0xA8*/4, 4, 4, 4, 4, 4, 8, 4,
                                                          /*0xB0*/4, 4, 4, 4, 4, 4, 8, 4,
                                                          /*0xB8*/4, 4, 4, 4, 4, 4, 8, 4 };

        public static Z80CPU cpu;

        public static void Setup()
        {
            RegisterOpcode("NOP", (paramValue, e) => {}, new byte[]{4}, "00000000");
            RegisterOpcode("LD (N), SP", LD_N_SP, new byte[] { 20 }, "00001000", Opcode.Params._16_BIT);//Good
            RegisterOpcode("LD R,N", LD_R_N, new byte[] { 12, 12, 12, 12 }, "00", OpcodeTypes.RegisterSP, "0001", Opcode.Params._16_BIT);//Good
            RegisterOpcode("ADD HL,R", ADD_HL_R, new byte[] { 8, 8, 8, 8 }, "00", OpcodeTypes.RegisterSP, "1001");//Good
            RegisterOpcode("LD (R),A", LD_R_A, new byte[] { 8, 8 }, "000", OpcodeTypes.RegisterSingle, "0010");//Good
            RegisterOpcode("LD A,(R)", LD_A_R, new byte[] { 8, 8 }, "000", OpcodeTypes.RegisterSingle, "1010");//Good
            RegisterOpcode("INC R", INC_R, new byte[] { 8, 8, 8, 8 }, "00", OpcodeTypes.RegisterSP, "0011");//Good
            RegisterOpcode("DEC R", DEC_R, new byte[] { 8, 8, 8, 8 }, "00", OpcodeTypes.RegisterSP, "1011");//Good
            RegisterOpcode("INC D", INC_D, new byte[] { 4, 4, 4, 4, 4, 4, 12, 4 }, "00", OpcodeTypes.Destination, "100");//Good
            RegisterOpcode("DEC D", DEC_D, new byte[] { 4, 4, 4, 4, 4, 4, 12, 4 }, "00", OpcodeTypes.Destination, "101");//Good
            RegisterOpcode("LD D,N", LD_D_N, new byte[] { 4, 4, 4, 4, 4, 4, 12, 8 }, "00", OpcodeTypes.Destination, "110", Opcode.Params._8_BIT);//Good
            RegisterOpcode("RdCA", RD_CA, new byte[] { 4, 4 }, "0000", OpcodeTypes.Direction, "111");//Good (fixed)
            RegisterOpcode("RdA", RD_A, new byte[] { 4, 4 }, "0001", OpcodeTypes.Direction, "111");//Good (fixed)
            RegisterOpcode("STOP", (paramValue, parameters) => { }, new byte[] { 4 }, "00010000");
            RegisterOpcode("JR N", JR_N, new byte[] { 8 }, "00011000", Opcode.Params._8_BIT);//Good
            RegisterOpcode("JR F,N", JR_F_N, new byte[] { 12, 12, 12, 12 }, "001", OpcodeTypes.Condition, "000", Opcode.Params._8_BIT);//Good
            RegisterOpcode("LDI (HL),A", LDI_LH_A, new byte[] { 8 }, "00100010");//Good
            RegisterOpcode("LDI A,(HL)", LDI_A_HL, new byte[] { 8 }, "00101010");//Good
            RegisterOpcode("LDD (HL),A", LDD_LH_A, new byte[] { 8 }, "00110010");//Good
            RegisterOpcode("LDD A,(HL)", LDD_A_HL, new byte[] { 8 }, "00111010");//Good
            RegisterOpcode("DAA", DAA, new byte[] { 4 }, "00100111");//Good (rewrote)
            RegisterOpcode("CPL", (paramValue, parameters) => { cpu.state.A = (byte)~cpu.state.A; cpu.state.Op = true; cpu.state.HalCar = true; }, new byte[] { 4 }, "00101111");//Good
            RegisterOpcode("SCF", (paramValue, parameters) => { cpu.state.Carry = true; cpu.state.HalCar = false; cpu.state.Op = false; }, new byte[] { 4 }, "00110111");//Good
            RegisterOpcode("CCF", (paramValue, parameters) => { cpu.state.Carry = !cpu.state.Carry; cpu.state.HalCar = false; cpu.state.Op = false; }, new byte[] { 4 }, "00111111");//Good
            RegisterOpcode("LD D,D", LD_D_D, LD_D_D_COSTS, "01", OpcodeTypes.Destination, OpcodeTypes.Destination);//Good
            //HALT
            RegisterOpcode("ALU A,D", ALU_A_D, ALU_COSTS, "10", OpcodeTypes.Operation, OpcodeTypes.Destination);//Good
            RegisterOpcode("ALU A,N", ALU_A_N, new byte[] { 8, 8, 8, 8, 8, 8, 8, 8 }, "11", OpcodeTypes.Operation, "110", Opcode.Params._8_BIT);//Good
            RegisterOpcode("POP R", POP_R, new byte[] { 12, 12, 12, 12 }, "11", OpcodeTypes.RegisterAF, "0001");//Good
            RegisterOpcode("PUSH R", PUSH_R, new byte[] { 16, 16, 16, 16 }, "11", OpcodeTypes.RegisterAF, "0101");//Good
            //PST N
            RegisterOpcode("RET F", RET_F, new byte[] { 8, 8, 8, 8 }, "110", OpcodeTypes.Condition, "000");
            RegisterOpcode("RET", RET, new byte[] { 8 }, "11001001");
            RegisterOpcode("RETI", (paramValue, parameters) => { RET(0, null); EI(0, null); }, new byte[] { 8 }, "11011001");
            RegisterOpcode("JP F, N", JP_F_N, new byte[] { 12, 12, 12, 12 }, "110", OpcodeTypes.Condition, "010", Opcode.Params._16_BIT);
            RegisterOpcode("JP N", JP_N, new byte[] { 12 }, "11000011", Opcode.Params._16_BIT);
            RegisterOpcode("CALL F, N", CALL_F_N, new byte[] { 12, 12, 12, 12 }, "110", OpcodeTypes.Condition, "100", Opcode.Params._16_BIT);
            RegisterOpcode("CALL N", CALL_N, new byte[] { 12 }, "11001101", Opcode.Params._16_BIT);
            RegisterOpcode("ADD SP, N", ADD_SP_N, new byte[] { 16 }, "11101000", Opcode.Params._8_BIT);
            RegisterOpcode("LD HL, SP+N", LD_HL_SP_N, new byte[] { 12 }, "11111000", Opcode.Params._8_BIT);
            RegisterOpcode("LD (FF00+N), A", LD_FF00_N_A, new byte[] { 12 }, "11100000", Opcode.Params._8_BIT);
            RegisterOpcode("LD A, (FF00+N)", LD_A_FF00_N, new byte[] { 12 }, "11110000", Opcode.Params._8_BIT);
            RegisterOpcode("LD A, (C)", LD_A_C, new byte[] { 8 }, "11110010");
            RegisterOpcode("LD (C), A", LD_C_A, new byte[] { 8 }, "11100010");
            //LD A, (C)
            RegisterOpcode("LD (N), A", LD_N_A, new byte[] { 16 }, "11101010", Opcode.Params._16_BIT);
            RegisterOpcode("LD A, (N)", LD_A_N, new byte[] { 16 }, "11111010", Opcode.Params._16_BIT);
            RegisterOpcode("JP HL", JP_HL, new byte[] { 4 }, "11101001");
            RegisterOpcode("LD SP,HL", LD_SP_HL, new byte[] { 8 }, "11111001");
            RegisterOpcode("DI", DI, new byte[] { 4 }, "11110011");
            RegisterOpcode("EI", EI, new byte[] { 4 }, "11111011");
            RegisterOpcode("RST", RST, new byte[] { 32, 32, 32, 32, 32, 32, 32, 32 }, "11", OpcodeTypes.ResetLocation, "111");
            //CB Opcodes
            RegisterCB();
        }

        public static void AttachCPU(Z80CPU parent)
        {
            cpu = parent;
        }

        private static void LD_N_SP(int paramValue, params object[] parameters)
        {
            cpu.ram.WriteShort(paramValue, (short)cpu.state.SP);
        }

        private static void LD_R_N(int paramValue, params object[] parameters)
        {
            SetRegister16BitSP((Opcode.RegisterSP)parameters[0], (ushort)paramValue);
        }

        private static void ADD_HL_R(int paramValue, params object[] parameters)
        {
            ushort before = cpu.state.HL;
            ushort addr = GetRegister16BitSP((Opcode.RegisterSP)parameters[0]);
            cpu.state.HL += addr;

            cpu.state.Op = false;
            cpu.state.HalCar = (before & 0x0FFF) > (cpu.state.HL & 0xFFF);
            cpu.state.Carry = (before & 0xFFFF) > (cpu.state.HL & 0xFFFF);
        }

        private static void LD_R_A(int paramValue, params object[] parameters)
        {
            cpu.writeByte(GetRegister16BitSing((Opcode.RegisterSingle)parameters[0]), cpu.state.A);
        }

        private static void LD_A_R(int paramValue, params object[] parameters)
        {
            cpu.state.A = cpu.ram.ReadByte(GetRegister16BitSing((Opcode.RegisterSingle)parameters[0]));
        }

        private static void INC_R(int paramValue, params object[] parameters)
        {
            SetRegister16BitSP((Opcode.RegisterSP)parameters[0], (ushort)(GetRegister16BitSP((Opcode.RegisterSP)parameters[0]) + 1));
        }

        private static void DEC_R(int paramValue, params object[] parameters)
        {
            SetRegister16BitSP((Opcode.RegisterSP)parameters[0], (ushort)(GetRegister16BitSP((Opcode.RegisterSP)parameters[0]) - 1));
        }

        private static void INC_D(int paramValue, params object[] parameters)
        {
            Opcode.Destination dest = (Opcode.Destination)parameters[0];
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original + 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = (original & 0x0F) == 0x0F;
        }

        private static void DEC_D(int paramValue, params object[] parameters)
        {
            Opcode.Destination dest = (Opcode.Destination)parameters[0];
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original - 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = true;
            cpu.state.HalCar = !((original & 0x0F) == 0x00);
        }

        private static void LD_D_N(int paramValue, params object[] parameters)
        {
            SetRegister8Bit((Opcode.Destination)parameters[0], (byte)paramValue);
        }

        private static void RD_CA(int paramValue, params object[] parameters)
        {
            byte src = cpu.state.A;
            switch ((Opcode.Direction)parameters[0])
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    break;
            }
            cpu.state.Zero = false;// src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.A = src;
        }

        private static void RD_A(int paramValue, params object[] parameters)
        {
            byte src = cpu.state.A;
            bool oldCarry = cpu.state.Carry;
            switch ((Opcode.Direction)parameters[0])
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    break;
            }
            cpu.state.Zero = false;// src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.A = src;
        }

        private static void JR_N(int paramValue, params object[] parameters)
        {
            cpu.state.PC = (ushort)(cpu.state.PC + ((sbyte)(paramValue & 0xFF)));
        }

        private static void JR_F_N(int paramValue, params object[] parameters)
        {
            if (GetFlag((Opcode.Condition)parameters[0]))
            {
                cpu.state.PC = (ushort)(cpu.state.PC + ((sbyte)(paramValue & 0xFF)));
            }
        }

        private static void LDI_LH_A(int paramValue, params object[] parameters)
        {
            cpu.writeByte(cpu.state.HL, cpu.state.A);
            cpu.state.HL++;
        }

        private static void LDI_A_HL(int paramValue, params object[] parameters)
        {
            cpu.state.A = cpu.ram.ReadByte(cpu.state.HL);
            cpu.state.HL++;
        }

        private static void LDD_LH_A(int paramValue, params object[] parameters)
        {
            cpu.writeByte(cpu.state.HL, cpu.state.A);
            cpu.state.HL--;
        }

        private static void LDD_A_HL(int paramValue, params object[] parameters)
        {
            cpu.state.A = cpu.ram.ReadByte(cpu.state.HL);
            cpu.state.HL--;
        }

        private static void DAA(int paramValue, params object[] parameters)
        {
            uint temp = cpu.state.A;
            if (cpu.state.Op)
            {
                if (cpu.state.HalCar)
                    temp = (temp - 6) & 0xFF;
                if (cpu.state.Carry)
                    temp -= 0x60;
            }
            else
            {
                if (cpu.state.HalCar || ((temp & 0x0F) > 9))
                    temp += 6;
                if (cpu.state.Carry || (temp > 0x9F))
                    temp += 0x60;
            }
            if ((temp & 0x100) != 0)
                cpu.state.Carry = true;

            cpu.state.HalCar = false;
            temp &= 0xFF;

            cpu.state.Zero = temp == 0x00;

            cpu.state.A = (byte)temp;
        }

        private static void LD_D_D(int paramValue, params object[] parameters)
        {
            Opcode.Destination to = (Opcode.Destination)parameters[0];
            Opcode.Destination from = (Opcode.Destination)parameters[1];
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
        }

        private static void ALU_A_D(int paramValue, params object[] parameters)
        {
            cpu.state.A = ALU8Bit((Opcode.Operation)parameters[0], GetRegister8Bit((Opcode.Destination)parameters[1]));
        }

        private static void ALU_A_N(int paramValue, params object[] parameters)
        {
            cpu.state.A = ALU8Bit((Opcode.Operation)parameters[0], (byte)paramValue);
        }

        private static void POP_R(int paramValue, params object[] parameters)
        {
            SetRegister16BitAF((Opcode.RegisterAF)parameters[0], (ushort)cpu.ram.ReadInt16(cpu.state.SP));
            cpu.state.SP += 2;
            //Console.WriteLine("Pop");
        }

        private static void PUSH_R(int paramValue, params object[] parameters)
        {
            ushort value = GetRegister16BitAF((Opcode.RegisterAF)parameters[0]);
            cpu.PushStackShort(value);
            //Console.WriteLine("Push");
        }

        private static void RET_F(int paramValue, params object[] parameters)
        {
            if (GetFlag((Opcode.Condition)parameters[0]))
            {
                cpu.state.PC = (ushort)cpu.ram.ReadInt16(cpu.state.SP);
                cpu.state.SP += 2;
                //Console.WriteLine("Returning to 0x{0:X}", cpu.state.PC);
            }
        }

        private static void RET(int paramValue, params object[] parameters)
        {
            cpu.state.PC = (ushort)cpu.ram.ReadInt16(cpu.state.SP);
            cpu.state.SP += 2;
            //Console.WriteLine("Returning to 0x{0:X}", cpu.state.PC);
            if (cpu.state.PC == 0)
            {

            }
        }

        private static void JP_F_N(int paramValue, params object[] parameters)
        {
            if (GetFlag((Opcode.Condition)parameters[0]))
                cpu.state.PC = (ushort)paramValue;
        }

        private static void JP_N(int paramValue, params object[] parameters)
        {
            cpu.state.PC = (ushort)paramValue;
        }

        private static void CALL_F_N(int paramValue, params object[] parameters)
        {
            if (GetFlag((Opcode.Condition)parameters[0]))
            {
                cpu.PushStackShort(cpu.state.PC);
                cpu.state.PC = (ushort)paramValue;
                //Console.WriteLine("Call Conditional 0x{0:X}", paramValue);
            }
        }

        private static void CALL_N(int paramValue, params object[] parameters)
        {
            cpu.PushStackShort(cpu.state.PC);
            cpu.state.PC = (ushort)paramValue;
            //Console.WriteLine("Call 0x{0:X}", paramValue);
        }

        private static void ADD_SP_N(int paramValue, params object[] parameters)
        {
            short offset = (short)((sbyte)paramValue);
            cpu.state.Carry = (0xFF - (cpu.state.SP & 0x00FF) < offset);
            cpu.state.HalCar = (0x0F - (cpu.state.SP & 0x000F) < (offset & 0x0f));
            cpu.state.SP = (ushort)(cpu.state.SP + offset);
            cpu.state.Zero = false;
            cpu.state.Op = false;
            //Console.WriteLine("SP Moved");
        }

        private static void LD_HL_SP_N(int paramValue, params object[] parameters)
        {
            short offset = (short)((sbyte)paramValue);
            cpu.state.Carry = (0xFF - (cpu.state.SP & 0x00FF) < offset);
            cpu.state.HalCar = (0x0F - (cpu.state.SP & 0x000F) < (offset & 0x0f));
            cpu.state.HL = (ushort)(cpu.state.SP + offset);
            cpu.state.Zero = false;
            cpu.state.Op = false;
        }

        private static void LD_FF00_N_A(int paramValue, params object[] parameters)
        {
            cpu.writeByte(0xFF00 + paramValue, cpu.state.A);
        }

        private static void LD_A_FF00_N(int paramValue, params object[] parameters)
        {
            cpu.state.A = cpu.ram.ReadByte(0xFF00 + paramValue);
        }

        private static void LD_A_C(int paramValue, params object[] parameters)
        {
            cpu.state.A = cpu.ram.ReadByte(0xFF00 + cpu.state.C);
        }

        private static void LD_C_A(int paramValue, params object[] parameters)
        {
            cpu.writeByte(0xFF00 + cpu.state.C, cpu.state.A);
        }

        private static void LD_N_A(int paramValue, params object[] parameters)
        {
            cpu.writeByte(paramValue, cpu.state.A);
        }

        private static void LD_A_N(int paramValue, params object[] parameters)
        {
            cpu.state.A = cpu.ram.ReadByte(paramValue);
        }

        private static void JP_HL(int paramValue, params object[] parameters)
        {
            cpu.state.PC = cpu.state.HL;
        }

        private static void LD_SP_HL(int paramValue, params object[] parameters)
        {
            cpu.state.SP = cpu.state.HL;
            //Console.WriteLine("SP <- HL");
        }

        private static void DI(int paramValue, params object[] parameters)
        {
            cpu.state.InterupEnabled = false;
        }

        private static void EI(int paramValue, params object[] parameters)
        {
            cpu.state.InterupEnabled = true;
            cpu.ram.WriteByte(GBRAM.IORegisters.IF, 0);
        }

        private static void RST(int paramValue, params object[] parameters)
        {
            cpu.PushStackShort(cpu.state.PC);
            switch ((Opcode.ResetLocation)parameters[0])
            {
                case Opcode.ResetLocation.H00: cpu.state.PC = 0; break;
                case Opcode.ResetLocation.H08: cpu.state.PC = 0x08; break;
                case Opcode.ResetLocation.H10: cpu.state.PC = 0x10; break;
                case Opcode.ResetLocation.H18: cpu.state.PC = 0x18; break;
                case Opcode.ResetLocation.H20: cpu.state.PC = 0x20; break;
                case Opcode.ResetLocation.H28: cpu.state.PC = 0x28; break;
                case Opcode.ResetLocation.H30: cpu.state.PC = 0x30; break;
                case Opcode.ResetLocation.H38: cpu.state.PC = 0x38; break;
            }
            //Console.WriteLine("Reset");
        }
    }
}
