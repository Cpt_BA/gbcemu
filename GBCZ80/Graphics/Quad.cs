﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using SharpDX;
using SharpDX.DXGI;

namespace GBCZ80.Graphics
{
    using SharpDX.Direct3D11;
    public class Quad : IDisposable
    {
        private InputLayout layout;
        private SamplerState sampleState;
        private Buffer matrixBuffer;
        private Buffer vertexBuffer;
        private Buffer indexBuffer;

        private VertexShader vertexShader;
        private PixelShader pixelShader;



        public void Initalize(Device device, VertexShader vertShader,PixelShader pixShader,byte[] shaderByteCode)
        {
            vertexShader = vertShader;
            pixelShader = pixShader;
            var inputElements = new InputElement[]
            {
                new InputElement()
                {
                    SemanticName = "POSITION",
                    SemanticIndex = 0,
                    Format = Format.R32G32B32_Float,
                    Slot = 0,
                    AlignedByteOffset = 0,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                },
                new InputElement()
                {
                    SemanticName = "TEXCOORD",
                    SemanticIndex = 0,
                    Format = Format.R32G32_Float,
                    Slot = 0,
                    AlignedByteOffset = Vertex.AppendAlignedElement,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                }
            };

            layout = new InputLayout(device, shaderByteCode, inputElements);

            var matrixBufferDescription = new BufferDescription()
            {
                Usage = ResourceUsage.Dynamic,
                SizeInBytes = Utilities.SizeOf<MatrixBuffer>(),
                BindFlags = BindFlags.ConstantBuffer,
                CpuAccessFlags = CpuAccessFlags.Write,
                OptionFlags = ResourceOptionFlags.None,
                StructureByteStride = 0
            };

            matrixBuffer = new Buffer(device, matrixBufferDescription);

            var samplerDescription = new SamplerStateDescription()
            {
                Filter = Filter.MinLinearMagPointMipLinear ,
                AddressU = TextureAddressMode.Wrap,
                AddressV = TextureAddressMode.Wrap,
                AddressW = TextureAddressMode.Wrap,
                ComparisonFunction = Comparison.Greater,
                BorderColor = Color.Green,
                MinimumLod = 0,
                MaximumLod = 0,
                MaximumAnisotropy = 0
            };
            sampleState = new SamplerState(device, samplerDescription);
            InitializeVertices(device);
        }

        private void InitializeVertices(Device device)
        {
            var vertices = new[]
             {
                 new Vertex()
                 {
                     position = new Vector3(-.5f,-.5f,0), //llower left
                     texture = new Vector2(0,1)
                 },
                 new Vertex()
                 {
                     position = new SharpDX.Vector3(.5f,.5f,0), //uppper right
                     texture = new SharpDX.Vector2(1,0)
                 },
                 new Vertex()
                 {
                    position = new SharpDX.Vector3(.5f,-.5f,0),
                    texture = new SharpDX.Vector2(1,1)
                 },
                 new Vertex()
                 {
                    position = new SharpDX.Vector3(-.5f,.5f,0),
                    texture = new SharpDX.Vector2(0,0)
                 }
             };

            var indices = new[]
            {   
                0,
                1,
                2,
                1,
                0,
                3
            };

            vertexBuffer = Buffer.Create(device, BindFlags.VertexBuffer, vertices);
            indexBuffer = Buffer.Create(device, BindFlags.IndexBuffer, indices);
        }

        public void Dispose()
        {
            Utilities.Dispose(ref sampleState);
        }

        public void SetShaderParameters(DeviceContext context, ShaderResourceView texture)
        {
            var matrix = new MatrixBuffer()
            {
                world = Matrix.Identity,
                view = View,
                projection = Projection
            }; //i dont know if we need these yet, so more of placeholders

            matrix.world.Transpose();
            matrix.view.Transpose();
            matrix.projection.Transpose();

            if (texture != null)
            {
            }
                DataStream mappedResource;
                context.MapSubresource(matrixBuffer, MapMode.WriteDiscard, MapFlags.None, out mappedResource);

                mappedResource.Write(matrix);
                context.UnmapSubresource(matrixBuffer, 0);


            context.VertexShader.SetConstantBuffer(0, matrixBuffer);
            context.PixelShader.SetShaderResource(0, texture);
        }

        public void Render(DeviceContext context)
        {
            context.InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(vertexBuffer, Utilities.SizeOf<Vertex>(), 0));
            context.InputAssembler.SetIndexBuffer(indexBuffer, Format.R32_UInt, 0);
            context.InputAssembler.PrimitiveTopology = SharpDX.Direct3D.PrimitiveTopology.TriangleList;

            // Set the vertex input layout.
            context.InputAssembler.InputLayout = layout;

            // Set the vertex and pixel shaders that will be used to render this triangle.
            context.VertexShader.Set(vertexShader);
            context.PixelShader.Set(pixelShader);
            // Set the sampler state in the pixel shader.
            context.PixelShader.SetSampler(0,sampleState);

            // Render the triangle.
            context.DrawIndexed(6, 0, 0); 

        }

        public Matrix World { get; set; }
        public Matrix View { get; set; }
        public Matrix Projection { get; set; }

        [StructLayout(LayoutKind.Sequential)]
        public struct Vertex
        {

            public static int AppendAlignedElement = 12;

            public Vector3 position;

            public Vector2 texture;

        }



        [StructLayout(LayoutKind.Sequential)]
        public struct MatrixBuffer
        {

            public Matrix world;
            public Matrix view;
            public Matrix projection;

        } 

    }
}
