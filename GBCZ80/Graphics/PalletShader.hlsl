
//struct VS_INPUT
//{
//   float4 vPosition : POSITION;
//   float3 vNormal : NORMAL;
//   float4 vBlendWeights : BLENDWEIGHT;
//};

Texture2D shaderTexture;
SamplerState SampleType;

struct PixelInputType
{
   float4 position : SV_POSITION;
   float2 tex : TEXCOORD0;
};

float4 ps_main(PixelInputType input) : SV_TARGET
{

   float4 textureColor;
   // Sample the pixel color from the texture using the sampler at this texture coordinate location
   textureColor = shaderTexture.Sample(SampleType, input.tex);

   return textureColor;// float4(input.position.y, 0, 0, 1);
}


//float4 ps_main() : COLOR0
//{
//   return float4(0, 1, 0, 1);
//}


//technique Default
//{
//   pass Pass1
//   {
//      //no pixel shader, this is 2D shader silly
//      //VertexShader = compile vs_3_0 VertexShaderFunction();
//      PixelShader = compile ps_2_0 PixelShaderFunction();
//   }
//}