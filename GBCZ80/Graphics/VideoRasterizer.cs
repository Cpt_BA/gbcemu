﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.DXGI;


namespace GBCZ80.Graphics
{
    using SharpDX.Direct3D11;
    public unsafe class VideoRasterizer : IDisposable
    {
        private Z80CPU cpu;
        private Device device;


        public static int[] baseColors = new int[] { 0xFFFFFF, 0xA8A8A8, 0x606060, 0x000000 };
        public byte pallet;


        private Surface spriteSurface;

        public VideoRasterizer(Z80CPU cpu)
        {
            this.cpu = cpu;
        }

        public void Dispose()
        {
        }

        public void Render(DeviceContext context)
        {
        }

        public Surface CreateSpriteSurface(int width, int height)
        {
            var description = new Texture2DDescription()
            {
                Width = 160, //size to change
                Height = 144,
                Usage = ResourceUsage.Dynamic,
                OptionFlags = ResourceOptionFlags.None,
                SampleDescription = new SampleDescription(1, 0),
                BindFlags = SharpDX.Direct3D11.BindFlags.ShaderResource,
                CpuAccessFlags = SharpDX.Direct3D11.CpuAccessFlags.Write,
                ArraySize = 1,
                MipLevels = 1,
                Format = Format.R8G8B8A8_UNorm //one channel supported, so we write pallet colors, the shader will fill in the real color
            };


            var tex = new Texture2D(device, description);
            return tex.QueryInterface<Surface>();
        }

        public void CopyTexture(int address,int* copyTo, int pitch)
        {
            bool emptyTransparent = true;
            int offset = 0;
            for(int y = 0; y < 8; y++ )
            {
                byte lowByte = cpu.ram.ReadByte(address + (y*2));
                byte highByte = cpu.ram.ReadByte(address + (y*2) + 1);
               
                for(int x = 0; x < 8; x++)
                {
                    bool l1p = (lowByte & (1<<x)) != 0;
                    bool l2p = (highByte & (1<< x)) != 0;

                    int pixel = l1p ? 1 : 0;
                    pixel += l1p ? 2 : 0;

                     if ((emptyTransparent && pixel != 0) || !emptyTransparent)
                     {
                         copyTo[x + (y * pitch) ] = GetColor(pixel);
                   
                     }
                }
            }
        }

        private int GetColor(int index)
        {
            byte baseIndex = (byte)((pallet >> (index * 2)) & 0x03);
            return baseColors[baseIndex];
        }
    }
}
