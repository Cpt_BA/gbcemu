﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GBCZ80.Graphics
{
    public class VideoManager
    {
        Z80CPU cpu;

        public delegate void ScreenRendered(object sender, int[] screen, byte scx, byte scy);
        public event ScreenRendered Rendered;

        byte IE;

        int[] screenBuffer;
        public static int[] baseColors = new int[] { 0xFFFFFF, 0xA8A8A8, 0x606060, 0x000000 };

        public VideoManager(Z80CPU parent, bool autostart = true)
        {
            cpu = parent;
            screenBuffer = new int[256 * 256];
            if (autostart)
                StartThread();
        }

        public void StartThread()
        {
            Thread video_thread = new Thread(Runner);
            video_thread.Name = "Video Thread";
            video_thread.Priority = ThreadPriority.BelowNormal;
            video_thread.IsBackground = true;
            video_thread.Start();
        }

        private void Runner()
        {
            ColorPallet bgp,obp0,obp1;
            int scx = 0, scy = 0;
            Sprite[] sprites = new Sprite[40];

            while (true)
            {
                IE = cpu.ram.ReadByte(0xFFFF);

                for (byte scanY = 0; scanY <= 160; scanY++)
                {
                    bgp.pallet = cpu.ram.ReadByte(GBRAM.IORegisters.BGP);
                    obp0.pallet = cpu.ram.ReadByte(GBRAM.IORegisters.OBP0);
                    obp1.pallet = cpu.ram.ReadByte(GBRAM.IORegisters.OBP1);

                    byte STAT = updateSTAT(0x02);
                    
                    cpu.ram.WriteByte(0xFF44, scanY);
                    byte LYC = cpu.ram[GBRAM.IORegisters.LYC];
                    if (LYC == scanY && ((STAT & 0x04) == 0x04)) cpu.ram[GBRAM.IORegisters.STAT] |= (1 << 6);
                    if (LYC != scanY && ((STAT & 0x04) != 0x04)) cpu.ram[GBRAM.IORegisters.STAT] |= (1 << 6);

                    if (LYC == scanY && ((STAT & 0x40) == 0x40) && cpu.state.InterupEnabled && ((IE & 0x02) == 0x02))
                    {
                        cpu.SetInterupt(Z80CPU.InteruptType.LCDCStatus);
                    }
                    cpu.Run(77, Z80CPU.WaiterPosition.Video);

                    STAT = updateSTAT(0x03);
                    cpu.Run(169, Z80CPU.WaiterPosition.Video);

                    byte LCDC = cpu.ram[GBRAM.IORegisters.LCDC];
                    //Rendering Loop

                    int wx = (byte)(cpu.ram.ReadByte(GBRAM.IORegisters.WX)-7);
                    int wy = cpu.ram.ReadByte(GBRAM.IORegisters.WY);
                    scx = cpu.ram.ReadByte(GBRAM.IORegisters.SCX);
                    scy = cpu.ram.ReadByte(GBRAM.IORegisters.SCY);
                    if (wx >= 0xF9) wx = 0;

                    int scanYAbs = (scanY + scy) % 256;

                    int start = ((LCDC & (1 << 4)) != 0) ? 0x8000 : 0x8800;
                    int bgstart = ((LCDC & (1 << 3)) != 0) ? 0x9C00 : 0x9800;
                    int windstart = ((LCDC & (1 << 6)) != 0) ? 0x9C00 : 0x9800;

                    for (byte i = 0; i < 40; i++)
                        sprites[i] = Sprite.FromInt(cpu.ram.ReadInt32(0xFE00 + (i * 4)), i);

                    sprites = sprites.OrderBy((a) => a.X).ThenBy((a) => a.index).ToArray();

                    RenderSprites(scanY, scx, scy, LCDC, obp0, obp1, sprites, true);

                    bgstart += (scanYAbs / 8) * 32;
                    int tileY = scanYAbs - (scanYAbs % 8);

                    for (int tileIndex = 0; tileIndex < 32; tileIndex++)
                    {
                        byte index = cpu.ram.ReadByte(bgstart + tileIndex);
                        if (start == 0x8800) index ^= 1 << 7;
                        drawSpriteRow(
                            tileIndex * 8,
                            tileY,
                            start + (index * 0x10),
                            0,
                            scanYAbs % 8,
                            bgp);
                    }

                    if ((LCDC & (1 << 5)) != 0)
                    {
                        if (scanY >= wy)
                        {
                            scanYAbs = (scanY - wy) % 256;
                            tileY = scanYAbs - (scanYAbs % 8);
                            windstart += (scanYAbs / 8) * 32;

                            for (int tileIndex = 0; tileIndex < 32; tileIndex++)
                            {
                                byte index = cpu.ram.ReadByte(windstart + tileIndex);
                                if (start == 0x8800) index ^= 1 << 7;

                                drawSpriteRow(
                                    (tileIndex * 8) + scx + wx,
                                    tileY + wy + scy,
                                    start + (index * 0x10),
                                    0,
                                    scanYAbs % 8,
                                    bgp);
                            }
                        }
                    }


                    RenderSprites(scanY, scx, scy, LCDC, obp0, obp1, sprites, false);

                    updateSTAT(0x00);
                    cpu.Run(201, Z80CPU.WaiterPosition.Video);
                }

                updateSTAT(0x01);

                if (cpu.state.InterupEnabled && (IE & 0x01) == 0x01)
                    cpu.SetInterupt(Z80CPU.InteruptType.VBlank);

                if (Rendered != null)
                    Rendered(this, screenBuffer, (byte)scx, (byte)scy);

                cpu.Run(4560, Z80CPU.WaiterPosition.Video);
            }
        }

        private void RenderSprites(int scanY, int ScrollX, int ScrollY, int LCDC, ColorPallet pallet1, ColorPallet pallet2, Sprite[] sprites, bool lowerLayer)
        {
            int scanYAbs = (scanY + ScrollY) % 256;
            bool expandedSprite = (LCDC & (1 << 2)) != 0;

            if ((LCDC & (1 << 1)) != 0)
            {
                foreach (Sprite sprite in sprites.Where((e) => (e.background == lowerLayer) && e.enabled))
                {
                    int xPosition = (sprite.X + ScrollX) % 256;
                    int yPosition = (sprite.Y + ScrollY) % 256;
                    if (sprite.Y+16 >= scanY && sprite.Y+16 <= scanY + 8) //this is clips it within window?
                    {
                        int scanLine = (scanY - sprite.Y) % 8;
                        if (sprite.yFlip) scanLine = 7 - scanLine;
                        int memoryLocation = sprite.getMemoryLocation(expandedSprite);
                        byte flags = sprite.flags;
                        ColorPallet pallet = sprite.pallet ? pallet2 : pallet1;

                        if (!expandedSprite)
                        {
                            drawSpriteRow(xPosition, yPosition, memoryLocation, flags, scanLine, pallet, true);
                        }
                        else
                        {
                            if (sprite.yFlip)
                            {
                                drawSpriteRow(xPosition, yPosition + 8, memoryLocation, flags, scanLine, pallet, true);
                                drawSpriteRow(xPosition, yPosition, memoryLocation + 0x10, flags, scanLine, pallet, true);
                            }
                            else
                            {
                                drawSpriteRow(xPosition, yPosition, memoryLocation, flags, scanLine, pallet, true);
                                drawSpriteRow(xPosition, yPosition + 8, memoryLocation + 0x10, flags, scanLine, pallet, true);
                            }
                        }
                    }
                }
            }
        }

        private byte updateSTAT(byte code)
        {
            byte STAT = cpu.ram[GBRAM.IORegisters.STAT];
            STAT = (byte)((STAT & 0xFC) | code);
            cpu.ram[GBRAM.IORegisters.STAT] = STAT;

            if(cpu.state.InterupEnabled && ((IE & 0x02) == 0x02))
            {
                if ((STAT & (1 << 3)) != 0 && code == 0)
                    cpu.SetInterupt(Z80CPU.InteruptType.LCDCStatus);
                else if ((STAT & (1 << 4)) != 0 && code == 1)
                    cpu.SetInterupt(Z80CPU.InteruptType.LCDCStatus);
                else if ((STAT & (1 << 5)) != 0 && code == 2)
                    cpu.SetInterupt(Z80CPU.InteruptType.LCDCStatus);
            }

            return STAT;
        }

        Random r = new Random();

        private void drawSpriteRow(int x, int y, int memBase, byte flags, int row, ColorPallet pallet, bool emptyTransparent = false)
        {
            bool flipX = (flags & (1 << 5)) != 0;
            bool flipY = (flags & (1 << 6)) != 0;

            byte l1 = cpu.ram.ReadByte(memBase + (row * 2));
            byte l2 = cpu.ram.ReadByte(memBase + (row * 2) + 1);
            for (byte xPos = 0; xPos < 8; xPos++)
            {
                int newX = (flipX ? xPos : (7 - xPos));
                bool l1p = (l1 & (1 << xPos)) != 0;
                bool l2p = (l2 & (1 << xPos)) != 0;
                byte pixel = (byte)((l1p ? 1 : 0) + (l2p ? 2 : 0));

                //Console.WriteLine("Set Pixel");
                if ((emptyTransparent && pixel != 0) || !emptyTransparent)
                {
                    setPixel(x + newX, y + (flipY ? (7 - row) : row), pallet.getColor(pixel));
                }
            }

           
        }

        private void setPixel(int x, int y, int value)
        {
            x %= 256;
            y %= 256;

            screenBuffer[x + 256 * y] = value;
        }

        struct ColorPallet
        {
            public byte pallet;

            public int getColor(byte index)
            {
                byte baseIndex = (byte)((pallet >> (index * 2)) & 0x03);
                return baseColors[baseIndex];
            }
        }

        struct Sprite
        {
            byte x;
            byte y;
            byte tile;
            public byte flags;
            public byte index;

            public static Sprite FromInt(int value, byte index)
            {
                Sprite s = new Sprite();
                s.y = (byte)((value >> 0) & 0xFF);
                s.x = (byte)((value >> 8) & 0xFF);
                s.tile = (byte)((value >> 16) & 0xFF);
                s.flags = (byte)((value >> 24) & 0xFF);
                s.index = index;
                return s;
            }

            public int getMemoryLocation(bool expanded)
            {
                byte to = (byte)(tile & (expanded ? 0xFE : 0xFF));
                return 0x8000 + (to * 0x10);
            }

            public byte X { get { return (byte)(x - 8); } }
            public byte Y { get { return (byte)(y - 16); } }

            public bool pallet { get { return (flags & (1 << 4)) != 0; } }
            public bool xFlip { get { return (flags & (1 << 5)) != 0; } }
            public bool yFlip { get { return (flags & (1 << 6)) != 0; } }
            public bool background { get { return (flags & (1 << 7)) != 0; } }

            public bool enabled { get { return x != 0 || y != 0; } }

            public override string ToString()
            {
                return String.Format("Enabled: {0}, x: {1}, y: {2}, tile:{3}", enabled, x, y, tile);
            }
        }

    }
}
