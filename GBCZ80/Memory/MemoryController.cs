﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;

namespace GBCZ80.Memory
{
    /// <summary>
    /// Controls any special logic triggered by writing or reading ram
    /// </summary>
    public unsafe class MemoryController : IDisposable
    {
        protected GBRAM ram;
        protected byte* switchableRam;
        protected Cartridge cartridge;
        
        public bool usesSave;
        public bool saveQueued;

        public MemoryController(Cartridge cart, bool battery = false)
        {
            cartridge = cart;
            //switchableRam = (byte*)Marshal.AllocHGlobal(Cartridge.RAM_BANKSIZE*4).ToPointer();
            usesSave = battery;
            if (usesSave)
            {
                string fileLoc = cartridge.GetSaveFileLocation();
                if (fileLoc != null)
                {

                }
                else
                {
                    Console.WriteLine("Save File could not be found, disabling save");
                    usesSave = false;
                }
            }
        }

        public void Dispose()
        {
            DisposeRam();
        }


        protected virtual void DisposeRam()
        {
            if (switchableRam != null)
            {
                Marshal.FreeHGlobal(new IntPtr(switchableRam));
            }
        }


        /// <summary>
        /// Allocates ram for bank controller, if ram has allready been allocated
        /// this function will automaticly dispose of it.
        /// </summary>
        protected virtual void AllocateRam(int amount)
        {
            if (switchableRam != null)
                DisposeRam();
            switchableRam =(byte*)Marshal.AllocHGlobal(amount).ToPointer();

            if (usesSave && cartridge.GetSaveFileLocation() != null && File.Exists(cartridge.GetSaveFileLocation()))
            {
                byte[] data = File.ReadAllBytes(cartridge.GetSaveFileLocation());
                for (int i = 0; i < Math.Min(amount, data.Length); i++)
                    switchableRam[i] = data[i];
                Console.WriteLine("Loaded RAM");
            }
            else
            {
                for (int i = 0; i < amount; i++)
                    switchableRam[i] = 0;
            }
        }

        protected virtual int ToCartMemLocation(int position)
        {
            if (position >= GBRAM.Offsets.SWITCHABLE_RAMBANK)
            {
                return position - GBRAM.Offsets.SWITCHABLE_RAMBANK;
            }
            else if (position >= GBRAM.Offsets.SWITCHABLE_ROMBANK)
            {
                return position - GBRAM.Offsets.SWITCHABLE_ROMBANK;
            }
            
            return position; //this must be rom 0 no transform is needed
        }
        

        public virtual bool PreprocessWrite(ref int position, int value)
        {
            position = ToCartMemLocation(position);
            return false;
        }

        public virtual bool PreprocessRead(ref int positon)
        {
            positon = ToCartMemLocation(positon);
            return false;
        }


        public byte* GetRAMBank(int bank)
        {
            int offset = Cartridge.RAM_BANKSIZE * bank ; 
            return switchableRam + offset;
        }

        private int _romBank;
        public int ROMBank;
        /*{
            get
            {
                return _romBank;
            }
            set
            {
                _romBank = value;
                if (_romBank != value || selectedROMBank == null)
                {
                    if (ROMBanks.ContainsKey(value))
                    {
                        selectedROMBank = ROMBanks[ROMBank];
                    }
                    else
                    {
                        UnmanagedArray ua = new UnmanagedArray(cartridge.GetRomBank(ROMBank), this, Cartridge.ROM_BANKSIZE);
                        ROMBanks[ROMBank] = ua;
                        selectedROMBank = ua;
                    }
                }
            }
        }*/
        public int RAMBank { get; set; }

        public bool RAMEnabled { get; set; }

        private Dictionary<int, UnmanagedArray> ROMBanks = new Dictionary<int, UnmanagedArray>();
        private Dictionary<int, UnmanagedArray> RAMBanks = new Dictionary<int, UnmanagedArray>();

        public IMemory _RomCache;

        private IMemory selectedROMBank;
        /*public IMemory SelectedROMBank
        {
            get
            {
                if (selectedROMBank == null)
                    ROMBank = _romBank; //'refresh' the selected bank

                return selectedROMBank;
            }
        }*/

        public IMemory SelectedROMBank
        {
            get
            {
                if (ROMBanks.ContainsKey(ROMBank))
                    return ROMBanks[ROMBank];
                else
                {
                    UnmanagedArray ua = new UnmanagedArray(cartridge.GetRomBank(ROMBank), this, Cartridge.ROM_BANKSIZE);
                    ROMBanks[ROMBank] = ua;
                    return ua;
                }
            }
        }

        public IMemory SelectedRAMBank
        {
            get
            {
                if (RAMBanks.ContainsKey(RAMBank))
                    return RAMBanks[RAMBank];
                else
                {
                    UnmanagedArray ua = new UnmanagedArray(GetRAMBank(RAMBank), this, Cartridge.RAM_BANKSIZE);
                    RAMBanks[RAMBank] = ua;
                    return ua;
                }
            }
        }


    }

}
