﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;

namespace GBCZ80.Memory
{
    /// <summary>
    /// Represents a cartrage from a stream. Once created most of this classes
    /// fields are parsed from the cartrages header but this is only done once.
    /// Not recomneded to force the header to change at all.
    /// </summary>
    public unsafe class Cartridge : IDisposable
    {
        public static readonly int ROM_BANKSIZE = 0x4000; //16 KB
        public static readonly int RAM_BANKSIZE = 0x2000; //8  KB
        
        private byte* romMemory;
        private CartridgeHeader parsedHeader;
        string romPath = null;

        private Cartridge(byte* newMemory) 
        {
            romMemory = newMemory;
            // offset 0x104 is the nintendo graphic which is where we start reading for metadata
            var headerStart = new IntPtr(romMemory + 0x104);

            parsedHeader = (CartridgeHeader)Marshal.PtrToStructure(headerStart, typeof(CartridgeHeader));
          
            byte[] headerNameBuffer = new byte[15];
            fixed(byte* rawName = parsedHeader.RawCartridgeName)
                for (int i = 0; i < headerNameBuffer.Length; i++)
                    headerNameBuffer[i] = rawName[i];
            Name = ASCIIEncoding.ASCII.GetString(headerNameBuffer).TrimEnd('\0');
            Console.WriteLine("Loaded: {0}", Name);
            Console.WriteLine("Memory Type: {0}", (CartridgeType)parsedHeader.CartType);
        }

        public static Cartridge Reload(Cartridge existing)
        {
            Cartridge newCart;
            newCart = Cartridge.From(existing.romPath);
            existing.Dispose();
            return newCart;
        }

        public void Dispose()
        {
            Marshal.Release(new IntPtr(romMemory));
            romMemory = null;
        }

        public static Cartridge Empty()
        {
            using (MemoryStream ms = new MemoryStream(0xFFFF))
            {
                for (int i = 0; i < 0xFFFF; i++)
                    ms.WriteByte(0);
                ms.Seek(0, SeekOrigin.Begin);
                return From(ms);
            }
        }

        public static Cartridge From(string filePath)
        {
            Cartridge c;
            using (FileStream file = new FileStream(filePath,FileMode.Open))
                c = From(file);
            if (c != null)
                c.romPath = filePath;
            return c;
        }

        /// <summary> Loads a file into this cartrage class
        /// current implementation ends up reading in the file twice.
        /// Once into a byte array and again into an unmanaged buffer. 
        /// I cant think of a way to do it all in one shot.
        /// </summary>
        public static Cartridge From(Stream stream)
        {
            //read file into a bytebuffer
            stream.Seek(0, SeekOrigin.Begin);
            byte[] fileBuffer = new byte[stream.Length];
            stream.Read(fileBuffer, 0, fileBuffer.Length);

            byte* romMemory = (byte*)Marshal.AllocHGlobal(fileBuffer.Length).ToPointer();
            //copy filebuffer into unmanaged buffer
            
            for (int i = 0; i < fileBuffer.Length; i++)
                romMemory[i] = fileBuffer[i];
            
            Cartridge finalCartrage = new Cartridge(romMemory);

            return finalCartrage;
        }

        /// <summary> Just for debuging right now, using this method then manualy writing vlaues means
        /// the header wont be exposed by this class which could (Eventualy) screw up the ram controller
        /// </summary>
        public static Cartridge CreateEmpty(int size)
        {
            byte* romMemory = (byte*)Marshal.AllocHGlobal(size).ToPointer();

            return new Cartridge(romMemory);
        }

        public byte* GetRomBank(int bank)
        {
            int offset = ROM_BANKSIZE * bank;
            if (bank != 0)
            {

            }
            return romMemory + offset;
        }

        public string Name
        {
            get;
            private set;
        }

        public string GetSaveFileLocation()
        {
            if (romPath != null)
            {
                return Path.ChangeExtension(romPath, "sav");
            }
            else
                return null;
        }

        public CartridgeHeader Header { get { return parsedHeader; } }
    }

    [StructLayout( LayoutKind.Sequential,Pack=1)]
    public unsafe struct CartridgeHeader
    {
        public fixed byte Nintendo[48];
        public fixed byte RawCartridgeName[15];
        public byte HardwareType;
        public short LicenseCode;
        public byte SuperGameboy;
        public CartridgeType CartType;
        public byte RomSize;
        public byte RamSize;
        public byte DestinataionCode;
        public byte LicensCodeOld;
        public byte MaskVersion; 
    }

    public enum CartridgeType : byte
    {
        RomOnly = 0x0,
        Rom_MBC1 = 0x1,
        Rom_MBC1_RAM = 0x2,
        Rom_MBC1_RAM_BATT = 0x3,
        Rom_MBC2 = 0x5,
        Rom_MBC2_BATT = 0x6,
        Rom_RAM = 0x8,
        Rom_MMM01 = 0xB,
        Rom_MMM01_SRAM = 0xC,
        Rom_MMM01_SRAM_BATT = 0xD,
        Rom_MBC3_RAM = 0x12,
        Rom_MBC3_RAM_BATT = 0x13,
        Rom_MBC5 = 0x19,
        Rom_MBC5_RAM = 0x1A,
        Rom_MBC5_RAM_BATT = 0x1B,
        Rom_MBC5_RUMBLE = 0x1C,
        Rom_MBC5_RUMBLE_SRAM = 0x1D,
        Rom_MBC5_RUMBLE_SRAM_BATT = 0x1E,
        PocketCamera = 0x1F,
        Bandai_TAMA5 = 0xFD,
        Hudson_HuC3 = 0xFE,
    }
}
