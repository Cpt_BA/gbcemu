﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBCZ80.Memory
{
    public interface IMemory
    {
        void Write(int position, int value);
        void Write(int position, short value);
        void Write(int position, byte value);
        
        byte ReadByte(int position);
        short ReadInt16(int position);
        int ReadInt32(int position);
    }
}
