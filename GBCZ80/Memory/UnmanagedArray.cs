﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using GBCZ80.Audio;

namespace GBCZ80.Memory
{
    public sealed class UnmanagedArray : IDisposable,IMemory
    {
        private unsafe byte* memory;
        private object owner; //for debuging
        private int maxSize;
        public unsafe UnmanagedArray(byte* toWrap)
        {
            //System.Diagnostics.Debug.Assert(toWrap != null);
            memory = toWrap;
            //memoryHandle = new IntPtr(memory);
        }

        //special constructor with debuging parameters
        public unsafe UnmanagedArray(byte* toWrap, object owner, int maxMemorySize) : this(toWrap)
        {
            maxSize = maxMemorySize;
            this.owner = owner;
        }

        public UnmanagedArray(int size)
        {
            unsafe
            {
                memory = (byte*)Marshal.AllocHGlobal(size).ToPointer();
            }
        }

        public void Dispose()
        {
            unsafe
            {
                Marshal.FreeHGlobal(new IntPtr(memory));
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(int position, int value)
        {
            unsafe
            {
                if ((position & 0x3) == 0) //is this 4 byte aligned? we can to this extra fast
                {
                    int newPosition = position >> 2; //divide by 4
                    ((int*)memory)[newPosition] = value;
                    System.Diagnostics.Debug.Assert(position % 4 == 0);
                }
                else //do slow way
                {
                    byte[] array = BitConverter.GetBytes(value);
                    for (int i = 0; i < 4; ++i)
                        memory[position + i] = array[i];
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(int position, short value)
        {
            unsafe
            {
                if ((position & 0x1) == 0) //is this two byte aligned?
                {
                    int newPosition = position >> 1; //divide by two
                    ((short*)memory)[newPosition] = value;
                }
                else
                {
                    byte[] array = BitConverter.GetBytes(value);
                    for (int i = 0; i < 2; ++i)
                        memory[position + i] = array[i];
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Write(int position, byte value)
        {
            //Channel is enabled (see length counter).
            //If length counter is zero, it is set to 64 (256 for wave channel).
            //Frequency timer is reloaded with period.
            //Volume envelope timer is reloaded with period.
            //Channel volume is reloaded from NRx2.
            //Noise channel's LFSR bits are all set to 1.
            //Wave channel's position is set to 0 but sample buffer is NOT refilled.
            //Square 1's sweep does several things (see frequency sweep).

            //Name Addr 7654 3210 Function
            //-----------------------------------------------------------------
            //       Square 1
            //NR10 FF10 -PPP NSSS Sweep period, negate, shift
            //NR11 FF11 DDLL LLLL Duty, Length load (64-L)
            //NR12 FF12 VVVV APPP Starting volume, Envelope add mode, period
            //NR13 FF13 FFFF FFFF Frequency LSB
            //NR14 FF14 TL-- -FFF Trigger, Length enable, Frequency MSB
            //
            //       Square 2
            //     FF15 ---- ---- Not used
            //NR21 FF16 DDLL LLLL Duty, Length load (64-L)
            //NR22 FF17 VVVV APPP Starting volume, Envelope add mode, period
            //NR23 FF18 FFFF FFFF Frequency LSB
            //NR24 FF19 TL-- -FFF Trigger, Length enable, Frequency MSB
            //
            //       Wave
            //NR30 FF1A E--- ---- DAC power
            //NR31 FF1B LLLL LLLL Length load (256-L)
            //NR32 FF1C -VV- ---- Volume code (00=0%, 01=100%, 10=50%, 11=25%)
            //NR33 FF1D FFFF FFFF Frequency LSB
            //NR34 FF1E TL-- -FFF Trigger, Length enable, Frequency MSB
            //
            //       Noise
            //     FF1F ---- ---- Not used
            //NR41 FF20 --LL LLLL Length load (64-L)
            //NR42 FF21 VVVV APPP Starting volume, Envelope add mode, period
            //NR43 FF22 SSSS WDDD Clock shift, Width mode of LFSR, Divisor code
            //NR44 FF23 TL-- ---- Trigger, Length enable
            //
            //       Control/Status
            //NR50 FF24 ALLL BRRR Vin L enable, Left vol, Vin R enable, Right vol
            //NR51 FF25 NW21 NW21 Left enables, Right enables
            //NR52 FF26 P--- NW21 Power control/status, Channel length statuses
            //
            //       Not used
            //     FF27 ---- ----
            //     .... ---- ----
            //     FF2F ---- ----
            //
            //       Wave Table
            //     FF30 0000 1111 Samples 0 and 1
            //     ....
            //     FF3F 0000 1111 Samples 30 and 31

            switch(position)
            {
                case 0xFF07:
                    Console.WriteLine("Timer: 0x{0:X}", value);
                    break;
                case GBRAM.IORegisters.DIV:
                    value = 0;
                    break;
                case GBRAM.IORegisters.NR10:
                    SoundManager._chan1.sweep.Shift = value & 0x07;
                    SoundManager._chan1.sweep.Direction = (value >> 3) & 0x01;
                    SoundManager._chan1.sweep.Period = (value >> 4) & 0x07;
                    break;
                case GBRAM.IORegisters.NR11:
                    SoundManager._chan1.length.SoundLength = 64 - (value & 0x3F);
                    SoundManager._chan1.duty.DutyMode = (value & 0xC0) >> 6;
                    break;
                case GBRAM.IORegisters.NR12:
                    SoundManager._chan1.envelope.InitialVolume = (value & 0xF0) >> 4;
                    SoundManager._chan1.envelope.Mode = (value & 0x08) >> 3;
                    SoundManager._chan1.envelope.Period = (value & 0x07);
                    break;
                case GBRAM.IORegisters.NR13:
                    SoundManager._chan1.Frequency = (SoundManager._chan1.Frequency & 0xFF00) | value;
                    break;
                case GBRAM.IORegisters.NR14:
                    SoundManager._chan1.Frequency = (SoundManager._chan1.Frequency & 0x00FF) | (((int)value & 0x07) << 8);
                    if ((value & 0x80) != 0)
                    {
                        SoundManager._chan1.Enabled = true;
                        SoundManager._chan1.length.SoundLength = 64;
                        SoundManager._chan1.envelope.Volume = SoundManager._chan1.envelope.InitialVolume;
                        SoundManager._chan1.FrequencyShadow = SoundManager._chan1.Frequency;
                        SoundManager._chan1.envelope.PeriodCounter = 0;
                        SoundManager._chan1.sweep.Counter = 0;
                    }
                    break;
                case GBRAM.IORegisters.NR21:
                    SoundManager._chan2.length.SoundLength = 64 - (value & 0x3F);
                    SoundManager._chan2.duty.DutyMode = (value & 0xC0) >> 6;
                    break;
                case GBRAM.IORegisters.NR22:
                    SoundManager._chan2.envelope.InitialVolume = (value & 0xF0) >> 4;
                    SoundManager._chan2.envelope.Mode = (value & 0x08) >> 3;
                    SoundManager._chan2.envelope.Period = (value & 0x07);
                    break;
                case GBRAM.IORegisters.NR23:
                    SoundManager._chan2.Frequency = (SoundManager._chan2.Frequency & 0xFF00) | value;
                    break;
                case GBRAM.IORegisters.NR24:
                    SoundManager._chan2.Frequency = (SoundManager._chan2.Frequency & 0x00FF) | ((value & 0x07) << 8);
                    if ((value & 0x80) != 0)
                    {
                        SoundManager._chan2.Enabled = true;
                        SoundManager._chan2.length.SoundLength = 64;
                        SoundManager._chan2.envelope.Volume = SoundManager._chan2.envelope.InitialVolume;
                        SoundManager._chan2.envelope.PeriodCounter = 0;
                    }
                    break;
                case GBRAM.IORegisters.NR31:
                    SoundManager._chan3.length.SoundLength = 256 - (value & 0x3F);
                    break;
                case GBRAM.IORegisters.NR32:
                    if (value == 0)
                        SoundManager._chan3.OutputShift = 4;
                    else
                        SoundManager._chan3.OutputShift = value - 1;
                    break;
                case GBRAM.IORegisters.NR33:
                    SoundManager._chan3.Frequency = (SoundManager._chan3.Frequency & 0xFF00) | value;
                    break;
                case GBRAM.IORegisters.NR34:
                    SoundManager._chan3.Frequency = (SoundManager._chan3.Frequency & 0x00FF) | ((value & 0x07) << 8);
                    if ((value & 0x80) != 0)
                    {
                        SoundManager._chan3.Enabled = true;
                        SoundManager._chan3.length.SoundLength = 256;
                        SoundManager._chan3.SampleIndex = 0;
                    }
                    break;
                case GBRAM.IORegisters.NR52:
                    if ((value & 0x80) != 0)
                    {
                        Write(GBRAM.IORegisters.NR50, ReadByte(GBRAM.IORegisters.NR50) | 0x88);
                    }
                    break;
                case 0xFF46:
                    //Console.WriteLine("DMA Transfer");
                    int dma_source = value * 0x100;
                    for (int i = 0; i < 0xA0; i++)
                    {
                        Write(0xFE00 + i, ReadByte(dma_source + i));
                    }
                    break;
                case 0xFFFF:
                    //Console.WriteLine("Interrupts: 0x{0:X}", value);
                    break;
                case 0xFF53:
                    break;
                case 0xFF55:
                    int sourceAddr = (ReadByte(0xFF51) << 8) | (ReadByte(0xFF52) & 0xF0);
                    int destAddr = 0x8000 | ((ReadByte(0xFF53) & 0x1F) << 8) | (ReadByte(0xFF54) & 0xF0);
                    int count = value & 0x7F;
                    //Console.WriteLine("DMA Transfer from {0:X} to {1:X}", sourceAddr, destAddr);
                    for (int i = 0; i < (count + 1) * 0x10; i++)
                    {
                        Write(destAddr + i, ReadByte(i + sourceAddr));
                    }
                    break;
            }
            
            unsafe
            {
                memory[position] = value;
            }
        }

        public void Write<T>(int position, T structure) where T:struct
        {
            unsafe
            {
                IntPtr handle = new IntPtr(memory + position);
                Marshal.StructureToPtr(structure, handle, false);
            }
        }

        public int ReadInt32(int position)
        {
            unsafe
            {
                return (int)((memory[position + 3] << 24) | (memory[position + 2] << 16) | (memory[position + 1] << 8) | (memory[position]));
            }
        }

        public short ReadInt16(int position)
        {
            unsafe
            {
                return (short)((memory[position + 1] << 8) | (memory[position]));
            }
        }

        public byte ReadByte(int position)
        {
            unsafe
            {
                if (position == GBRAM.IORegisters.NR52)
                {
                    byte b = memory[position];

                    b &= 0x80;

                    if (SoundManager._chan1.Enabled) b |= 0x01;
                    if (SoundManager._chan2.Enabled) b |= 0x02;
                    if (SoundManager._chan3.Enabled) b |= 0x04;

                    return b;
                }
                else
                {
                    return memory[position];
                }
            }
        }
    }
}
