﻿using GBCZ80.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBCZ80.CartridgeTypes
{
    public class ROMOnly : MemoryController
    {
        public ROMOnly(Cartridge cart) : base(cart)
        {
            AllocateRam(Cartridge.RAM_BANKSIZE);
            ROMBank = 1;
        }

        public override bool PreprocessWrite(ref int position, int value)
        {
            if (position < 0x8000)
                return true;
            return false;
        }
    }
}
