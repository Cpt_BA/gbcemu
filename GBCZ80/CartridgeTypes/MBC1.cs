﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO.MemoryMappedFiles;
using Microsoft.Win32.SafeHandles;
using GBCZ80.Memory;

namespace GBCZ80.CartridgeTypes
{
    /// <summary>
    /// MBC1 has two difrent maximum memory modes
    /// 16Mbit ROM/8KB Ram or 4 MbitROM and 32KB ram
    /// </summary>
    public class MBC1 : MemoryController
    {
        protected bool isIn8kRamMode;
        public MBC1(Cartridge cart, bool battery=false):base(cart, battery)
        {
            mode = MemoryMode.ROM16mRAM8k; //Default mode
            AllocateRam(0x8000); //Allow for max ram to begin with
            RAMEnabled = true;
            ROMBank = 1;
        }

        public override bool PreprocessWrite(ref int position, int value)
        {
            if (position >= 0x2000 && position < 0x4000)
            {
                int bankToSelect = value & 0x1F;
                bankToSelect = Math.Max(bankToSelect, 1);
                ROMBank = (ROMBank & 0xE0) | bankToSelect;
            }
            else if (position >= 0x4000 && position < 0x6000)
            {
                if (mode == MemoryMode.ROM16mRAM8k) //16/8 mode
                {
                    ROMBank = (ROMBank & 0x1F) | ((value & 0x03) << 5);
                }
                else // 4/32 Mode
                {
                    RAMBank = (value & 0x03);
                }
            }
            else if (position >= 0x6000 && position < 0x8000)
            {
                mode = (MemoryMode)(value & 0x01);
            }

            if (position >= 0xA000 && position < 0xC000)
                saveQueued = true;

            if (position < 0x8000)
                return true;
            position = ToCartMemLocation(position);
            return false;
        }

        public override bool PreprocessRead(ref int positon)
        {
            return base.PreprocessRead(ref positon);
        }

        public bool RamBankEnabled { get; set; }

        public MemoryMode mode;
        public enum MemoryMode:int
        {
            ROM16mRAM8k = 0, //16 Mbit, 8  Kbyte
            ROM4mRAM21k = 1  //4  Mbit, 32 kByte
        }
    }
}
