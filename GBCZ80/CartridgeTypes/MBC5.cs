﻿using GBCZ80.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBCZ80.CartridgeTypes
{
    public class MBC5 : MemoryController
    {
        public MBC5(Cartridge cart, bool battery = false)
            : base(cart, battery)
        {
            AllocateRam(512*640);
            RAMEnabled = false;
            ROMBank = 0;
        }

        public override bool PreprocessWrite(ref int position, int value)
        {
            if (position >= 0x0000 && position <= 0x1FFF)
                RAMEnabled = value == 0x0A;
            else if (position >= 0x2000 && position <= 0x2FFF)
                ROMBank = (ROMBank & 0xFF00) | value;
            else if (position >= 0x3000 && position <= 0x3FFF)
                ROMBank = (ROMBank & 0x00FF) | ((value & 0x01) << 8);
            else if (position >= 0x4000 && position <= 0x5FFF)
                RAMBank = value & 0x0F;


            if (position >= 0xA000 && position < 0xC000)
                saveQueued = true;

            if (position < 0x8000)
                return true;
            position = ToCartMemLocation(position);
            return false;
        }
    }
}
