﻿using GBCZ80.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBCZ80.CartridgeTypes
{
    public class MBC2 : MemoryController
    {
        public MBC2(Cartridge cart, bool battery = false)
            : base(cart, battery)
        {
            AllocateRam(512*640);
            RAMEnabled = false;
            ROMBank = 1;
        }

        public override bool PreprocessWrite(ref int position, int value)
        {
            if (position >= 0 && position <= 0x1EFF)
            {
                Console.WriteLine("RAM Switch");
            }
            if (position >= 0x2000 && position <= 0x3FFF)
            {
                if ((value & 0x10F) != 0)
                {
                    ROMBank = Math.Max(value, 1);
                    Console.WriteLine("Switching Banks: {0}", ROMBank);
                }
                else
                {

                }
            }

            if (position >= 0xA000 && position < 0xC000)
                saveQueued = true;

            if (position < 0x8000)
                return true;
            position = ToCartMemLocation(position);
            return false;
        }
    }
}
