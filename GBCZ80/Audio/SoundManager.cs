﻿using System;
using System.Threading;
using System.Linq;
using SharpDX;
using SharpDX.Multimedia;
using SharpDX.DirectSound;

namespace GBCZ80.Audio
{
    public class SoundManager
    {
        Z80CPU cpu;

        DirectSound _ds;
        WaveFormat format;
        SecondarySoundBuffer _secondarySoundBuffer;

        Thread sampleThread;
        AutoResetEvent sampleBufferFilled;

        static short channels = 2;
        static short bits = 8;
        static int bufferSamples = 2048;
        static int bytesPerSample = channels * (bits / 8);

        static int _bufferSize = bufferSamples * bytesPerSample;
        byte[] _soundBuffer = new byte[_bufferSize];
        byte[] _writeBuffer = new byte[128];
        int _writeBufferPosition = 0;

        int lastWritePosition;

        long LengthNextOperation;
        long EnvelopeNextOperation;
        long SweepNextOpartion;
        long SampleGenerateNextCycle;

        public static Channel1 _chan1 = new Channel1();
        public static Channel2 _chan2 = new Channel2();
        public static Channel3 _chan3 = new Channel3();

        public static volatile int TestFrequency = 0;
        public static volatile int SamplesWritten = 0;

        public SoundManager(IntPtr hwnd, Z80CPU cpu)
        {
            this.cpu = cpu;

            _chan1.envelope.Volume = 0x0F;
            _chan2.envelope.Volume = 0x0F;

            _chan3.ProduceAudio = _chan2.ProduceAudio = _chan1.ProduceAudio = true;


            var devices = DirectSound.GetDevices();
            DeviceInformation outputDevice = devices.FirstOrDefault(d => d.Description.StartsWith("CABLE"));
            if(outputDevice == null) outputDevice = devices.First();

            _ds = new DirectSound(outputDevice.DriverGuid);
            _ds.SetCooperativeLevel(hwnd, CooperativeLevel.Priority);

            format = new WaveFormat(44100, bits, channels);

            SoundBufferDescription description = new SoundBufferDescription
            {
                Format = format,
                Flags =
                    BufferFlags.GlobalFocus |
                    BufferFlags.Software |
                    BufferFlags.GetCurrentPosition2 |
                    BufferFlags.ControlVolume |
                    BufferFlags.ControlPositionNotify |
                    BufferFlags.Trueplayposition,
                BufferBytes = _bufferSize
            };

            for (int i = 0; i < _bufferSize; i++)
                _soundBuffer[i] = 0x80;

            _secondarySoundBuffer = new SecondarySoundBuffer(_ds, description);
            //DSoundBuffer.Volume = 10;

            _secondarySoundBuffer.Write(_soundBuffer, 0, LockFlags.EntireBuffer);
            _secondarySoundBuffer.Play(0, PlayFlags.Looping);

            sampleBufferFilled = new AutoResetEvent(false);
            sampleThread = new Thread(SampleThread);
            sampleThread.Name = "Audio Sample Generation Thread";
            sampleThread.IsBackground = true;
        }

        public void StartSampleThread()
        {
            sampleThread.Start();
        }

        private void SampleThread()
        {
            int bytesAvailable;
            int samplesAvailable;
            int bufferIndex, playIndex;

            while (true)
            {
                bool triggered = sampleBufferFilled.WaitOne(10);


                _secondarySoundBuffer.GetCurrentPosition(out playIndex, out bufferIndex);
                bytesAvailable = circularDist(lastWritePosition, playIndex, _soundBuffer.Length);//GetAvailableBytes(out bufferIndex);
                samplesAvailable = bytesAvailable / bytesPerSample;

                if (triggered)
                {
                    //if (samplesAvailable >= _writeBufferPosition / 2)
                    //{
                    //enough room in audio buffer to write into
                    //_secondarySoundBuffer.Write(_writeBuffer, 0, LockFlags.FromWriteCursor);
                    //TODO: Mashal.Copy
                    int samplesToWrite = Math.Min(_writeBufferPosition / 2, samplesAvailable);
                    for (int writeOffset = 0; writeOffset < samplesToWrite; writeOffset++)
                        _soundBuffer[(lastWritePosition + writeOffset) % _soundBuffer.Length] = _writeBuffer[writeOffset];
                    _secondarySoundBuffer.Write(_soundBuffer, 0, LockFlags.EntireBuffer);

                    lastWritePosition += samplesToWrite * bytesPerSample;
                    SamplesWritten += samplesToWrite;
                    _writeBufferPosition = 0;
                    /*}
                    else
                    {
                        Console.WriteLine("Not Enough room to write to audio buffer {0}/{1}", samplesAvailable, _writeBufferPosition / 2);
                    }*/
                }
                else
                {
                    Thread.Yield();
                }
            }
        }

        private void NewSamplesAvailable()
        {
            if (_writeBufferPosition < 128)
            {
                GetSamples(1, out _writeBuffer[_writeBufferPosition], out _writeBuffer[_writeBufferPosition + 1]);
                _writeBufferPosition += 2;
            }

            if (_writeBufferPosition == 128)
                sampleBufferFilled.Set();
        }

        private int GetAvailableBytes(out int writePosition)
        {
            int play;
            int lastWritePosition;
            _secondarySoundBuffer.GetCurrentPosition(out play, out lastWritePosition);
            writePosition = lastWritePosition;
            return circularDist(lastWritePosition, play, _soundBuffer.Length);
        }

        public long nextCycle(long counter, float frequency)
        {
            if (frequency == 0.0)
                return 0;
            return (counter + (int)(Z80CPU.CyclesPerSecond / frequency));
        }

        public int GetChan3Sample(int sample)
        {
            int value;
            int wave = cpu.ram[GBRAM.IORegisters.WavePatternRam + (sample >> 1)];

            if (sample % 2 == 1)
            {
                //Odd Samples
                value = (wave & 0x0F);
            }
            else
            {
                //Even Samples
                value = ((wave & 0xF0) >> 4);
            }

            //value = value - 8;

            return value;
        }

        public byte[] SampleLength = new byte[] { 1, 2, 4, 6 };

        public void GetSamples(int mode, out byte left, out byte right)
        {
            //byte volume = 0x07;

            if (mode == 0)
            {
                //Mono
                left = 0;
                right = 0;
            }
            else
            {
                //Stereo
                int chan1Sample = _chan1.sample;
                int chan2Sample = 0;// _chan2.sample;
                int chan3Sample = 0;// _chan3.sample;
                int chan4Sample = 0;
                byte NR50 = cpu.ram[GBRAM.IORegisters.NR50];

                byte mixingMode = cpu.ram[GBRAM.IORegisters.NR51];

                int leftSample = 0;
                int rightSample = 0;

                //Left
                if ((mixingMode & 0x01) != 0) leftSample += chan1Sample;
                if ((mixingMode & 0x02) != 0) leftSample += chan2Sample;
                if ((mixingMode & 0x04) != 0) leftSample += chan3Sample;
                if ((mixingMode & 0x08) != 0) leftSample += chan4Sample;

                //volume = (byte)(NR50 & 0x07);

                //Right
                if ((mixingMode & 0x10) != 0) rightSample += chan1Sample;
                if ((mixingMode & 0x20) != 0) rightSample += chan2Sample;
                if ((mixingMode & 0x40) != 0) rightSample += chan3Sample;
                if ((mixingMode & 0x80) != 0) rightSample += chan4Sample;

                //volume = (byte)((NR50 >> 4) & 0x07);

                left = (byte)(Math.Min(Math.Max(leftSample + 0x80, 0), 0xFF));
                right = (byte)(Math.Min(Math.Max(rightSample + 0x80, 0), 0xFF));
            }
            
        }

        public void SoundUpdate(long cpuCycles)
        {
            UpdateChannel1(cpuCycles);
            UpdateChannel2(cpuCycles);
            UpdateChannel3(cpuCycles);

            if (cpuCycles >= LengthNextOperation) LengthNextOperation = nextCycle(LengthNextOperation, 256);
            if (cpuCycles >= EnvelopeNextOperation) EnvelopeNextOperation = nextCycle(EnvelopeNextOperation, 64);
            if (cpuCycles >= SweepNextOpartion) SweepNextOpartion = nextCycle(SweepNextOpartion, 128);

            //currentLeftSample = getSample(1);
            //currentRightSample = getSample(2);

            if(cpuCycles >= SampleGenerateNextCycle)
            {
                SampleGenerateNextCycle += 95; //TODO: Adjust this for different clock rates

                NewSamplesAvailable();
            }
        }

        int circularDist(int from, int to, int size)
        {
            if (size == 0)
                return 0;
            int diff = (to - from);
            while (diff < 0)
                diff += size;
            return diff;
        }

        public void UpdateChannel1(long cpuCycles)
        {
            if (cpuCycles >= _chan1.LastOperation + _chan1.GetUpdateCycles())
            {
                _chan1.Counter = (_chan1.Counter + 1) % 8;
                _chan1.LastOperation = cpuCycles;
            }

            if (cpuCycles >= EnvelopeNextOperation)
            {
                TestFrequency += 1;
                if (_chan1.envelope.Period == 0)
                {
                    //Do Nothing
                }
                else
                {
                    _chan1.envelope.PeriodCounter++;
                    if (_chan1.envelope.PeriodCounter >= _chan1.envelope.Period)
                    {
                        _chan1.envelope.PeriodCounter = 0;
                        _chan1.envelope.Volume += (_chan1.envelope.Mode == 1 ? 1 : -1);
                        _chan1.envelope.Volume = Math.Min(Math.Max(0, _chan1.envelope.Volume), 15);
                    }
                }
            }

            if (cpuCycles >= LengthNextOperation)
            {
                if (_chan1.length.Consecutive == 1)
                {
                    if (_chan1.length.SoundLength != 0) _chan1.length.SoundLength--;
                    if (_chan1.length.SoundLength == 0)
                        _chan1.Enabled = false;
                }
            }

            if (cpuCycles >= SweepNextOpartion)
            {
                if (_chan1.sweep.Period == 0)
                {
                    //Do Nothing
                }
                else
                {
                    _chan1.sweep.Counter++;
                    if (_chan1.sweep.Counter >= _chan1.sweep.Period)
                    {
                        _chan1.sweep.Counter = 0;

                        if (_chan1.sweep.Shift != 0)
                        {
                            int adjustment = (_chan1.Frequency >> _chan1.sweep.Shift) * (_chan1.sweep.Direction == 0 ? 1 : -1);
                            if (_chan1.FrequencyShadow + adjustment > 2047)
                                _chan1.Enabled = false;
                            else
                                _chan1.FrequencyShadow = _chan1.FrequencyShadow + adjustment;
                        }
                    }
                }
            }

            int sample = 0;

            if (_chan1.Enabled && _chan1.ProduceAudio)
            {

                if ((Duty.patterns[_chan1.duty.DutyMode] & (1 << (_chan1.Counter % 0x7))) != 0)
                    sample = (_chan1.envelope.Volume);
                else
                    sample = -(_chan1.envelope.Volume);
            }

            _chan1.sample = sample;
        }

        public void UpdateChannel2(long cpuCycles)
        {
            if (cpuCycles >= nextCycle(_chan2.LastOperation, _chan2.GetUpdateFrequency()))
            {
                _chan2.Counter = (_chan2.Counter + 1) % 8;
                _chan2.LastOperation = cpuCycles;
            }

            if (cpuCycles >= EnvelopeNextOperation)
            {
                if (_chan2.envelope.Period == 0);//Do Nothing
                else
                {
                    _chan2.envelope.PeriodCounter++;
                    if (_chan2.envelope.PeriodCounter >= _chan2.envelope.Period)
                    {
                        _chan2.envelope.PeriodCounter = 0;
                        _chan2.envelope.Volume += (_chan2.envelope.Mode == 1 ? 1 : -1);
                        _chan2.envelope.Volume = Math.Min(Math.Max(0, _chan2.envelope.Volume), 15);
                    }
                }
            }

            if (cpuCycles >= LengthNextOperation)
            {
                if (_chan2.length.SoundLength != 0) _chan2.length.SoundLength--;
                if (_chan2.length.SoundLength == 0)
                    if (_chan2.length.Consecutive == 1)
                        _chan2.Enabled = false;
            }

            int sample = 0;

            if (_chan2.Enabled && _chan2.ProduceAudio)
                if ((Duty.patterns[_chan2.duty.DutyMode] & (1 << _chan2.Counter)) != 0)
                    sample = (_chan2.envelope.Volume);
                else
                    sample = -(_chan2.envelope.Volume);

            _chan2.sample = sample;
        }

        public void UpdateChannel3(long cpuCycles)
        {
            if (cpuCycles >= nextCycle(_chan3.LastOperation, _chan3.GetUpdateFrequency()))
            {
                _chan3.SampleIndex = (_chan3.SampleIndex + 1) % 32;
                _chan3.LastOperation = cpuCycles;
            }

            if (cpuCycles >= LengthNextOperation)
            {
                if (_chan3.length.SoundLength != 0) _chan3.length.SoundLength--;
                if (_chan3.length.SoundLength == 0)
                    if (_chan3.length.Consecutive == 1)
                        _chan3.Enabled = false;
            }

            _chan3.sample = GetChan3Sample(_chan3.SampleIndex) * 4;
        }

        public struct Channel1
        {
            public int sample;

            public long LastOperation;
            public bool Enabled;
            public bool ProduceAudio;
            public int Frequency;
            public int FrequencyShadow;

            public Envelope envelope;
            public Length length;
            public Sweep sweep;
            public Duty duty;

            public int Counter;

            public int GetUpdateCycles()
            {
                return 4 * (2048 - FrequencyShadow);
            }
        }

        public struct Channel2
        {
            public int sample;

            public long LastOperation;
            public bool Enabled;
            public bool ProduceAudio;
            public int Frequency;

            public Envelope envelope;
            public Length length;
            public Duty duty;

            public int Counter;

            public int GetUpdateFrequency()
            {
                return (int)(131072.0 / (2048.0 - Frequency));
            }
        }

        public struct Channel3
        {
            public int sample;

            public long LastOperation;
            public bool Enabled;
            public bool ProduceAudio;
            public int Frequency;

            public Length length;

            public int SampleIndex;
            public int OutputShift;

            public int GetUpdateFrequency()
            {
                return (int)(65536.0 / (2048.0 - Frequency));
            }
        }

        public struct Duty
        {
            public static byte[] patterns = new byte[]{0x80, 0xC0, 0xF0, 0xFC};
            public int DutyMode;
        }

        public struct Envelope
        {
            public int Mode;
            public int InitialVolume;
            public int Volume;
            public int PeriodCounter;
            public int Period;
        }

        public struct Sweep
        {
            public int Counter;
            public int Period;
            public int Shift;
            public int Direction;
        }

        public struct Length
        {
            public int SoundLength;
            public int Consecutive;
        }
    }
}
