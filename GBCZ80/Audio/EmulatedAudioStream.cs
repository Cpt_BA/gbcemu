﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace GBCZ80.Audio
{
    public class EmulatedAudioStream : DataStream
    {
        GameboyAudio _fromAudio;

        public EmulatedAudioStream(GameboyAudio audioSource) : base(4410 * 2, true, false)
        {
            _fromAudio = audioSource;
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (count == 1) return 0;

            buffer[offset + 0] = _fromAudio.GetSample(1);
            buffer[offset + 1] = _fromAudio.GetSample(2);
            return 2;
        }

        public override long Position
        {
            get
            {
                return 0;
            }

            set
            {
            }
        }

        public override int ReadByte()
        {
            throw new NotImplementedException();
        }

        public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
        {
            return base.BeginRead(buffer, offset, count, callback, state);
        }
        public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
        {
            return base.ReadAsync(buffer, offset, count, cancellationToken);
        }
        

    }
}
