﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBCZ80.Audio
{
    public class GameboyAudio
    {

        #region State

        Z80CPU cpu;

        long LengthNextOperation;
        long EnvelopeNextOperation;
        long SweepNextOpartion;

        public static Channel1 AudioChannel1 = new Channel1();
        public static Channel2 AudioChannel2 = new Channel2();
        public static Channel3 AudioChannel3 = new Channel3();

        public byte[] SampleLength = new byte[] { 1, 2, 4, 6 };

        #endregion

        public GameboyAudio(Z80CPU parent)
        {
            cpu = parent;
        }

        private long nextCycle(long counter, float frequency)
        {
            if (frequency == 0.0)
                return 0;
            return (counter + (long)(Z80CPU.CyclesPerSecond / frequency));
        }

        #region Sample Generation

        public byte GetSample(int mode)
        {
            int sample = 0;
            byte volume = 0x07;

            if (mode == 0)
            {
                //Mono
            }
            else
            {
                //Stereo
                int chan1Sample = 0;
                int chan2Sample = 0;
                int chan3Sample = 0;
                int chan4Sample = 0;
                byte NR50 = cpu.ram[GBRAM.IORegisters.NR50];

                if (AudioChannel1.Enabled && AudioChannel1.ProduceAudio)
                {
                    if (((Duty.patterns[AudioChannel1.duty.DutyMode] >> AudioChannel1.Counter) & 0x01) == 1)
                        chan1Sample += (AudioChannel1.envelope.Volume * 2);
                    else
                        chan1Sample -= (AudioChannel1.envelope.Volume * 2);
                }

                if (AudioChannel2.Enabled && AudioChannel2.ProduceAudio)
                {
                    if (((Duty.patterns[AudioChannel2.duty.DutyMode] >> AudioChannel2.Counter) & 0x01) == 1)
                        chan2Sample += (AudioChannel2.envelope.Volume * 2);
                    else
                        chan2Sample -= (AudioChannel2.envelope.Volume * 2);
                }
                if (AudioChannel3.Enabled && AudioChannel3.ProduceAudio)
                {
                    chan3Sample = GetChan3Sample(AudioChannel3.SampleIndex) * 4;
                }

                byte mixingMode = cpu.ram[GBRAM.IORegisters.NR51];

                if (mode == 1)
                {
                    //Left
                    if ((mixingMode & 0x01) != 0) sample += chan1Sample;
                    if ((mixingMode & 0x02) != 0) sample += chan2Sample;
                    if ((mixingMode & 0x04) != 0) sample += chan3Sample;
                    if ((mixingMode & 0x08) != 0) sample += chan4Sample;

                    volume = (byte)(NR50 & 0x07);
                }
                else if (mode == 2)
                {
                    //Right
                    if ((mixingMode & 0x10) != 0) sample += chan1Sample;
                    if ((mixingMode & 0x20) != 0) sample += chan2Sample;
                    if ((mixingMode & 0x40) != 0) sample += chan3Sample;
                    if ((mixingMode & 0x80) != 0) sample += chan4Sample;

                    volume = (byte)((NR50 >> 4) & 0x07);
                }
            }

            return (byte)(0x80 + (Math.Min(Math.Max(sample, -0x80), 0x7f)));
        }

        public int GetChan3Sample(int sample)
        {
            int value;
            int wave = cpu.ram[GBRAM.IORegisters.WavePatternRam + (sample >> 1)];

            if (sample % 2 == 1)
            {
                //Odd Samples
                value = (wave & 0x0F);
            }
            else
            {
                //Even Samples
                value = ((wave & 0xF0) >> 4);
            }

            //value = value - 8;

            return value;
        }

        #endregion

        #region Channel Logic

        public void SoundUpdate(long cpuCycles)
        {
            UpdateChannel1(cpuCycles);
            UpdateChannel2(cpuCycles);
            UpdateChannel3(cpuCycles);

            if (cpuCycles >= LengthNextOperation) LengthNextOperation = nextCycle(cpuCycles, 256);
            if (cpuCycles >= EnvelopeNextOperation) EnvelopeNextOperation = nextCycle(cpuCycles, 64);
            if (cpuCycles >= SweepNextOpartion) SweepNextOpartion = nextCycle(cpuCycles, 128);
        }

        public void UpdateChannel1(long cpuCycles)
        {
            if (cpuCycles >= AudioChannel1.LastOperation + AudioChannel1.GetUpdateCycles())
            {
                AudioChannel1.Counter = (AudioChannel1.Counter + 1) % 8;
                AudioChannel1.LastOperation = cpuCycles;
            }

            if (cpuCycles >= EnvelopeNextOperation)
            {
                if (AudioChannel1.envelope.Period == 0)
                {
                    //Do Nothing
                }
                else
                {
                    AudioChannel1.envelope.PeriodCounter++;
                    if (AudioChannel1.envelope.PeriodCounter >= AudioChannel1.envelope.Period)
                    {
                        AudioChannel1.envelope.PeriodCounter = 0;
                        AudioChannel1.envelope.Volume += (AudioChannel1.envelope.Mode == 1 ? 1 : -1);
                        AudioChannel1.envelope.Volume = Math.Min(Math.Max(0, AudioChannel1.envelope.Volume), 15);
                    }
                }
            }

            if (cpuCycles >= LengthNextOperation)
            {
                if (AudioChannel1.length.SoundLength != 0) AudioChannel1.length.SoundLength--;
                if (AudioChannel1.length.SoundLength == 0)
                    if (AudioChannel1.length.Consecutive == 1)
                        AudioChannel1.Enabled = false;
            }

            if (cpuCycles >= SweepNextOpartion)
            {
                if (AudioChannel1.sweep.Period == 0)
                {
                    //Do Nothing
                }
                else
                {
                    AudioChannel1.sweep.Counter++;
                    if (AudioChannel1.sweep.Counter >= AudioChannel1.sweep.Period)
                    {
                        AudioChannel1.sweep.Counter = 0;

                        if (AudioChannel1.sweep.Shift != 0)
                        {
                            int adjustment = (AudioChannel1.Frequency >> AudioChannel1.sweep.Shift) * (AudioChannel1.sweep.Direction == 0 ? 1 : -1);
                            if (AudioChannel1.FrequencyShadow + adjustment > 2047)
                                AudioChannel1.Enabled = false;
                            else
                                AudioChannel1.FrequencyShadow = AudioChannel1.FrequencyShadow + adjustment;
                        }
                    }
                }
            }
        }

        public void UpdateChannel2(long cpuCycles)
        {
            if (cpuCycles >= nextCycle(AudioChannel2.LastOperation, AudioChannel2.GetUpdateFrequency()))
            {
                AudioChannel2.Counter = (AudioChannel2.Counter + 1) % 8;
                AudioChannel2.LastOperation = cpuCycles;
            }

            if (cpuCycles >= EnvelopeNextOperation)
            {
                if (AudioChannel2.envelope.Period == 0) ;//Do Nothing
                else
                {
                    AudioChannel2.envelope.PeriodCounter++;
                    if (AudioChannel2.envelope.PeriodCounter >= AudioChannel2.envelope.Period)
                    {
                        AudioChannel2.envelope.PeriodCounter = 0;
                        AudioChannel2.envelope.Volume += (AudioChannel2.envelope.Mode == 1 ? 1 : -1);
                        AudioChannel2.envelope.Volume = Math.Min(Math.Max(0, AudioChannel2.envelope.Volume), 15);
                    }
                }
            }

            if (cpuCycles >= LengthNextOperation)
            {
                if (AudioChannel2.length.SoundLength != 0) AudioChannel2.length.SoundLength--;
                if (AudioChannel2.length.SoundLength == 0)
                    if (AudioChannel2.length.Consecutive == 1)
                        AudioChannel2.Enabled = false;
            }
        }

        public void UpdateChannel3(long cpuCycles)
        {
            if (cpuCycles >= nextCycle(AudioChannel3.LastOperation, AudioChannel3.GetUpdateFrequency()))
            {
                AudioChannel3.SampleIndex = (AudioChannel3.SampleIndex + 1) % 32;
                AudioChannel3.LastOperation = cpuCycles;
            }

            if (cpuCycles >= LengthNextOperation)
            {
                if (AudioChannel3.length.SoundLength != 0) AudioChannel3.length.SoundLength--;
                if (AudioChannel3.length.SoundLength == 0)
                    if (AudioChannel3.length.Consecutive == 1)
                        AudioChannel3.Enabled = false;
            }
        }

        #endregion

        #region Structs        

        public struct Channel1
        {
            public byte sample;

            public long LastOperation;
            public bool Enabled;
            public bool ProduceAudio;
            public int Frequency;
            public int FrequencyShadow;

            public Envelope envelope;
            public Length length;
            public Sweep sweep;
            public Duty duty;

            public int Counter;

            public int GetUpdateFrequency()
            {
                return (int)(131072.0 / (2048.0 - FrequencyShadow));
            }

            public int GetUpdateCycles()
            {
                return (2048 - FrequencyShadow);
            }
        }

        public struct Channel2
        {
            public byte sample;

            public long LastOperation;
            public bool Enabled;
            public bool ProduceAudio;
            public int Frequency;

            public Envelope envelope;
            public Length length;
            public Duty duty;

            public int Counter;

            public int GetUpdateFrequency()
            {
                return (int)(131072.0 / (2048.0 - Frequency));
            }
        }

        public struct Channel3
        {
            public byte sample;

            public long LastOperation;
            public bool Enabled;
            public bool ProduceAudio;
            public int Frequency;

            public Length length;

            public int SampleIndex;
            public int OutputShift;

            public int GetUpdateFrequency()
            {
                return (int)(65536.0 / (2048.0 - Frequency));
            }
        }

        public struct Duty
        {
            public static byte[] patterns = new byte[] { 0x01, 0x81, 0x87, 0x7E };
            public int DutyMode;
        }

        public struct Envelope
        {
            public int Mode;
            public int InitialVolume;
            public int Volume;
            public int PeriodCounter;
            public int Period;
        }

        public struct Sweep
        {
            public int Counter;
            public int Period;
            public int Shift;
            public int Direction;
        }

        public struct Length
        {
            public int SoundLength;
            public int Consecutive;
        }

        #endregion
    }
}
