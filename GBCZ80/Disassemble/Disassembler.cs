﻿using GBCZ80.Decoding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GBCZ80.Disassemble
{
    public class Disassembler
    {
        public Z80CPU parent;

        public Disassembler(Z80CPU cpu)
        {
            parent = cpu;
        }


        public List<Instruction> Go()
        {
            List<Instruction> instructions = new List<Instruction>();

            List<int> scanPositions = new List<int>();
            List<int> nextScanPositions = new List<int>();
            scanPositions.Add(0x100);
            scanPositions.Add(0x00);
            scanPositions.Add(0x08);
            scanPositions.Add(0x10);
            scanPositions.Add(0x18);
            scanPositions.Add(0x20);
            scanPositions.Add(0x28);
            scanPositions.Add(0x30);
            scanPositions.Add(0x38);
            scanPositions.Add(0x40); //V-Blank Interrupt
            scanPositions.Add(0x48); //LCDC Status Interrupt
            scanPositions.Add(0x50); //Timer Overflow
            scanPositions.Add(0x58); //Serial Transfer Complete
            scanPositions.Add(0x60); //Button Interrupt

            do
            {
                Console.WriteLine("Scanning {0} Branches", scanPositions.Count);

                foreach (int position in scanPositions)
                {
                    int new_position = position;
                    byte opcode = parent.ram[new_position];
                    Instruction oc = new Instruction();
                    
                    if (opcode == 0xCB)
                    {
                        new_position++;
                        if (Decoder.CBOpcodes[opcode] == null) continue;
                        opcode = parent.ram[new_position];
                        oc.text = Decoder.CBOpcodes[opcode].name;
                        oc.opcodeParam = Decoder.CBOpcodes[opcode].opcodeParam;
                        oc.is_CB = true;
                    }
                    else
                    {
                        if (Decoder.Opcodes[opcode] == null) continue;
                        oc.text = Decoder.Opcodes[opcode].name;
                        oc.opcodeParam = Decoder.Opcodes[opcode].opcodeParam;
                        oc.is_CB = false;
                    }
                    oc.position = position;
                    new_position++;
                    switch (oc.opcodeParam)
                    {
                        case Opcode.Params._8_BIT:
                            int param_8 = parent.ram.ReadByte(new_position) & 0xFF;
                            new_position += 1;
                            oc.uint8_arg = (byte)param_8;
                            break;
                            //if (enableOutput) Console.WriteLine("Executing 0x{0:X}({3:X})\t\t From 0x{1:X}, {2}", opcode, cpu.state.PC - 2, oc.name, param_8);
                        case Opcode.Params._16_BIT:
                            int param_16 = parent.ram.ReadInt16(new_position) & 0xFFFF;
                            new_position += 2;
                            oc.uint16_arg = (short)param_16;
                            break;
                            //if (enableOutput) Console.WriteLine("Executing 0x{0:X}({3:X})\t From 0x{1:X}, {2}", opcode, cpu.state.PC - 3, oc.name, param_16);
                    }

                    if (oc.is_CB == false)
                    {
                        if (opcode == 0xC3 || opcode == 0xC2 || opcode == 0xCA || opcode == 0xD2 || opcode == 0xDA)
                        {
                            nextScanPositions.Add(oc.uint16_arg);
                        }
                        else if (opcode == 0xE9)
                        {
                            //Jump to (HL), can't decompile this
                        }
                        else if (opcode == 0x18 || opcode == 0x20 || opcode == 0x28 || opcode == 0x30 || opcode == 0x38)
                        {
                            nextScanPositions.Add((new_position + ((sbyte)oc.uint8_arg)) & 0xFFFF);
                        }
                        else if (opcode == 0xCD || opcode == 0xC4 || opcode == 0xCC || opcode == 0xD4 || opcode == 0xDC)
                        {
                            //nextScanPositions.Add(oc.uint16_arg);
                        }

                        if (opcode == 0xC9 || opcode == 0xC0 || opcode == 0xC8 || opcode == 0xD0 || opcode == 0xD8 || opcode == 0xD9)
                        {

                        }
                        else
                        {
                            nextScanPositions.Add(new_position);
                        }
                    }
                    else
                    {
                        nextScanPositions.Add(new_position);
                    }

                    instructions.Add(oc);
                }
                scanPositions.Clear();
                scanPositions.AddRange(nextScanPositions.Distinct().ToList());
                scanPositions.RemoveAll(e => instructions.Any(i => i.position == e));
                nextScanPositions.Clear();
            } while (scanPositions.Count != 0);

            return instructions.OrderBy(e => e.position).ToList();
        }
    }

    public class Instruction
    {
        public Instruction()
        {

        }

        public string text;
        public Opcode.Params opcodeParam;
        public bool is_CB = false;
        public byte uint8_arg;
        public short uint16_arg;
        public long position;
    }
}
