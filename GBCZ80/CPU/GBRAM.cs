﻿//#define MEMORY_STRICT
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.MemoryMappedFiles;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using GBCZ80.CartridgeTypes;
using System.IO;
using GBCZ80.Memory;

namespace GBCZ80
{
    public sealed unsafe class GBRAM
    {
        private readonly int MAX_MEMORY_SIZE = 0xFFFF;


        private UnmanagedArray memory;
        private UnmanagedArray staticROM;

        public GBRAM()
        {
            byte* rawMemory = (byte*)Marshal.AllocHGlobal(MAX_MEMORY_SIZE).ToPointer();
            memory = new UnmanagedArray(rawMemory,this,MAX_MEMORY_SIZE);
            //Cartridge = Cartridge.CreateEmpty(0x8000);
        }

        public void Dispose()
        {
            memory.Dispose();
            if(BankController != null)
                BankController.Dispose();
        }

        public void TestCratridge(Cartridge cart)
        {
            if (BankController == null)
            {
                BankController = new RAMOnly(cart);
            }
            Cartridge = cart;
        }

        public void LoadCartridge(Cartridge cart)
        {
            if (BankController != null)
                BankController.Dispose();

            Cartridge = cart;
            switch (cart.Header.CartType)
            {
                case CartridgeType.RomOnly:
                    BankController = new ROMOnly(cart);
                    break;

                case CartridgeType.Rom_MBC1:
                case CartridgeType.Rom_MBC1_RAM:
                    BankController = new MBC1(cart);
                    break;
                case CartridgeType.Rom_MBC2:
                    BankController = new MBC2(cart);
                    break;
                case CartridgeType.Rom_MBC3_RAM:
                    BankController = new MBC3(cart);
                    break;
                case CartridgeType.Rom_MBC5:
                case CartridgeType.Rom_MBC5_RAM:
                    BankController = new MBC5(cart);
                    break;

                //Battery Backed games (save file)
                case CartridgeType.Rom_MBC1_RAM_BATT:
                    BankController = new MBC1(cart, true);
                break;
                case CartridgeType.Rom_MBC2_BATT:
                    BankController = new MBC2(cart, true);
                    break;
                case CartridgeType.Rom_MBC3_RAM_BATT:
                    BankController = new MBC3(cart, true);
                    break;
                case CartridgeType.Rom_MBC5_RAM_BATT:
                    BankController = new MBC5(cart, true);
                    break;


                default:
                    throw new Exception(String.Format("Unsupported Cart Type, {0}", cart.Header.CartType));
            }
            
        }

        /// <summary></summary>
        /// <returns> True if operation sould be canceled or false if it is to procede</returns>
        private bool ProcessParameters(ref int position, out IMemory memoryToRefrence, int? writeValue = null)
        {
            bool cancelOperation = false;
            bool isWriting = writeValue.HasValue;

            memoryToRefrence = memory; // default to normal memory
            if (position < Offsets.VIDEO_RAM) //are we in Cartrage?
            {
                if (position < Offsets.SWITCHABLE_ROMBANK)
                {
                    if(staticROM == null) staticROM = new UnmanagedArray(Cartridge.GetRomBank(0),this,Cartridge.ROM_BANKSIZE);
                    memoryToRefrence = staticROM;
                }
                else
                {
                    memoryToRefrence = BankController.SelectedROMBank;// new UnmanagedArray(Cartridge.GetRomBank(BankController.ROMBank), this, Cartridge.ROM_BANKSIZE);
                    //position -= Offsets.SWITCHABLE_ROMBANK;
                }

                if (isWriting)
                    cancelOperation = BankController.PreprocessWrite(ref position, writeValue.Value);
                else
                    BankController.PreprocessRead(ref position);

            }
            else if (position < Offsets.SWITCHABLE_RAMBANK) //we in video ram?
            {
                //use default memory
            }
            else if (position < Offsets.INTERNAL_RAM_LOW) //are we in switchable ram
            {
                if (isWriting)
                    cancelOperation = BankController.PreprocessWrite(ref position, writeValue.Value);
                else
                    BankController.PreprocessRead(ref position);

                memoryToRefrence = BankController.SelectedRAMBank;
            }
            else if (position < Offsets.ECHO_RAM) // are we in internal ram?
            {

            }
            else if (position < Offsets.SPRITE_ATTRIBUTE) //are we in EchoRam
            {
                position -= 0x2000; //echo ram is echoing internal ram, lets just redirect them back to internal ram
            }
            else
            {

            }



            return cancelOperation;
        }

        /// <summary>
        /// Function does some consistancy checks to makesure doing an unsafe operation
        /// only active in debug mode.
        /// </summary>
        [System.Diagnostics.Conditional("DEBUG")]
        private void ProcessDebugInformation(int position,int operationSize,bool isWriteing = false)
        {
            if (BankController == null)
                throw new Exception("BankController should not be null, ensure that a cartridge is loaded");

            if(position < 0 || position > MAX_MEMORY_SIZE)
                throw new InvalidOperationException("Program is accessing memory beyond valid address at " + position);


            if (position < Offsets.VIDEO_RAM && position + operationSize > Offsets.VIDEO_RAM)
                throw new InvalidOperationException("Write or Read attempting to perform operation straddle ROM Bank and Video memory");

            if ((position < Offsets.INTERNAL_RAM_LOW && position + operationSize > Offsets.INTERNAL_RAM_LOW) ||
               (position < Offsets.SWITCHABLE_RAMBANK && position + operationSize > Offsets.SWITCHABLE_RAMBANK))
                throw new InvalidOperationException("Write or Read attempting to perform operation straddles RAM bank boundrys");



            #if MEMORY_STRICT
            if (position < Offsets.VIDEO_RAM && isWriteing)
                throw new InvalidOperationException("Program is writeing to ROM at location " + position);
            #endif
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteInt(int position,int value)
        {
            IMemory memoryToWrite;
            ProcessDebugInformation(position, sizeof(int),true);
            bool cancelWriteOp = ProcessParameters(ref position, out memoryToWrite,value);
            if (!cancelWriteOp)
            {
                memoryToWrite.Write(position, value);
            }
            else
            {

            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteShort(int position, short value)
        {
            IMemory memoryToWrite;
            ProcessDebugInformation(position, sizeof(short), true);
            bool cancelWriteOp = ProcessParameters(ref position, out memoryToWrite, value);
            if (!cancelWriteOp)
            {
                memoryToWrite.Write(position, value);
            }
            else
            {

            }
            
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool WriteByte(int position, byte value)
        {
            //if (position == 0) throw new Exception("WTF");
            IMemory memoryToWrite;

            ProcessDebugInformation(position, sizeof(byte), true);
            bool cancelWriteOp = ProcessParameters(ref position, out memoryToWrite, value);
            if (!cancelWriteOp)
                memoryToWrite.Write(position, value);
            return cancelWriteOp;
        }

        public int ReadOverheadTest(int position)
        {
            ProcessDebugInformation(position, sizeof(int));
            IMemory memoryToRead;
            ProcessParameters(ref position, out memoryToRead);
            return 0;
        }

        public bool WriteOverheadTest(int position, byte value)
        {
            //if (position == 0) throw new Exception("WTF");
            IMemory memoryToWrite;

            ProcessDebugInformation(position, sizeof(byte), true);
            bool cancelWriteOp = ProcessParameters(ref position, out memoryToWrite, value);
            return cancelWriteOp;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int ReadInt32(int position)
        {
            ProcessDebugInformation(position, sizeof(int));
            IMemory memoryToRead;
            ProcessParameters(ref position, out memoryToRead);
            return memoryToRead.ReadInt32(position);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public short ReadInt16(int position)
        {
            ProcessDebugInformation(position, sizeof(short));
            IMemory memoryToRead;
            ProcessParameters(ref position, out memoryToRead);
            return memoryToRead.ReadInt16(position);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public byte ReadByte(int position)
        {
            ProcessDebugInformation(position, sizeof(byte));
            IMemory memoryToRead;
            ProcessParameters(ref position, out memoryToRead);
            return memoryToRead.ReadByte(position);
        }

        public MemoryController BankController
        {
            get; set;
        }

        public Cartridge Cartridge { get; private set; }


        public byte this[int position]
        {
            get { return ReadByte(position); }
            set { WriteByte(position,value); } 
        }

        public static class Offsets
        {
            public const int ROMBANK0 = 0;
            public const int SWITCHABLE_ROMBANK = 0x4000;
            public const int VIDEO_RAM = 0x8000;
            public const int SWITCHABLE_RAMBANK = 0xA000;
            public const int INTERNAL_RAM_LOW = 0xC000;
            public const int ECHO_RAM = 0xE000;
            public const int SPRITE_ATTRIBUTE = 0xFE00;
            public const int EMPTY_IO_LOW = 0xFEA0; //needs beter name
            public const int IO_PORTS = 0xF000;
            public const int EMPTY_IO_HIGH = 0xFF4C; // also needs beter name
            public const int INTERNAL_RAM_HIGH = 0xFF80;
            public const int INTERUPT_ENABLE_REGISTER = 0xFFFF;
        }

        public static class IORegisters
        {
            public const int P1 = 0xFF00;
            public const int SB = 0xFF01;
            public const int SC = 0xFF02;
            public const int DIV = 0xFF04;
            public const int TIMA = 0xFF05;
            public const int TMA = 0xFF06;
            public const int TAC = 0xFF07;
            public const int IF = 0xFF0F;
            public const int NR10 = 0xFF10;
            public const int NR11 = 0xFF11;
            public const int NR12 = 0xFF12;
            public const int NR13 = 0xFF13;
            public const int NR14 = 0xFF14;
            public const int NR21 = 0xFF16;
            public const int NR22 = 0xFF17;
            public const int NR23 = 0xFF18;
            public const int NR24 = 0xFF19;
            public const int NR30 = 0xFF1A;
            public const int NR31 = 0xFF1B;
            public const int NR32 = 0xFF1C;
            public const int NR33 = 0xFF1D;
            public const int NR34 = 0xFF1E;
            public const int NR41 = 0xFF20;
            public const int NR42 = 0xFF21;
            public const int NR43 = 0xFF22;
            public const int NR44 = 0xFF23;
            public const int NR50 = 0xFF24;
            public const int NR51 = 0xFF25;
            public const int NR52 = 0xFF26;
            public const int WavePatternRam = 0xFF30;
            public const int LCDC = 0xFF40;
            public const int STAT = 0xFF41;
            public const int SCY = 0xFF42;
            public const int SCX = 0xFF43;
            public const int LY = 0xFF44;
            public const int LYC = 0xFF45;
            public const int DMA = 0xFF46;
            public const int BGP = 0xFF47;
            public const int OBP0 = 0xFF48;
            public const int OBP1 = 0xFF49;
            public const int WY = 0xFF4A;
            public const int WX = 0xFF4B;
            public const int IE = 0xFFFF;
        }
    }
}
