﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;
using System.Diagnostics;
using GBCZ80.Memory;
using GBCZ80.Decoding;
using System.Runtime.CompilerServices;
using GBCZ80.Audio;

namespace GBCZ80
{
    public class Z80CPU
    {
        public ProcessorState state;
        public GBRAM ram;
        public CPUInput keypad;
        public Cartridge loadedCart;

        public GameboyAudio audioSubsytem;

        public SoundManager soundManager;

        private long Cycles = 0;
        public volatile bool running = false;
        
        long lastTimerCycleFire = 0;
        public static readonly int CyclesPerSecond = 4194304;
        
        private int threadsWaiting = 0;

        private CycleWaiter[] waiters;

        public Z80CPU()
        {
            waiters = new CycleWaiter[2];
            ram = new GBRAM();
            keypad = new CPUInput();
        }

        public void Initialize(IntPtr hwnd)
        {

            //audioSubsytem = new GameboyAudio(this);
            if (hwnd != IntPtr.Zero)
            {
                soundManager = new SoundManager(hwnd, this);
            }
        }

        public void LoadROM(string fileName, bool withReset = true)
        {
            loadedCart = Cartridge.From(fileName);
            if (withReset)
                ResetCPU();
        }

        public void SetupBlankCart()
        {
            loadedCart = Cartridge.CreateEmpty(0xFFFF);
            ResetCPU(true);
        }

        public void ResetCPU(bool testing = false)
        {
            state.PC = 0x100;
            state.SP = 0xFFFE;

            //Initialization values as per bgb emulator
            state.AF = 0x01B0;
            state.BC = 0x0013;
            state.DE = 0x00D8;
            state.HL = 0x014D;

            //Initialization values as per manual pdf
            /*
            state.AF = 0x1180;
            state.BC = 0x0000;
            state.DE = 0xFF56;
            state.HL = 0x000F;
            */

            state.Halted = false;
            if (testing)
            {
                ram.TestCratridge(loadedCart);
            }
            else
            {
                ram.LoadCartridge(loadedCart);
            }

            ram.WriteByte(0xFF06, 0x00);
            ram.WriteByte(0xFF07, 0x00);
            ram.WriteByte(0xFF10, 0x80);
            ram.WriteByte(0xFF11, 0xBF);
            ram.WriteByte(0xFF12, 0xF3);
            ram.WriteByte(0xFF14, 0xBF);
            ram.WriteByte(0xFF16, 0x3F);
            ram.WriteByte(0xFF17, 0x00);
            ram.WriteByte(0xFF19, 0xBF);
            ram.WriteByte(0xFF1A, 0x7F);
            ram.WriteByte(0xFF1B, 0xFF);
            ram.WriteByte(0xFF1C, 0x9F);
            ram.WriteByte(0xFF1E, 0xBF);
            ram.WriteByte(0xFF20, 0xFF);
            ram.WriteByte(0xFF21, 0x00);
            ram.WriteByte(0xFF22, 0x00);
            ram.WriteByte(0xFF23, 0xBF);
            ram.WriteByte(0xFF24, 0x77);
            ram.WriteByte(0xFF25, 0xF3);
            ram.WriteByte(0xFF26, 0xF1);
            ram.WriteByte(0xFF40, 0x91);
            ram.WriteByte(0xFF42, 0x00);
            ram.WriteByte(0xFF43, 0x00);
            ram.WriteByte(0xFF45, 0x00);
            ram.WriteByte(0xFF47, 0xFC);
            ram.WriteByte(0xFF48, 0xFF);
            ram.WriteByte(0xFF49, 0xFF);
            ram.WriteByte(0xFF4A, 0x00);
            ram.WriteByte(0xFF4B, 0x00);
            ram.WriteByte(0xFF4D, 0x7E);
            ram.WriteByte(0xFFFF, 0x00);
        }

        public void StartRunner()
        {

            Thread cpu_thread = new Thread(Runner);
            cpu_thread.Name = "CPU Runner";
            cpu_thread.Priority = ThreadPriority.Highest;
            cpu_thread.IsBackground = true;
            cpu_thread.Start();

            var timerThread = new Thread(SpeedTimer);
            timerThread.Priority = ThreadPriority.Lowest;
            timerThread.Name = "Speed Timer";
            timerThread.IsBackground = true;
            timerThread.Start();

            soundManager.StartSampleThread();
        }

        public void Run(long UntilCycles, WaiterPosition position)
        {
            WaitForCycles(UntilCycles + Cycles, position);
        }

        public void RunUntil(long UntilCycles, WaiterPosition position)
        {
            WaitForCycles(UntilCycles, position);
        }

        public void Runner()
        {
            RunnerSmart(1, 0.1);
        }

        public void RunnerSmart(double speed, double stepTime = .050)
        {
            double ips = CyclesPerSecond * speed;
            long instructions_per_step = (long)(ips * stepTime);
            HighPerformanceTimer hpt = new HighPerformanceTimer();

            while (true)
            {
                hpt.Start();
                long startCycles = Cycles;
                while (this.Cycles - startCycles < instructions_per_step) //Target the instructions with this loop
                    RunSingleClocked();

                while (hpt.ElapsedSeconds < stepTime)
                    Thread.Yield();
            }
        }

        public void SpeedTimer()
        {
            HighPerformanceTimer hpt = new HighPerformanceTimer();
            long measureCycles = Cycles;

            while (true)
            {
                hpt.Start();
                measureCycles = Cycles;

                Thread.Sleep(1000);

                int ticksPerSecond = (int)((Cycles - measureCycles) / hpt.ElapsedSeconds);
                Console.WriteLine("Cycles Per Second: {0}, {1}% [{2}]", ticksPerSecond, (ticksPerSecond * 100) / CyclesPerSecond, SoundManager.SamplesWritten);
                SoundManager.SamplesWritten = 0;
            }
        }
        
        public int RunSingle()
        {
            byte value = ram.ReadByte(state.PC);
            state.PC++;
            return Decoder.Execute(value, this);
        }

        public void RunSingleClocked()
        {
            Cycles += RunSingle();
            
            if (Cycles % 256 == 0) //every 256th cycle, we increment this register;
                ram[0xFF04] = (byte)((ram[0xFF04] + 1) % 256);

            ram[GBRAM.IORegisters.P1] = keypad.getValue(ram[GBRAM.IORegisters.P1]);

            EvaluateTimer();
            EvaluateInterupt();
            CheckWaiters();
            //audioSubsytem.SoundUpdate(Cycles);
            soundManager.SoundUpdate(Cycles);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void CheckWaiters()
        {
            for (int i = waiters.Length - 1; i >= 0; i--)
            {
                CycleWaiter w = waiters[i];
                if (w != null && w.targetCycles <= this.Cycles)
                {
                    waiters[i] = null;
                    w.Trigger();
                    w = null;
                }
            }
        }

        public void WaitForCycles(long NewCycles, WaiterPosition position)
        {
            Interlocked.Increment(ref threadsWaiting);
            CycleWaiter w = new CycleWaiter(NewCycles);
            waiters[(int)position] = w;
            w.Wait();
            Interlocked.Decrement(ref threadsWaiting);
        }

        public void PushStackShort(ushort val)
        {
            short value = (short)val;
            byte a = (byte)((value >> 8) & 0xFF);
            byte b = (byte)(value & 0xFF);
            ram.WriteByte(--state.SP, a);
            ram.WriteByte(--state.SP, b);
            //Console.WriteLine("Pushing 0x{0:X} to stack 0x{1:X}", value, state.SP);
        }

        public void writeByte(int pos, byte val)
        {
            ram.WriteByte(pos, val);
        }

        // do note function does not allow interupt type of none
        public void SetInterupt(InteruptType newInteruptType)
        {
            if (state.InterupEnabled && (ram[GBRAM.IORegisters.IE] & (byte)newInteruptType) != 0)
                ram[GBRAM.IORegisters.IF] |= (byte)newInteruptType;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void EvaluateInterupt()
        {
            byte IF = ram[GBRAM.IORegisters.IF];
            byte IE = ram[GBRAM.IORegisters.IE];
            if ((IF & IE & 0x1F) == 0)
                return;

            ushort interuptAddress;
            int resetMask = 0x00;
            if ((IE & IF & (byte)InteruptType.VBlank) != 0) { interuptAddress = 0x40; resetMask = 0x01; }
            else if ((IE & IF & (byte)InteruptType.LCDCStatus) != 0) { interuptAddress = 0x48; resetMask = 0x02; }
            else if ((IE & IF & (byte)InteruptType.TimerOverflow) != 0) { interuptAddress = 0x50; resetMask = 0x04; }
            else if ((IE & IF & (byte)InteruptType.SerialTransfer) != 0) { interuptAddress = 0x58; resetMask = 0x08; }
            else if ((IE & IF & (byte)InteruptType.HiLowPin) != 0) { interuptAddress = 0x60; resetMask = 0x10; }
            else throw new InvalidOperationException("Invalid interuptType");

            if (state.InterupEnabled)
            {
                state.InterupEnabled = false;
                PushStackShort(state.PC);
                state.PC = interuptAddress;
                state.Halted = false;
                ram[GBRAM.IORegisters.IF] &= (byte)(~resetMask);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void EvaluateTimer()
        {
            //read TAC 
            byte TAC = ram[(int)GBRAM.IORegisters.TAC];
            bool enabled = (TAC & 0x4) == 0x4;

            int cpuFrequency = CyclesPerSecond;

            if (enabled)
            {
                int frequency;
                switch (TAC & 0x3) //first two bits are frequency type
                {
                    case 0x3: frequency = 16384; break;
                    case 0x2: frequency = 65536; break;
                    case 0x1: frequency = 262144; break;
                    case 0x0:
                    default:
                        frequency = 4096;
                    break;
                }

                int cycleMax = CyclesPerSecond / frequency;
                byte counter = ram[GBRAM.IORegisters.TIMA];

                if(Cycles - lastTimerCycleFire > cycleMax)
                {
                    lastTimerCycleFire = Cycles;
                    if (counter == 255)
                    {
                        counter = ram[GBRAM.IORegisters.TMA];
                        SetInterupt(InteruptType.TimerOverflow);
                    }
                    else counter++;
                }

                ram[GBRAM.IORegisters.TIMA] = counter;
            }
        }
        public enum InteruptType
        {
            None = 0x00,
            VBlank = 0x1,
            LCDCStatus = 0x2,
            TimerOverflow = 0x4,
            SerialTransfer = 0x8,
            HiLowPin = 0x10
        }


        [StructLayout(LayoutKind.Explicit, Pack = 1)]
        public struct ProcessorState
        {
            [FieldOffset(1)]
            public byte A;
            [FieldOffset(0)]
            public byte f;
            [FieldOffset(3)]
            public byte B;
            [FieldOffset(2)]
            public byte C;
            [FieldOffset(5)]
            public byte D;
            [FieldOffset(4)]
            public byte E;
            [FieldOffset(7)]
            public byte H;
            [FieldOffset(6)]
            public byte L;

            [FieldOffset(0)]
            public ushort AF;
            [FieldOffset(2)]
            public ushort BC;
            [FieldOffset(4)]
            public ushort DE;
            [FieldOffset(6)]
            public ushort HL;
            [FieldOffset(8)]
            public ushort SP;
            [FieldOffset(10)]
            public ushort PC;

            public bool Zero { get { return (F & (1 << 7)) != 0; } set { if (value) F = (byte)((1 << 7) | F); else F &= 0x7F; } }
            public bool Op { get { return (F & (1 << 6)) != 0; } set { if (value) F = (byte)((1 << 6) | F); else F &= 0xBF; } }
            public bool HalCar { get { return (F & (1 << 5)) != 0; } set { if (value) F = (byte)((1 << 5) | F); else F &= 0xDF; } }
            public bool Carry { get { return (F & (1 << 4)) != 0; } set { if (value) F = (byte)((1 << 4) | F); else F &= 0xEF; } }

            public byte F
            {
                get { return f; }
                set { f = (byte)(value & 0xF0); }
            }

            [FieldOffset(12)]
            public bool InterupEnabled;

            [FieldOffset(13)]
            public bool Halted;
        }

        public class CPUInput
        {
            public bool A;
            public bool B;
            public bool Start;
            public bool Select;
            public bool Up;
            public bool Down;
            public bool Left;
            public bool Right;

            public byte getValue(byte port)
            {
                byte value = port;
                bool P14 = (port & (1 << 4)) == 0;
                bool P15 = (port & (1 << 5)) == 0;
                value |= 0x0F;

                if (P14)
                {
                    if (Right)
                        value &= (byte)0xFE;
                    if (Left)
                        value &= (byte)0xFD;
                    if (Up)
                        value &= (byte)0xFB;
                    if (Down)
                        value &= (byte)0xF7;
                }
                if (P15)
                {
                    if (A)
                        value &= (byte)0xFE;
                    if (B)
                        value &= (byte)0xFD;
                    if (Select)
                        value &= (byte)0xFB;
                    if (Start)
                        value &= (byte)0xF7;
                }

                return value;
            }
        }

        class CycleWaiter
        {
            ManualResetEventSlim resetter;
            public long targetCycles;

            public CycleWaiter(long cycles)
            {
                targetCycles = cycles;
                resetter = new ManualResetEventSlim(false);
            }

            ~CycleWaiter()
            {
                resetter.Dispose();
            }

            public void Trigger()
            {
                resetter.Set();
            }

            public void Wait()
            {
                resetter.Wait();
            }
        }

        public enum WaiterPosition
        {
            Video = 0,
            Audio = 1,

        }
    }
}
