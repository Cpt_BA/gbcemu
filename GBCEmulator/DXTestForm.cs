﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GBCEmulator
{
    public partial class DXTestForm : Form
    {
        private RenderingSubsystem subsystem;
        public DXTestForm()
        {
            subsystem = new RenderingSubsystem(this.Handle);
            this.FormClosing += DXTestForm_FormClosing;
            InitializeComponent();
        }

        void DXTestForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Visible = false;
            }
            
        }

        public void Render()
        {
            subsystem.Render();
        }
    }
}
