﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading;
using SharpDX.Direct3D11;

using GBCZ80;
using GBCZ80.Graphics;

namespace GBCEmulator
{
    using SharpDX.DXGI;
    using Decoder = GBCZ80.Decoding.Decoder;
    using System.IO;
    using GBCZ80.Audio;

    public partial class Form1 : Form
    {
        Z80CPU cpu;
        Bitmap bmp;
        byte renderScx, renderScy;
        int[] memoryMap = new int[256 * 256];
        VideoManager vm;

        SoundManager sm;

        private RenderingSubsystem renderer;

        private SharpDX.DXGI.Surface texture;
        private ShaderResourceView view;
        private VideoRasterizer raster;

        private DXTestForm dxTestForm;

        public Form1()
        {
            //dxTestForm = new DXTestForm();
            bmp = new Bitmap(256, 256);
            InitializeComponent();

            cpu = new Z80CPU();
            cpu.Initialize(this.Handle);

            Decoder.Setup();
            Decoder.AttachCPU(cpu);
            //ROM ONLY
            //cpu.LoadROM("../../../Roms/ROMOnly/World Bowling.gb"); //Works quite well, little to no graphics issues
            //cpu.LoadROM("../../../Roms/ROMOnly/Testrom.gb");
            //cpu.LoadROM("../../../Roms/ROMOnly/3d.gb");
            //cpu.LoadROM("../../../Roms/ROMOnly/Runtime - Test Rom (PD).gb");
            //cpu.LoadROM("../../../Roms/ROMOnly/Asteroids.gb");  //Fully functional
            //cpu.LoadROM("../../../Roms/ROMOnly/175 Sprite Parallax Starfield Demo (PD) [C].gb"); //Seems to work pretty well, mostly just minor graphics stuff
            //cpu.LoadROM("../../../Roms/ROMOnly/Collision Detection 1.40 (PD).gb");
            //cpu.LoadROM("../../../Roms/ROMOnly/Tetris.gb"); //Playable, spawns only squares
            //cpu.LoadROM("../../../Roms/ROMOnly/PI.gb");

            //GBC Rom Only
            //cpu.LoadROM("../../../Roms/ROMOnly/99 Demo (PD) [C].gbc"); //Minor graphical glitching
            //cpu.LoadROM("../../../Roms/ROMOnly/40x24 Text Demo (PD) [C].gbc"); //Go home letters, your drunk-
            //cpu.LoadROM("../../../Roms/58-in-1 (Menu) (Unl) [C].gbc"); //Empty screen
            //cpu.LoadROM("../../../Roms/ROMOnly/2560 Colors Demo (PD) [C].gb");
            //cpu.LoadROM("../../../Roms/ROMOnly/water.gbc");
            //cpu.LoadROM("../../../Roms/ROMOnly/sonic.gbc");

            //MBC1
            //cpu.LoadROM("../../../Roms/128-in-1 Menu (1993) (Unl).gb"); //Mostly playable
            //cpu.LoadROM("../../../Roms/Bomberman GB (U) [S][!].gb");//Works pretty well
            //cpu.LoadROM("../../../Roms/chn-introc.gbc"); //Wat
            //cpu.LoadROM("../../../Roms/Super Mario 4.gb");
            //cpu.LoadROM("../../../Roms/Super Mario Land.gb");
            cpu.LoadROM("../../../Roms/Pokemon Trading Card Game.gb"); 
            //cpu.LoadROM("../../../Roms/Kirby's Dream Land.gb"); 
            //cpu.LoadROM("../../../Roms/Mega Man II.gb");
            //cpu.LoadROM("../../../Roms/Sound Test (PD).gb");
            //cpu.LoadROM("../../../Roms/ROMOnly/MySoundTest.gb");
            
            //MBC1 RAM BATTERY
            //cpu.LoadROM("../../../Roms/Zelda - Link's Awakening.gb"); //Runs

            //MBC3
            //cpu.LoadROM("../../../Roms/Pokemon Red.gb"); //holy shit it works, only some minor graphical issues

            //MBC5
            //cpu.LoadROM("../../../Roms/X-Men - Mutant Academy (J) [C][!].gbc");//Waiting for LCDC to indicate OAM transfer
            //cpu.LoadROM("../../../Roms/Grand Theft Auto 2.gbc");
            //cpu.LoadROM("../../../Roms/1942.gbc"); //Small Graphical glitching, but all functionally there

            //MBC5 RAM BATTERY
            //cpu.LoadROM("../../../Roms/Mario Tennis (U) [C][!].gbc"); //Overwrites memory
            //cpu.LoadROM("../../../Roms/Pocket Bomberman.gbc"); //Runs but sprite table does not appear to load properly

            /*Matches VBA*/
            //cpu.LoadROM("../../../Roms/Tests/cpu_instrs/individual/01-special.gb");
            //cpu.LoadROM("../../../Roms/Tests/cpu_instrs/individual/02-interrupts.gb");
            //cpu.LoadROM("../../../Roms/Tests/cpu_instrs/individual/03-op sp,hl.gb");
            /*PASSES*/
            //cpu.LoadROM("../../../Roms/Tests/cpu_instrs/individual/04-op r,imm.gb");
            /*PASSES*/
            //cpu.LoadROM("../../../Roms/Tests/cpu_instrs/individual/05-op rp.gb");
            /*PASSES*/
            //cpu.LoadROM("../../../Roms/Tests/cpu_instrs/individual/06-ld r,r.gb");
            /*PASSES*/
            //cpu.LoadROM("../../../Roms/Tests/cpu_instrs/individual/07-jr,jp,call,ret,rst.gb");
            //cpu.LoadROM("../../../Roms/Tests/cpu_instrs/individual/08-misc instrs.gb");
            /*PASSES*/
            //cpu.LoadROM("../../../Roms/Tests/cpu_instrs/individual/09-op r,r.gb");
            /*PASSES*/
            //cpu.LoadROM("../../../Roms/Tests/cpu_instrs/individual/10-bit ops.gb");
            /*PASSES*/
            //cpu.LoadROM("../../../Roms/Tests/cpu_instrs/individual/11-op a,(hl).gb");

            //cpu.LoadROM("../../../Roms/Tests/mem_timing/individual/01-read_timing.gb");

            //cpu.LoadROM("../../../Roms/Tests/cpu_instrs/cpu_instrs.gb");
            //cpu.LoadROM("../../../Roms/Tests/instr_timing/instr_timing.gb");
            //cpu.LoadROM("../../../Roms/Tests/mem_timing/mem_timing.gb");


            //raster = new VideoRasterizer(cpu);

            //StreamPlayer audio = new StreamPlayer(new GBCZ80.Audio.EmulatedAudioStream(cpu.audioSubsytem));
            //audio.InitializeAudioSource();
            //audio.Play();

            renderer = new RenderingSubsystem(panel1.Handle);
            texture = renderer.InitFrontend(out view);

            vm = new VideoManager(cpu);
            vm.Rendered += disp_Rendered;
            cpu.StartRunner();
        }

        public void Render()
        {
            lock (renderer)
            {
                renderer.Render(view);
            }

            if(dxTestForm != null && dxTestForm.Visible)
            {
                dxTestForm.Render();
            }
        }
        void disp_Rendered(object sender, int[] screenBuffer, byte scx, byte scy)
        {
            Render();

            lock (renderer)
            {
                unsafe
                {
                    var rectangle = texture.Map(MapFlags.Discard | MapFlags.Write);
                    int subPitch = rectangle.Pitch / 4;

                    renderScx = scx;
                    renderScy = scy;

                    uint* buffer = (uint*)rectangle.DataPointer.ToPointer();
                    Parallel.For(0, 160, (x) =>
                    {
                        for (int y = 0; y < 144; y++)
                        {
                            buffer[(subPitch * y) + x] = (uint)((long)0xFF000000 | screenBuffer[((x + renderScx) % 256) + (((y + renderScy) % 256) * 256)]);
                        }
                    });
                }

                texture.Unmap();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cpu.SetInterupt(Z80CPU.InteruptType.VBlank);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            cpu.SetInterupt(Z80CPU.InteruptType.LCDCStatus);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            cpu.SetInterupt(Z80CPU.InteruptType.TimerOverflow);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Decoder.enableOutput = true;

            dxTestForm.Visible = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //while (cpu.running) ;
            //cpu.ram.WriteByte(0xC01A, 0x28);
            for (int i = 0; i < 256; i++)
            {
                if (Decoder.OpcodeCounts[i] != 0)
                    Console.WriteLine("{0:X2} => {1}\t\t({2})", i, Decoder.OpcodeCounts[i], Decoder.Opcodes[i].name);
            }
            for (int i = 0; i < 256; i++)
            {
                if (Decoder.CBOpcodeCounts[i] != 0)
                    Console.WriteLine("{0:X2} => {1}\t\t({2})[CB]", i, Decoder.CBOpcodeCounts[i], Decoder.CBOpcodes[i].name);
            }
        }

        private void panel1_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.W: cpu.keypad.Up = false; break;
                case Keys.S: cpu.keypad.Down = false; break;
                case Keys.A: cpu.keypad.Left = false; break;
                case Keys.D: cpu.keypad.Right = false; break;

                case Keys.H: cpu.keypad.A = false; break;
                case Keys.J: cpu.keypad.B = false; break;
                case Keys.K: cpu.keypad.Select = false; break;
                case Keys.L: cpu.keypad.Start = false; break;
            }
        }

        private void panel1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.W: cpu.keypad.Up = true; break;
                case Keys.S: cpu.keypad.Down = true; break;
                case Keys.A: cpu.keypad.Left = true; break;
                case Keys.D: cpu.keypad.Right = true; break;

                case Keys.H: cpu.keypad.A = true; break;
                case Keys.J: cpu.keypad.B = true; break;
                case Keys.K: cpu.keypad.Select = true; break;
                case Keys.L: cpu.keypad.Start = true; break;
                case Keys.R:
                    cpu.ResetCPU();
                    break;
            }
        }

        private void saveTimer_Tick(object sender, EventArgs e)
        {
            if (cpu != null && cpu.ram.BankController != null && cpu.ram.BankController.usesSave && cpu.ram.BankController.saveQueued && cpu.loadedCart.GetSaveFileLocation() != null)
            {
                unsafe
                {
                    byte* data = cpu.ram.BankController.GetRAMBank(0);
                    byte[] dataToWrite = new byte[0x4000];
                    for (int i = 0; i < 0x4000; i++)
                        dataToWrite[i] = data[i];
                    File.WriteAllBytes(cpu.loadedCart.GetSaveFileLocation(), dataToWrite);
                    cpu.ram.BankController.saveQueued = false;
                    Console.WriteLine("Saved");
                }
            }
        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            //vm.layer0Enabled = checkBox8.Checked;
            //vm.layer1Enabled = checkBox9.Checked;
            //vm.layer2Enabled = checkBox10.Checked;
            //vm.layer3Enabled = checkBox11.Checked;
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            new Disassembler(cpu).Show();
        }

        private void checkBox12_CheckedChanged(object sender, EventArgs e)
        {
            SoundManager._chan1.ProduceAudio = checkBox12.Checked;
            SoundManager._chan2.ProduceAudio = checkBox13.Checked;
            SoundManager._chan3.ProduceAudio = checkBox14.Checked;
        }
    }
}
