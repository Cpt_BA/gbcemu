﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.D3DCompiler;

using SharpDX;
using D2 = SharpDX.Direct2D1;
using SharpDX.DXGI;
using SharpDX.Direct3D;
using SharpDX.Toolkit.Graphics;
using GBCZ80.Graphics;

namespace GBCEmulator
{
    using SharpDX.Direct3D11;
    public class RenderingSubsystem : IDisposable
    {
        public Device device;
        private SwapChain chain;
        private DeviceContext context;
        private GraphicsDevice gDevice;

        private RenderTargetView surface;
        private PixelShader emulatorShader;


        private IntPtr outputHandle;

        int? desiredWidth;
        int? desiredHeight;

        private D2.Factory d2dSubsystem;
        D2.SolidColorBrush redBrush;

        public ShaderResourceView testTexture;
        SpriteBatch batch;
        Quad quadable;
        
        public RenderingSubsystem(IntPtr outputHandle, int? width = null, int? height = null)
        {
            quadable = new Quad();
            Initalize(outputHandle, width, height);
        }

        private void Initalize(IntPtr outputHandle, int? widthOverride = null, int? heightOverride = null)
        {
            this.outputHandle = outputHandle;
            var outCtrl = System.Windows.Forms.Control.FromHandle(outputHandle);
            desiredWidth = widthOverride;
            desiredHeight = heightOverride;

            int width = desiredWidth ?? outCtrl.ClientSize.Width;
            int height = desiredHeight ?? outCtrl.ClientSize.Height;


            var description = new SwapChainDescription()
            {
                BufferCount = 2,
                IsWindowed = true,
                OutputHandle = outputHandle,
                SwapEffect= SwapEffect.Discard,
                ModeDescription = new ModeDescription(width, height, refreshRate: new Rational(60, 1), format: Format.B8G8R8A8_UNorm),
                Usage = Usage.RenderTargetOutput,
                SampleDescription = new SampleDescription(1,0) //hopefuly this gives some decent default settings? dont care about this
            };
          
            Device.CreateWithSwapChain(DriverType.Hardware, DeviceCreationFlags.BgraSupport |  DeviceCreationFlags.Debug | DeviceCreationFlags.SingleThreaded, description, out device, out chain);
            context = device.ImmediateContext;

            gDevice = GraphicsDevice.New(device);
            batch = new SpriteBatch(gDevice);

            var rasterDescription = new RasterizerStateDescription()
            {
                IsAntialiasedLineEnabled = false,
                CullMode = CullMode.Back,
                DepthBias = 0,
                DepthBiasClamp = .0f,
                IsDepthClipEnabled = false,
                FillMode = FillMode.Solid,
                IsFrontCounterClockwise = false,
                IsMultisampleEnabled = false,
                IsScissorEnabled = false,
                SlopeScaledDepthBias = .0f
            };

            var rasterizer = new RasterizerState(device,rasterDescription);

            context.Rasterizer.State = rasterizer;
            context.Rasterizer.SetViewport(0, 0, width, height, 0, 1);
            //testTexture = ShaderResourceView.FromFile(device, @"C:\Users\Andy\Desktop\GrassTexture.jpg");
            //testTexture = resource.QueryInterface<ShaderResourceView>();
            d2dSubsystem = new D2.Factory(D2.FactoryType.SingleThreaded, D2.DebugLevel.Error);

            var compilation = SharpDX.D3DCompiler.ShaderBytecode.CompileFromFile(@"Graphics\PalletShader.hlsl","ps_main", "ps_4_0", ShaderFlags.EnableBackwardsCompatibility | ShaderFlags.Debug);
            if(compilation.HasErrors)
            {
                throw new Exception("ERROR SHADER");
            }
            string msg = compilation.Message;
            emulatorShader = new PixelShader(device, compilation);

            compilation = SharpDX.D3DCompiler.ShaderBytecode.CompileFromFile(@"Graphics\VertexShader.hlsl","vs_main", "vs_4_0", ShaderFlags.EnableBackwardsCompatibility | ShaderFlags.Debug);
            if(compilation.HasErrors)
            {
                throw new Exception("ERROR SHADER");
            }
            var vertShader = new VertexShader(device, compilation);

            //var testEffect = new Effect(gDevice, compilation);
            //little bit of duplicated work here but watever.

            quadable.Initalize(device, vertShader, emulatorShader,compilation);
            OnResize();

            Projection = Matrix.PerspectiveFovLH((float)(Math.PI / 4), (float)(width / height), 1.0f, 1000);
        }

        public void Dispose()
        {
            Utilities.Dispose(ref surface);
            Utilities.Dispose(ref chain);
            Utilities.Dispose(ref context);
            Utilities.Dispose(ref device);
        }

        private void OnResize()
        {
            Utilities.Dispose(ref surface);

            var outCtrl = System.Windows.Forms.Control.FromHandle(outputHandle);

            int width = desiredWidth ?? outCtrl.ClientSize.Width;
            int height = desiredHeight ?? outCtrl.ClientSize.Height;

            chain.ResizeBuffers(0, width, height, Format.Unknown, SwapChainFlags.None);

            var backBuffer = Texture2D.FromSwapChain<Texture2D>(chain, 0);
            surface = new RenderTargetView(device, backBuffer);

            context.OutputMerger.SetTargets(surface);
        }



        public void Render(ShaderResourceView texture = null)
        {
            context.ClearRenderTargetView(surface, Color.CornflowerBlue.ToColor4());
            quadable.Projection = Matrix.OrthoRH(1,1,1.0f,500);
            quadable.View = Matrix.Identity;//Matrix.LookAtRH(new Vector3(0, -10, 0), new Vector3(), new Vector3(0, 0, 1));
            quadable.SetShaderParameters(context, texture);

            quadable.Render(context);

           // var d2TargetProps = new D2.RenderTargetProperties()
           // {
           //     PixelFormat = new D2.PixelFormat(Format.Unknown, D2.AlphaMode.Premultiplied),
           //     Type = D2.RenderTargetType.Default,
           //     DpiX = d2dSubsystem.DesktopDpi.Width,
           //     DpiY = d2dSubsystem.DesktopDpi.Height,
           // };


          
           // var s = Surface.FromSwapChain(chain, 0);
           // var direct = new D2.RenderTarget(d2dSubsystem, s, d2TargetProps);

           // var brush = new D2.SolidColorBrush(direct, Color.Red.ToColor4());
           // direct.BeginDraw();
           //// direct.FillRectangle(new RectangleF(0, 0, 500, 500), brush);
           // direct.EndDraw();

         


           // brush.Dispose();
           // direct.Dispose();


            //gDevice.Clear(Color.CornflowerBlue);
            //batch.Begin();
            //batch.Draw(testTexture, new Vector2(0,0), Color.Red);
            //batch.End();
            //gDevice.Present();
            chain.Present(0,PresentFlags.None); 
        }


        public Surface CreateLayer()
        {
            var description = new Texture2DDescription()
            {
                Width = 512, //size to change
                Height = 512,
                Usage = ResourceUsage.Dynamic,
                OptionFlags= ResourceOptionFlags.None,
                SampleDescription = new SampleDescription(1,0),
                Format = Format.R8_SNorm //one channel supported, so we write pallet colors, the shader will fill in the real color
            };
         
            var layerTexture = new Texture2D(device, description);
          
            return layerTexture.QueryInterface<Surface>();

        }

        public Surface InitFrontend(out ShaderResourceView view)
        {
            var description = new Texture2DDescription()
            {
                Width = 160, //size to change
                Height = 144,
                Usage = ResourceUsage.Dynamic,
                OptionFlags = ResourceOptionFlags.None,
                SampleDescription = new SampleDescription(1, 0),
                BindFlags = SharpDX.Direct3D11.BindFlags.ShaderResource,
                CpuAccessFlags = SharpDX.Direct3D11.CpuAccessFlags.Write,
                ArraySize = 1,
                MipLevels = 1,
                Format = Format.R8G8B8A8_UNorm //one channel supported, so we write pallet colors, the shader will fill in the real color
            };

            var tex = new Texture2D(device, description);
            Surface texture = tex.QueryInterface<Surface>();
            view = new ShaderResourceView(device, tex);

            return texture;
        }

        public Matrix Projection { get; set; }
        public Matrix World { get; set; }
        public Matrix View { get; set; }
    }
}
