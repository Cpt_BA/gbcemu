﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlimDX;
using SlimDX.DirectSound;
using SlimDX.Multimedia;
using System.Diagnostics;
using System.Threading;

namespace SoundTester
{
    class Program
    {
        static DirectSound ds;
        static WaveFormat format;
        static SoundBufferDescription description;
        static SecondarySoundBuffer DSoundBuffer;
        static short bits = 8;
        static short channels = 2;
        static int byetsPerSample = channels * (bits / 8);

        static int bufferSize = 4410 * byetsPerSample;

        static int soundOffset = 0;
        static void Main(string[] args)
        {
            byte[] SoundBuffer = new byte[bufferSize];

            //Console.Write("Enter Frequency: ");
            int freq = 1000;// int.Parse(Console.ReadLine());

            var devices = DirectSound.GetDevices();
            DeviceInformation outputDevice = devices.First(d => d.Description.StartsWith("CABLE"));

            ds = new DirectSound(outputDevice.DriverGuid);

            ds.SetCooperativeLevel(Process.GetCurrentProcess().MainWindowHandle , CooperativeLevel.Priority);

            format = new WaveFormat
            {
                SamplesPerSecond = 44100,
                BitsPerSample = bits,
                Channels = channels,
                FormatTag = WaveFormatTag.Pcm,
                BlockAlignment = (short)(byetsPerSample)
            };
            format.AverageBytesPerSecond = format.SamplesPerSecond * format.Channels * (format.BitsPerSample / 8);


            description = new SoundBufferDescription
            {
                Format = format,
                Flags =
                    BufferFlags.GlobalFocus |
                    BufferFlags.Software |
                    BufferFlags.GetCurrentPosition2 |
                    BufferFlags.ControlVolume,
                SizeInBytes = bufferSize
            };
            DSoundBuffer = new SecondarySoundBuffer(ds, description);
            //DSoundBuffer.Volume = 10;
            
            DSoundBuffer.Write(SoundBuffer, 0, LockFlags.FromWriteCursor);
            DSoundBuffer.CurrentPlayPosition = 0;
            DSoundBuffer.Play(0, PlayFlags.Looping);

            long counter = 0;
            //int samples = SoundBuffer.Length / (channels * (bits / 8));
            int samplePosition = 0;
            bool reversed = false;
            
            while (true)
            {
                int availableBytes = GetAudioSpace(samplePosition);
                if (availableBytes > 0)
                {
                    int samplesToGenerate = availableBytes / byetsPerSample;
                    for (int i = 0; i < samplesToGenerate; i++)
                    {
                        byte sample = Frequency(freq, counter);
                        /*for (int j = 0; j < channels; j++)
                        {
                            SoundBuffer[(i * channels) + j] = (byte)(counter % 256);
                        }*/

                        SoundBuffer[samplePosition] = sample;// (byte)(counter % 256);
                        SoundBuffer[samplePosition + 1] = (byte)(counter / 128);

                        counter++;
                        samplePosition = (samplePosition + byetsPerSample) % SoundBuffer.Length;
                        
                        freq += reversed ? -1 : 1;

                        if (freq > 5000 || freq < 1000)
                            reversed = !reversed;
                    }

                    DSoundBuffer.Write(SoundBuffer, 0, LockFlags.EntireBuffer);
                }
            }
        }

        static byte Frequency(double frequency, long counter)
        {
            double samplesPerHZ = format.SamplesPerSecond / frequency;
            double angle = ((counter % samplesPerHZ) / samplesPerHZ) * Math.PI * 2.0;

            return (byte)(128 + (Math.Sin(angle) * 128));
        }

        static int circularDist(int from, int to, int size)
        {
            if (size == 0)
                return 0;
            int diff = (to - from);
            while (diff < 0)
                diff += size;
            return diff;
        }

        static int GetAudioSpace(int position)
        {
            if (DSoundBuffer == null) return 0;

            int playcursor = DSoundBuffer.CurrentPlayPosition;
            return circularDist(position, playcursor, bufferSize);
        }

    }
}
