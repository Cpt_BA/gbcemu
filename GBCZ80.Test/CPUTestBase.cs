﻿using GBCZ80.Decoding;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GBCZ80.Test
{
    [TestClass]
    public class CPUTestBase
    {
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            Decoder.Setup();
        }

        public CPUTestBase()
        {
            
        }

        public Z80CPU GetCPU()
        {
            Z80CPU cpu = new Z80CPU();

            cpu.Initialize(IntPtr.Zero);
            cpu.SetupBlankCart();
            return cpu;
        }

        public void SetupCPU(Z80CPU cpu, params byte[] code)
        {
            Decoder.AttachCPU(cpu);
            cpu.ResetCPU(true);

            cpu.ram[0x100] = 0xC3;
            cpu.ram[0x101] = 0x50;
            cpu.ram[0x102] = 0x01;

            cpu.ram[0x134] = (byte)'T';
            cpu.ram[0x135] = (byte)'E';
            cpu.ram[0x136] = (byte)'S';
            cpu.ram[0x137] = (byte)'T';
            cpu.ram[0x138] = 0x00;

            for (int i = 0; i < code.Length; i++)
            {
                cpu.ram[0x150 + i] = code[i];
            }
        }
    }
}
