﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GBCZ80.Decoding;

namespace GBCZ80.Test
{
    [TestClass]
    public class ALUTest : CPUTestBase
    {
        [TestMethod]
        public void AddFlags()
        {
            var cpu = GetCPU();

            for (int i = 0; i < byte.MaxValue; i += 1)
            {
                for (int j = 0; j < byte.MaxValue; j += 1)
                {
                    SetupCPU(cpu, 0x3E, (byte)i, 0xc6, (byte)j);

                    cpu.RunSingle(); //JMP @ 0x100
                    cpu.RunSingle(); //LD i
                    cpu.RunSingle(); //ADD j

                    string testName = string.Format("ADD 0x{0:X}, 0x{1:X} ", i, j);

                    int fullSum = i + j;
                    byte sum = (byte)(fullSum);

                    bool shouldCarry = (fullSum & 0x100) == 0x100;
                    bool shouldHalfCarry = ((i & 0x0F) + (j & 0x0F)) > 0x0F;

                    Assert.AreEqual(sum, cpu.state.A, testName);

                    Assert.AreEqual(sum == 0, cpu.state.Zero, testName + "Zero flag incorrect.");
                    Assert.AreEqual(shouldCarry, cpu.state.Carry, testName + "CPU Carry flag incorrect.");
                    Assert.AreEqual(shouldHalfCarry, cpu.state.HalCar, testName + "CPU halfCarry flag incorrect.");
                }
            }
        }

        [TestMethod]
        public void AddWithCarryFlags()
        {
            var cpu = GetCPU();

            for (int carry = 0; carry <= 1; carry++)
            {
                for (int i = 0; i < byte.MaxValue; i += 1)
                {
                    for (int j = 0; j < byte.MaxValue; j += 1)
                    {
                        byte flagRegister = (byte)(carry << 4); //Carry flag is bit #4

                        SetupCPU(cpu, 0x01, flagRegister, (byte)i, 0xC5, 0xF1, 0xCE, (byte)j);

                        cpu.RunSingle(); //JMP @ 0x100
                        cpu.RunSingle(); //LD BC, 0x0cii [FFAA]
                        cpu.RunSingle(); //PUSH BC
                        cpu.RunSingle(); //POP AF
                        cpu.RunSingle(); //ADC j

                        string testName = string.Format("ADC 0x{0:X}, 0x{1:X} ", i, j);
                        byte operand = (byte)(j + carry);

                        int fullSum = i + operand;
                        byte sum = (byte)(fullSum);

                        bool shouldCarry = (fullSum & 0x100) == 0x100;
                        bool shouldHalfCarry = ((i & 0x0F) + (operand & 0x0F)) > 0x0F;

                        Assert.AreEqual(sum, cpu.state.A, testName);

                        Assert.AreEqual(sum == 0, cpu.state.Zero, testName + "Zero flag incorrect.");
                        Assert.AreEqual(shouldCarry, cpu.state.Carry, testName + "CPU Carry flag incorrect.");
                        Assert.AreEqual(shouldHalfCarry, cpu.state.HalCar, testName + "CPU halfCarry flag incorrect.");
                    }
                }
            }
        }

        [TestMethod]
        public void SubtractFlags()
        {
            var cpu = GetCPU();

            for (int i = 0; i < byte.MaxValue; i += 1)
            {
                for (int j = 0; j < byte.MaxValue; j += 1)
                {
                    SetupCPU(cpu, 0x3E, (byte)i, 0xD6, (byte)j);

                    cpu.RunSingle(); //JMP @ 0x100
                    cpu.RunSingle(); //LD i
                    cpu.RunSingle(); //SUB j

                    string testName = string.Format("SUB 0x{0:X}, 0x{1:X} ", i, j);

                    int fullSum = i - j;
                    byte sum = (byte)(fullSum);

                    bool halfBitBorrow = (i & 0x0F) < (j & 0x0F);
                    bool bitBorrow = i < j;

                    Assert.AreEqual(sum, cpu.state.A, testName);

                    Assert.AreEqual(sum == 0, cpu.state.Zero, testName + "Zero flag should be set" + testName);
                    Assert.AreEqual(!bitBorrow, cpu.state.Carry, "CPU Carry flag incorrect." + testName);
                    Assert.AreEqual(!halfBitBorrow, cpu.state.HalCar, "CPU halfCarry flag incorrect." + testName);
                    Assert.IsTrue(cpu.state.Op);
                }
            }
        }

        [TestMethod]
        public void SubtractWithCarryFlags()
        {
            var cpu = GetCPU();

            for (int carry = 0; carry <= 1; carry++)
            {
                for (int i = 0; i < byte.MaxValue; i += 1)
                {
                    for (int j = 0; j < byte.MaxValue; j += 1)
                    {
                        byte flagRegister = (byte)(carry << 4); //Carry flag is bit #4

                        SetupCPU(cpu, 0x01, flagRegister, (byte)i, 0xC5, 0xF1, 0x06, (byte)j, 0x98);

                        cpu.RunSingle(); //JMP @ 0x100
                        cpu.RunSingle(); //LD BC, 0x0cii [FFAA]
                        cpu.RunSingle(); //PUSH BC
                        cpu.RunSingle(); //POP AF
                        cpu.RunSingle(); //LD B, j
                        cpu.RunSingle(); //SBC B

                        string testName = string.Format("SBC 0x{0:X}, 0x{1:X} ", i, j);
                        byte operand = (byte)(j + carry);

                        int fullSum = i - operand;
                        byte sum = (byte)(fullSum);

                        bool halfBitBorrow = (i & 0x0F) < (operand & 0x0F);
                        bool bitBorrow = i < operand;

                        Assert.AreEqual(sum, cpu.state.A, testName);

                        Assert.AreEqual(sum == 0, cpu.state.Zero, testName + "Zero flag incorrect." + testName);
                        Assert.AreEqual(!bitBorrow, cpu.state.Carry, "CPU Carry flag incorrect." + testName);
                        Assert.AreEqual(!halfBitBorrow, cpu.state.HalCar, "CPU halfCarry flag incorrect." + testName);
                        Assert.IsTrue(cpu.state.Op);
                    }
                }
            }
        }

        [TestMethod]
        public void AndFlags()
        {
            var cpu = GetCPU();

            for (int i = 0; i < byte.MaxValue; i += 1)
            {
                for (int j = 0; j < byte.MaxValue; j += 1)
                {
                    SetupCPU(cpu, 0x3E, (byte)i, 0xE6, (byte)j);

                    cpu.RunSingle(); //JMP @ 0x100
                    cpu.RunSingle(); //LD i
                    cpu.RunSingle(); //AND j

                    string testName = string.Format("AND 0x{0:X}, 0x{1:X} ", i, j);

                    byte result = (byte)(i & j);

                    Assert.AreEqual(result, cpu.state.A, testName);

                    Assert.AreEqual(result == 0, cpu.state.Zero, testName + "Zero flag incorrect.");
                    Assert.IsFalse(cpu.state.Carry, testName + "CPU Carry flag incorrect.");
                    Assert.IsTrue(cpu.state.HalCar, testName + "CPU halfCarry flag incorrect.");
                    Assert.IsFalse(cpu.state.Op, testName + "CPU Op flag incorrect.");
                }
            }
        }

        [TestMethod]
        public void OrFlags()
        {
            var cpu = GetCPU();

            for (int i = 0; i < byte.MaxValue; i += 1)
            {
                for (int j = 0; j < byte.MaxValue; j += 1)
                {
                    SetupCPU(cpu, 0x3E, (byte)i, 0xF6, (byte)j);

                    cpu.RunSingle(); //JMP @ 0x100
                    cpu.RunSingle(); //LD i
                    cpu.RunSingle(); //OR j

                    string testName = string.Format("OR 0x{0:X}, 0x{1:X} ", i, j);

                    byte result = (byte)(i | j);

                    Assert.AreEqual(result, cpu.state.A, testName);

                    Assert.AreEqual(result == 0, cpu.state.Zero, testName + "Zero flag incorrect.");
                    Assert.IsFalse(cpu.state.Carry, testName + "CPU Carry flag incorrect.");
                    Assert.IsFalse(cpu.state.HalCar, testName + "CPU halfCarry flag incorrect.");
                    Assert.IsFalse(cpu.state.Op, testName + "CPU Op flag incorrect.");
                }
            }
        }

        [TestMethod]
        public void XorFlags()
        {
            var cpu = GetCPU();

            for (int i = 0; i < byte.MaxValue; i += 1)
            {
                for (int j = 0; j < byte.MaxValue; j += 1)
                {
                    SetupCPU(cpu, 0x3E, (byte)i, 0xEE, (byte)j);

                    cpu.RunSingle(); //JMP @ 0x100
                    cpu.RunSingle(); //LD i
                    cpu.RunSingle(); //AND j

                    string testName = string.Format("XOR 0x{0:X}, 0x{1:X} ", i, j);

                    byte result = (byte)(i ^ j);

                    Assert.AreEqual(result, cpu.state.A, testName);

                    Assert.AreEqual(result == 0, cpu.state.Zero, testName + "Zero flag incorrect.");
                    Assert.IsFalse(cpu.state.Carry, testName + "CPU Carry flag incorrect.");
                    Assert.IsFalse(cpu.state.HalCar, testName + "CPU halfCarry flag incorrect.");
                    Assert.IsFalse(cpu.state.Op, testName + "CPU Op flag incorrect.");
                }
            }
        }

        [TestMethod]
        public void CpFlags()
        {
            var cpu = GetCPU();

            for (int i = 0; i < byte.MaxValue; i += 1)
            {
                for (int j = 0; j < byte.MaxValue; j += 1)
                {
                    SetupCPU(cpu, 0x3E, (byte)i, 0xFE, (byte)j);

                    cpu.RunSingle(); //JMP @ 0x100
                    cpu.RunSingle(); //LD i
                    cpu.RunSingle(); //CP j

                    string testName = string.Format("CP 0x{0:X}, 0x{1:X} ", i, j);

                    bool halfBitBorrow = (i & 0x0F) < (j & 0x0F);

                    Assert.AreEqual(i, cpu.state.A, testName);

                    Assert.AreEqual(i - j == 0, cpu.state.Zero, testName + "Zero flag incorrect.");
                    Assert.AreEqual(!(i < j), cpu.state.Carry, testName + "CPU Carry flag incorrect.");
                    Assert.AreEqual(!halfBitBorrow, cpu.state.HalCar, testName + "CPU halfCarry flag incorrect.");
                    Assert.IsTrue(cpu.state.Op, testName + "CPU Op flag incorrect.");
                }
            }
        }

        [TestMethod]
        public void IncFlags()
        {
            var cpu = GetCPU();

            for (int i = 0; i < byte.MaxValue; i += 1)
            {
                SetupCPU(cpu, 0x3E, (byte)i, 0x3C);

                cpu.RunSingle(); //JMP @ 0x100
                cpu.RunSingle(); //LD A, i
                cpu.RunSingle(); //INC A

                string testName = string.Format("INC 0x{0:X} ", i);

                byte result = (byte)(i + 1);
                bool shouldHalfCarry = (i & 0x0F) == 0x0F;

                Assert.AreEqual(result, cpu.state.A, testName);

                Assert.AreEqual(result == 0, cpu.state.Zero, testName + "Zero flag incorrect.");
                Assert.AreEqual(shouldHalfCarry, cpu.state.HalCar, testName + "CPU halfCarry flag incorrect.");
                Assert.IsFalse(cpu.state.Op, testName + "CPU Op flag incorrect.");
            }
        }

        [TestMethod]
        public void DecFlags()
        {
            var cpu = GetCPU();

            for (int i = 0; i < byte.MaxValue; i += 1)
            {
                SetupCPU(cpu, 0x3E, (byte)i, 0x3D);

                cpu.RunSingle(); //JMP @ 0x100
                cpu.RunSingle(); //LD A, i
                cpu.RunSingle(); //DEC A

                string testName = string.Format("DEC 0x{0:X} ", i);

                byte result = (byte)(i - 1);

                bool shouldHalfCarry = (i & 0x0F) == 0x00;

                Assert.AreEqual(result, cpu.state.A, testName);

                Assert.AreEqual(result == 0, cpu.state.Zero, testName + "Zero flag incorrect.");
                Assert.AreEqual(!shouldHalfCarry, cpu.state.HalCar, testName + "CPU halfCarry flag incorrect.");
                Assert.IsTrue(cpu.state.Op, testName + "CPU Op flag incorrect.");
            }
        }
    }
}