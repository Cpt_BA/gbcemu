﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBCZ80.Test
{
    [TestClass]
    public class ControlFlowTest : CPUTestBase
    {
        [TestMethod]
        public void Jump()
        {
            //Just use the jmp at start of program
            var cpu = GetCPU();

            SetupCPU(cpu);

            Assert.AreEqual(0x100, cpu.state.PC);
            cpu.RunSingle();
            Assert.AreEqual(0x150, cpu.state.PC);
        }

        [TestMethod]
        public void JumpRelative()
        {
            //Just use the jmp at start of program
            var cpu = GetCPU();

            SetupCPU(cpu, 0x18, 0x20);

            Assert.AreEqual(0x100, cpu.state.PC);
            cpu.RunSingle(); //Entry point jump
            Assert.AreEqual(0x150, cpu.state.PC);
            cpu.RunSingle(); //JR 0x20
            Assert.AreEqual(0x172, cpu.state.PC);
        }
    }
}
