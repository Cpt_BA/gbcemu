﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis;

namespace GBCRoslynDecoder
{
    using SF = SyntaxFactory;
    using GBCZ80.Decoding;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Reflection;
    using System.IO;

    class Program
    {
        static BlockSyntax param_8BitParsed;
        static BlockSyntax param_16BitParsed;
        static SyntaxList<StatementSyntax> setupCBOpcodeParsed;

        static void Main(string[] args)
        {

            string targetClassName = "Decoder";
            string executionFunctionName = "Execute";

            var validSetupFunctions = new List<string>() { "Setup", "RegisterCB" };
            var registrationFunctions = new List<string>() { "RegisterOpcode", "RegisterCBOpcode" };
            
            var param_8bit = @"
satic void test(){
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC++) & 0xFF;
}";
            var param_16bit = @"
satic void test(){
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC) & 0xFFFF;
                    cpu.state.PC += 2;
}";

            var setupCBOpcode = @"static void test(){int opCode2 = cpu.ram.ReadByte(cpu.state.PC++);}";

            var opcodeInvocationNodes = new List<InvocationItem>();

            param_8BitParsed = SF.Block((CSharpSyntaxTree.ParseText(param_8bit).GetRoot().ChildNodes().Last() as BaseMethodDeclarationSyntax).Body.Statements);
            param_16BitParsed = SF.Block((CSharpSyntaxTree.ParseText(param_16bit).GetRoot().ChildNodes().Last() as BaseMethodDeclarationSyntax).Body.Statements);
            setupCBOpcodeParsed = (CSharpSyntaxTree.ParseText(setupCBOpcode).GetRoot().ChildNodes().Last() as BaseMethodDeclarationSyntax).Body.Statements;
            
            var files = new List<SyntaxTree>()
            {
                CSharpSyntaxTree.ParseText(File.ReadAllText(@"..\..\..\..\..\GBCZ80\Decoding\Decoder.cs")),
                CSharpSyntaxTree.ParseText(File.ReadAllText(@"..\..\..\..\..\GBCZ80\Decoding\DecoderCB.cs"))
            };


            //select all classes, in all files, of any namespace
            var allClassDefinitions = files.SelectMany((st) =>
                                        st.GetRoot()
                                        .ChildNodes()
                                        .OfType<NamespaceDeclarationSyntax>()
                                        .SelectMany((@namespace) => @namespace.ChildNodes().OfType<ClassDeclarationSyntax>()));

            Decoder.Setup();

            foreach (var @class in allClassDefinitions)
            {
                //find the setup function that declares all of the opcodes
                var setupFunctions = @class.Members
                                        .OfType<MethodDeclarationSyntax>()
                                        .Where(mthd => validSetupFunctions.Contains(mthd.Identifier.Text));

                var setupInvocations = setupFunctions.SelectMany((func) =>
                                        func.Body.ChildNodes()
                                            .SelectMany(exp => exp.ChildNodes())
                                            .OfType<InvocationExpressionSyntax>());



                //find all registered functions
                foreach (var funcCall in setupInvocations)
                {
                    var identifier = funcCall.ChildNodes().OfType<IdentifierNameSyntax>().FirstOrDefault();
                    if (identifier != null && registrationFunctions.Contains(identifier.Identifier.Text))
                    {

                        //"DEC D", DEC_D, new byte[] { 4, 4, 4, 4, 4, 4, 12, 4 }, "00", OpcodeTypes.Destination, "101"     
                        var arguments = funcCall.ArgumentList.Arguments;

                        var item = new InvocationItem()
                        {
                            Name = arguments[0].ChildNodes().OfType<LiteralExpressionSyntax>().First().Token.ValueText,
                            Node = arguments[1].ChildNodes().First()
                        };
                        opcodeInvocationNodes.Add(item);
                    }
                }
            }


            var MainOpcodes = GenerateOpcodeFunctionMap(Decoder.Opcodes, opcodeInvocationNodes, allClassDefinitions);
            var CBOpcodes = GenerateOpcodeFunctionMap(Decoder.CBOpcodes, opcodeInvocationNodes, allClassDefinitions);

            MainOpcodes[0xCB] = SF.Block(
                    setupCBOpcodeParsed.Add(
                        GenerateSwitchStatement(CBOpcodes, Decoder.CBOpcodes, SF.IdentifierName("opCode2"))
                    )
                );

            var MainSwitch = GenerateSwitchStatement(MainOpcodes, Decoder.Opcodes, SF.IdentifierName("opCode"));

            var code = MainSwitch.ToFullString();

            var finalCode = @"using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.CompilerServices;

namespace GBCZ80.Decoding
{
    public partial class Decoder
    {
        private static int ExecuteSwitch(byte opCode)
        {" + code + "}}}";

            File.WriteAllText("../../../../../GBCZ80/Decoding/DecoderGenerated.cs", finalCode);

            
        }

        public static IDictionary<int, BlockSyntax> GenerateOpcodeFunctionMap(Opcode[] opcodes, List<InvocationItem> opcodeInvocationNodes, IEnumerable<ClassDeclarationSyntax> allClassDefinitions)
        {
            Dictionary<int, BlockSyntax> registeredOpcodes = new Dictionary<int, BlockSyntax>();

            //find the definition of all these registered functions
            //TODO this needs tochange,some registrations are lambdas, this doesnot take thatinto acount

            foreach (var opcode in opcodes)
            {
                if (opcode != null)
                {
                    InvocationItem item = opcodeInvocationNodes.FirstOrDefault(oin => oin.Name == opcode.name);
                    if (item != null)
                    {
                        SyntaxNode node = item.Node;
                        if (node is IdentifierNameSyntax)
                        {
                            //var funcDef = GetFunctionByName(allClassDefinitions, (node as IdentifierNameSyntax).Identifier.ValueText);
                            //registeredOpcodes.Add(opcode.opcodeNum, funcDef.Body);

                            ArgumentSyntax paramVal;
                            if (opcode.opcodeParam == Opcode.Params._NONE)
                                paramVal = SF.Argument(
                                    SF.LiteralExpression(
                                        SyntaxKind.NumericLiteralExpression,
                                        SF.Literal(0).WithLeadingTrivia(SF.Whitespace(" ")).WithTrailingTrivia(SF.Whitespace(" "))));
                            else
                                paramVal = SF.Argument(SF.IdentifierName("paramValue"));


                            SeparatedSyntaxList<ArgumentSyntax> args = new SeparatedSyntaxList<ArgumentSyntax>();

                            args = args.Add(paramVal);

                            foreach (var arg in opcode.delegateParams)
                            {
                                Type t = arg.GetType();

                                var leftSide = SF.MemberAccessExpression(
                                    SyntaxKind.SimpleMemberAccessExpression,
                                    SF.IdentifierName(t.Namespace),
                                    SF.IdentifierName("Opcode." + t.Name));

                                args = args.Add(
                                    SF.Argument(SF.MemberAccessExpression(SyntaxKind.SimpleMemberAccessExpression,
                                        leftSide,
                                        SF.IdentifierName(arg.ToString())
                                    )));
                            }

                            string funcName = (node as IdentifierNameSyntax).Identifier.ValueText;
                            var callerExpression = SF.ExpressionStatement(
                                SF.InvocationExpression(
                                    SF.IdentifierName(funcName),
                                    SF.ArgumentList(args)));

                            registeredOpcodes.Add(opcode.opcodeNum, SF.Block(callerExpression));
                        }
                        else if (node is ParenthesizedLambdaExpressionSyntax)
                        {
                            var lambda = (node as ParenthesizedLambdaExpressionSyntax);
                            registeredOpcodes.Add(opcode.opcodeNum, lambda.Body as BlockSyntax);
                        }
                        else
                        {

                        }
                    }
                }
            }

            return registeredOpcodes;
        }

        public static SwitchStatementSyntax GenerateSwitchStatement(IDictionary<int, BlockSyntax> opcodeMap, Opcode[] opcodes, ExpressionSyntax switchExpression)
        {
            var switcher = SF.SwitchStatement(switchExpression);

            foreach (var kvp in opcodeMap)
            {
                var opcode = opcodes[kvp.Key];
                BlockSyntax body = SF.Block();

                var @switch = SF.SwitchSection()
                    .AddLabels(
                    SF.CaseSwitchLabel(
                        SF.Token(SyntaxKind.CaseKeyword),
                        SF.LiteralExpression(SyntaxKind.NumericLiteralExpression, SF.Literal(kvp.Key).WithLeadingTrivia(SF.Whitespace(" ")).WithTrailingTrivia(SF.Whitespace(" "))),
                        SF.Token(SyntaxKind.ColonToken)));

                if (opcode != null)
                {
                    switch (opcode.opcodeParam)
                    {
                        case Opcode.Params._NONE:
                            break;
                        case Opcode.Params._8_BIT:
                            body = body.AddStatements(param_8BitParsed.Statements.ToArray());
                            break;
                        case Opcode.Params._16_BIT:
                            body = body.AddStatements(param_16BitParsed.Statements.ToArray());
                            break;
                    }

                    //body = body.AddStatements(FinalizeBlock(opcode, kvp.Value).ToArray());
                    body = body.AddStatements(kvp.Value.Statements.ToArray());

                    /*body = body.AddStatements(
                        SF.ExpressionStatement(
                            SF.AssignmentExpression(
                                SyntaxKind.AddAssignmentExpression,
                                SF.MemberAccessExpression(
                                    SyntaxKind.SimpleMemberAccessExpression,
                                    SF.IdentifierName("cpu"),
                                    SF.Token(SyntaxKind.DotToken),
                                    SF.IdentifierName("Cycles").WithTrailingTrivia(SF.Whitespace(" "))
                                ),
                                SF.Token(
                                        SyntaxTriviaList.Create(SF.Whitespace(" ")),
                                        SyntaxKind.PlusEqualsToken,
                                        SyntaxTriviaList.Create(SF.Whitespace(" "))
                                    ),
                                SF.LiteralExpression(
                                    SyntaxKind.NumericLiteralExpression,
                                    SF.Literal(opcode.cost)
                                )
                            )
                        )
                    );*/

                    

                    body = body.AddStatements(SF.ReturnStatement(SF.ParenthesizedExpression(SF.LiteralExpression(SyntaxKind.NumericLiteralExpression,SF.Literal(opcode.cost)))));
                }
                else
                {
                    body = kvp.Value;
                }

                @switch = @switch.AddStatements(body);
                switcher = switcher.AddSections(@switch);
            }

            var defaultCase =
                SF.SwitchSection(
                    SF.List<SwitchLabelSyntax>
                    (
                        new SwitchLabelSyntax[]
                        {
                                SF.DefaultSwitchLabel()
                        }
                    ),
                    SF.List<StatementSyntax>
                    (
                       new StatementSyntax[]
                       {
                               SF.ReturnStatement(SF.ParenthesizedExpression(SF.LiteralExpression(SyntaxKind.NumericLiteralExpression,SF.Literal(0))))
                       }
                    ));
            switcher = switcher.AddSections(defaultCase);

            return switcher;
        }


        public static IEnumerable<BaseMethodDeclarationSyntax> GetFunctionByNames(IEnumerable<ClassDeclarationSyntax> classDeclarations, IEnumerable<string> functionNames)
        {
            var methods = classDeclarations.SelectMany(cd => cd.ChildNodes().OfType<MethodDeclarationSyntax>());
            foreach (var method in methods)
            {
                if (functionNames.Contains(method.Identifier.ValueText)) yield return method;
            }
        }

        public static BaseMethodDeclarationSyntax GetFunctionByName(IEnumerable<ClassDeclarationSyntax> classDeclarations, string functionName)
        {
            var methods = classDeclarations.SelectMany(cd => cd.ChildNodes().OfType<MethodDeclarationSyntax>());
            foreach (var method in methods)
            {
                if (functionName == method.Identifier.ValueText) return method;
            }
            return null;
        }

        public static SyntaxList<StatementSyntax> FinalizeBlock(Opcode forOpcode, BlockSyntax originalBlock)
        {
            var nodes = originalBlock.DescendantNodes();
            var rewrite = new CastRewriter(forOpcode);
            return (rewrite.Visit(originalBlock) as BlockSyntax).Statements;
        }
        
    }

    public class CastRewriter : CSharpSyntaxRewriter
    {
        Opcode myOpcode;
        public CastRewriter(Opcode forOpcode)
        {
            myOpcode = forOpcode;
        }

        public override SyntaxNode VisitCastExpression(CastExpressionSyntax node)
        {
            var nodes = node.ChildNodes();
            if(nodes.Any(n => n is ElementAccessExpressionSyntax))
            {
                var paramIndex = int.Parse(nodes.OfType<ElementAccessExpressionSyntax>().First().ArgumentList.Arguments[0].Expression.ToFullString());

                //var castType = Type.GetType("GBCZ80.Decoding.");
                Type castType = null;
                switch (node.Type.ToString())
                {
                    case ("Opcode.RegisterSP"):
                        castType = typeof(Opcode.RegisterSP);
                        break;
                    case ("Opcode.RegisterSingle"):
                        castType = typeof(Opcode.RegisterSingle);
                        break;
                    case ("Opcode.Destination"):
                        castType = typeof(Opcode.Destination);
                        break;
                    case ("Opcode.Direction"):
                        castType = typeof(Opcode.Direction);
                        break;
                    case ("Opcode.Condition"):
                        castType = typeof(Opcode.Condition);
                        break;
                    case ("Opcode.Operation"):
                        castType = typeof(Opcode.Operation);
                        break;
                    case ("Opcode.RegisterAF"):
                        castType = typeof(Opcode.RegisterAF);
                        break;
                    case ("Opcode.ResetLocation"):
                        castType = typeof(Opcode.ResetLocation);
                        break;
                    case ("Opcode.RegisterBit"):
                        castType = typeof(Opcode.RegisterBit);
                        break;
                    default:

                        break;
                }
                var t = Enum.ToObject(castType, (int)(myOpcode.delegateParams[paramIndex]));
                //var t = Convert.ChangeType(,castType);
                
                return SF.QualifiedName(SF.IdentifierName(t.GetType().FullName.Replace('+', '.')), SF.IdentifierName(t.ToString()));
            }
            else
                return base.VisitCastExpression(node);
        }
    }



    public class InvocationItem
    {
        public string Name {get; set; }
        public SyntaxNode Node { get; set; }
    }
}
