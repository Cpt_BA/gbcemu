switch(opCode){case 1 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            SetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.BC, (ushort)paramValue);
        }
break;}case 2 :        {
            cpu.writeByte(GetRegister16BitSing(GBCZ80.Decoding.Opcode.RegisterSingle.BC), cpu.state.A);
break;        }
case 3 :        {
            SetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.BC, (ushort)(GetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.BC) + 1));
break;        }
case 4 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original + 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = (newVal & 0x0F) == 0; //(original & 0x0F) > (newVal & 0x0F);
break;        }
case 5 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original - 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = true;
            cpu.state.HalCar = (newVal & 0x0F) == 0x0F;
break;        }
case 6 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            SetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.B, (byte)paramValue);
        }
break;}case 7 :        {
            byte src = cpu.state.A;
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    break;
            }
            cpu.state.Zero = false;// src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.A = src;
break;        }
case 8 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            cpu.ram.WriteShort(paramValue, (short)cpu.state.SP);
        }
break;}case 9 :        {
            ushort before = cpu.state.HL;
            ushort addr = GetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.BC);
            cpu.state.HL += addr;

            cpu.state.Op = false;
            cpu.state.HalCar = (before & 0x0FFF) > (cpu.state.HL & 0xFFF);
            cpu.state.Carry = (before & 0xFFFF) > (cpu.state.HL & 0xFFFF);
break;        }
case 10 :        {
            cpu.state.A = cpu.ram.ReadByte(GetRegister16BitSing(GBCZ80.Decoding.Opcode.RegisterSingle.BC));
break;        }
case 11 :        {
            SetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.BC, (ushort)(GetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.BC) - 1));
break;        }
case 12 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original + 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = (newVal & 0x0F) == 0; //(original & 0x0F) > (newVal & 0x0F);
break;        }
case 13 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original - 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = true;
            cpu.state.HalCar = (newVal & 0x0F) == 0x0F;
break;        }
case 14 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            SetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.C, (byte)paramValue);
        }
break;}case 15 :        {
            byte src = cpu.state.A;
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    break;
            }
            cpu.state.Zero = false;// src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.A = src;
break;        }
case 17 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            SetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.DE, (ushort)paramValue);
        }
break;}case 18 :        {
            cpu.writeByte(GetRegister16BitSing(GBCZ80.Decoding.Opcode.RegisterSingle.DE), cpu.state.A);
break;        }
case 19 :        {
            SetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.DE, (ushort)(GetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.DE) + 1));
break;        }
case 20 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original + 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = (newVal & 0x0F) == 0; //(original & 0x0F) > (newVal & 0x0F);
break;        }
case 21 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original - 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = true;
            cpu.state.HalCar = (newVal & 0x0F) == 0x0F;
break;        }
case 22 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            SetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.D, (byte)paramValue);
        }
break;}case 23 :        {
            byte src = cpu.state.A;
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    break;
            }
            cpu.state.Zero = false;// src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.A = src;
break;        }
case 24 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            cpu.state.PC = (ushort)(cpu.state.PC + ((sbyte)(paramValue & 0xFF)));
        }
break;}case 25 :        {
            ushort before = cpu.state.HL;
            ushort addr = GetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.DE);
            cpu.state.HL += addr;

            cpu.state.Op = false;
            cpu.state.HalCar = (before & 0x0FFF) > (cpu.state.HL & 0xFFF);
            cpu.state.Carry = (before & 0xFFFF) > (cpu.state.HL & 0xFFFF);
break;        }
case 26 :        {
            cpu.state.A = cpu.ram.ReadByte(GetRegister16BitSing(GBCZ80.Decoding.Opcode.RegisterSingle.DE));
break;        }
case 27 :        {
            SetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.DE, (ushort)(GetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.DE) - 1));
break;        }
case 28 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original + 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = (newVal & 0x0F) == 0; //(original & 0x0F) > (newVal & 0x0F);
break;        }
case 29 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original - 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = true;
            cpu.state.HalCar = (newVal & 0x0F) == 0x0F;
break;        }
case 30 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            SetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.E, (byte)paramValue);
        }
break;}case 31 :        {
            byte src = cpu.state.A;
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    break;
            }
            cpu.state.Zero = false;// src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.A = src;
break;        }
case 32 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.NotZero))
            {
                cpu.state.PC = (ushort)(cpu.state.PC + ((sbyte)(paramValue & 0xFF)));
            }
        }
break;}case 33 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            SetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.HL, (ushort)paramValue);
        }
break;}case 34 :        {
            cpu.writeByte(cpu.state.HL, cpu.state.A);
            cpu.state.HL++;
break;        }
case 35 :        {
            SetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.HL, (ushort)(GetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.HL) + 1));
break;        }
case 36 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original + 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = (newVal & 0x0F) == 0; //(original & 0x0F) > (newVal & 0x0F);
break;        }
case 37 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original - 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = true;
            cpu.state.HalCar = (newVal & 0x0F) == 0x0F;
break;        }
case 38 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            SetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.H, (byte)paramValue);
        }
break;}case 39 :        {
            uint temp = cpu.state.A;
            if (cpu.state.Op)
            {
                if (cpu.state.HalCar)
                    temp = (temp - 6) & 0xFF;
                if (cpu.state.Carry)
                    temp -= 0x60;
            }
            else
            {
                if (cpu.state.HalCar || ((temp & 0x0F) > 9))
                    temp += 6;
                if (cpu.state.Carry || (temp > 0x9F))
                    temp += 0x60;
            }
            if ((temp & 0x100) != 0)
                cpu.state.Carry = true;

            cpu.state.HalCar = false;
            temp &= 0xFF;

            cpu.state.Zero = temp == 0x00;

            cpu.state.A = (byte)temp;
break;        }
case 40 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.Zero))
            {
                cpu.state.PC = (ushort)(cpu.state.PC + ((sbyte)(paramValue & 0xFF)));
            }
        }
break;}case 41 :        {
            ushort before = cpu.state.HL;
            ushort addr = GetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.HL);
            cpu.state.HL += addr;

            cpu.state.Op = false;
            cpu.state.HalCar = (before & 0x0FFF) > (cpu.state.HL & 0xFFF);
            cpu.state.Carry = (before & 0xFFFF) > (cpu.state.HL & 0xFFFF);
break;        }
case 42 :        {
            cpu.state.A = cpu.ram.ReadByte(cpu.state.HL);
            cpu.state.HL++;
break;        }
case 43 :        {
            SetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.HL, (ushort)(GetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.HL) - 1));
break;        }
case 44 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original + 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = (newVal & 0x0F) == 0; //(original & 0x0F) > (newVal & 0x0F);
break;        }
case 45 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original - 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = true;
            cpu.state.HalCar = (newVal & 0x0F) == 0x0F;
break;        }
case 46 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            SetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.L, (byte)paramValue);
        }
break;}case 48 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.NotCarry))
            {
                cpu.state.PC = (ushort)(cpu.state.PC + ((sbyte)(paramValue & 0xFF)));
            }
        }
break;}case 49 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            SetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.SP, (ushort)paramValue);
        }
break;}case 50 :        {
            cpu.writeByte(cpu.state.HL, cpu.state.A);
            cpu.state.HL--;
break;        }
case 51 :        {
            SetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.SP, (ushort)(GetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.SP) + 1));
break;        }
case 52 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original + 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = (newVal & 0x0F) == 0; //(original & 0x0F) > (newVal & 0x0F);
break;        }
case 53 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original - 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = true;
            cpu.state.HalCar = (newVal & 0x0F) == 0x0F;
break;        }
case 54 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            SetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.HL, (byte)paramValue);
        }
break;}case 56 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.Carry))
            {
                cpu.state.PC = (ushort)(cpu.state.PC + ((sbyte)(paramValue & 0xFF)));
            }
        }
break;}case 57 :        {
            ushort before = cpu.state.HL;
            ushort addr = GetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.SP);
            cpu.state.HL += addr;

            cpu.state.Op = false;
            cpu.state.HalCar = (before & 0x0FFF) > (cpu.state.HL & 0xFFF);
            cpu.state.Carry = (before & 0xFFFF) > (cpu.state.HL & 0xFFFF);
break;        }
case 58 :        {
            cpu.state.A = cpu.ram.ReadByte(cpu.state.HL);
            cpu.state.HL--;
break;        }
case 59 :        {
            SetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.SP, (ushort)(GetRegister16BitSP(GBCZ80.Decoding.Opcode.RegisterSP.SP) - 1));
break;        }
case 60 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original + 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = (newVal & 0x0F) == 0; //(original & 0x0F) > (newVal & 0x0F);
break;        }
case 61 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            byte original = GetRegister8Bit(dest);
            byte newVal = (byte)(original - 1);
            SetRegister8Bit(dest, newVal);
            cpu.state.Zero = newVal == 0;
            cpu.state.Op = true;
            cpu.state.HalCar = (newVal & 0x0F) == 0x0F;
break;        }
case 62 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            SetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.A, (byte)paramValue);
        }
break;}case 64 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.B;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 65 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.C;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 66 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.D;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 67 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.E;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 68 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.H;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 69 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.L;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 70 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.HL;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 71 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.A;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 72 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.B;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 73 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.C;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 74 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.D;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 75 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.E;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 76 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.H;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 77 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.L;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 78 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.HL;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 79 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.A;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 80 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.B;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 81 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.C;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 82 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.D;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 83 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.E;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 84 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.H;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 85 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.L;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 86 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.HL;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 87 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.A;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 88 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.B;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 89 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.C;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 90 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.D;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 91 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.E;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 92 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.H;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 93 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.L;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 94 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.HL;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 95 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.A;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 96 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.B;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 97 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.C;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 98 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.D;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 99 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.E;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 100 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.H;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 101 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.L;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 102 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.HL;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 103 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.A;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 104 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.B;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 105 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.C;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 106 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.D;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 107 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.E;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 108 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.H;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 109 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.L;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 110 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.HL;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 111 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.A;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 112 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.B;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 113 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.C;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 114 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.D;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 115 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.E;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 116 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.H;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 117 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.L;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 118 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.HL;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 119 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.A;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 120 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.B;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 121 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.C;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 122 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.D;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 123 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.E;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 124 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.H;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 125 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.L;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 126 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.HL;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 127 :        {
            Opcode.Destination to = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.Destination from = GBCZ80.Decoding.Opcode.Destination.A;
            if (to == Opcode.Destination.HL && from == Opcode.Destination.HL)
            {
                //HALT!
                //Console.WriteLine("-------------HALT---------------");
                cpu.state.Halted = true;
            }
            else
            {
                SetRegister8Bit(to, GetRegister8Bit(from));
            }
break;        }
case 128 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADD, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.B));
break;        }
case 129 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADD, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.C));
break;        }
case 130 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADD, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.D));
break;        }
case 131 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADD, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.E));
break;        }
case 132 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADD, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.H));
break;        }
case 133 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADD, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.L));
break;        }
case 134 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADD, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.HL));
break;        }
case 135 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADD, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.A));
break;        }
case 136 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.B));
break;        }
case 137 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.C));
break;        }
case 138 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.D));
break;        }
case 139 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.E));
break;        }
case 140 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.H));
break;        }
case 141 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.L));
break;        }
case 142 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.HL));
break;        }
case 143 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.A));
break;        }
case 144 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SUB, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.B));
break;        }
case 145 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SUB, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.C));
break;        }
case 146 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SUB, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.D));
break;        }
case 147 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SUB, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.E));
break;        }
case 148 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SUB, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.H));
break;        }
case 149 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SUB, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.L));
break;        }
case 150 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SUB, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.HL));
break;        }
case 151 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SUB, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.A));
break;        }
case 152 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SBC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.B));
break;        }
case 153 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SBC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.C));
break;        }
case 154 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SBC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.D));
break;        }
case 155 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SBC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.E));
break;        }
case 156 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SBC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.H));
break;        }
case 157 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SBC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.L));
break;        }
case 158 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SBC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.HL));
break;        }
case 159 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SBC, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.A));
break;        }
case 160 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.AND, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.B));
break;        }
case 161 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.AND, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.C));
break;        }
case 162 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.AND, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.D));
break;        }
case 163 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.AND, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.E));
break;        }
case 164 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.AND, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.H));
break;        }
case 165 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.AND, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.L));
break;        }
case 166 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.AND, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.HL));
break;        }
case 167 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.AND, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.A));
break;        }
case 168 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.XOR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.B));
break;        }
case 169 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.XOR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.C));
break;        }
case 170 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.XOR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.D));
break;        }
case 171 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.XOR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.E));
break;        }
case 172 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.XOR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.H));
break;        }
case 173 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.XOR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.L));
break;        }
case 174 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.XOR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.HL));
break;        }
case 175 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.XOR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.A));
break;        }
case 176 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.OR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.B));
break;        }
case 177 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.OR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.C));
break;        }
case 178 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.OR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.D));
break;        }
case 179 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.OR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.E));
break;        }
case 180 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.OR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.H));
break;        }
case 181 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.OR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.L));
break;        }
case 182 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.OR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.HL));
break;        }
case 183 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.OR, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.A));
break;        }
case 184 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.CP, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.B));
break;        }
case 185 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.CP, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.C));
break;        }
case 186 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.CP, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.D));
break;        }
case 187 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.CP, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.E));
break;        }
case 188 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.CP, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.H));
break;        }
case 189 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.CP, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.L));
break;        }
case 190 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.CP, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.HL));
break;        }
case 191 :        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.CP, GetRegister8Bit(GBCZ80.Decoding.Opcode.Destination.A));
break;        }
case 192 :        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.NotZero))
            {
                cpu.state.PC = (ushort)cpu.ram.ReadInt16(cpu.state.SP);
                cpu.state.SP += 2;
                //Console.WriteLine("Returning to 0x{0:X}", cpu.state.PC);
            }
break;        }
case 193 :        {
            SetRegister16BitAF(GBCZ80.Decoding.Opcode.RegisterAF.BC, (ushort)cpu.ram.ReadInt16(cpu.state.SP));
            cpu.state.SP += 2;
break;            //Console.WriteLine("Pop");
        }
case 194 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.NotZero))
                cpu.state.PC = (ushort)paramValue;
        }
break;}case 195 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            cpu.state.PC = (ushort)paramValue;
        }
break;}case 196 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.NotZero))
            {
                cpu.PushStackShort(cpu.state.PC);
                cpu.state.PC = (ushort)paramValue;
                //Console.WriteLine("Call Conditional 0x{0:X}", paramValue);
            }
        }
break;}case 197 :        {
            ushort value = GetRegister16BitAF(GBCZ80.Decoding.Opcode.RegisterAF.BC);
            cpu.PushStackShort(value);
break;            //Console.WriteLine("Push");
        }
case 198 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADD, (byte)paramValue);
        }
break;}case 199 :        {
            cpu.PushStackShort(cpu.state.PC);
            switch (GBCZ80.Decoding.Opcode.ResetLocation.H00)
            {
                case Opcode.ResetLocation.H00: cpu.state.PC = 0; break;
                case Opcode.ResetLocation.H08: cpu.state.PC = 0x08; break;
                case Opcode.ResetLocation.H10: cpu.state.PC = 0x10; break;
                case Opcode.ResetLocation.H18: cpu.state.PC = 0x18; break;
                case Opcode.ResetLocation.H20: cpu.state.PC = 0x20; break;
                case Opcode.ResetLocation.H28: cpu.state.PC = 0x28; break;
                case Opcode.ResetLocation.H30: cpu.state.PC = 0x30; break;
                case Opcode.ResetLocation.H38: cpu.state.PC = 0x38; break;
            }
break;            //Console.WriteLine("Reset");
        }
case 200 :        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.Zero))
            {
                cpu.state.PC = (ushort)cpu.ram.ReadInt16(cpu.state.SP);
                cpu.state.SP += 2;
                //Console.WriteLine("Returning to 0x{0:X}", cpu.state.PC);
            }
break;        }
case 201 :        {
            cpu.state.PC = (ushort)cpu.ram.ReadInt16(cpu.state.SP);
            cpu.state.SP += 2;
            //Console.WriteLine("Returning to 0x{0:X}", cpu.state.PC);
            if (cpu.state.PC == 0)
            {

            }
break;        }
case 202 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.Zero))
                cpu.state.PC = (ushort)paramValue;
        }
break;}case 204 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.Zero))
            {
                cpu.PushStackShort(cpu.state.PC);
                cpu.state.PC = (ushort)paramValue;
                //Console.WriteLine("Call Conditional 0x{0:X}", paramValue);
            }
        }
break;}case 205 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            cpu.PushStackShort(cpu.state.PC);
            cpu.state.PC = (ushort)paramValue;
            //Console.WriteLine("Call 0x{0:X}", paramValue);
        }
break;}case 206 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.ADC, (byte)paramValue);
        }
break;}case 207 :        {
            cpu.PushStackShort(cpu.state.PC);
            switch (GBCZ80.Decoding.Opcode.ResetLocation.H08)
            {
                case Opcode.ResetLocation.H00: cpu.state.PC = 0; break;
                case Opcode.ResetLocation.H08: cpu.state.PC = 0x08; break;
                case Opcode.ResetLocation.H10: cpu.state.PC = 0x10; break;
                case Opcode.ResetLocation.H18: cpu.state.PC = 0x18; break;
                case Opcode.ResetLocation.H20: cpu.state.PC = 0x20; break;
                case Opcode.ResetLocation.H28: cpu.state.PC = 0x28; break;
                case Opcode.ResetLocation.H30: cpu.state.PC = 0x30; break;
                case Opcode.ResetLocation.H38: cpu.state.PC = 0x38; break;
            }
break;            //Console.WriteLine("Reset");
        }
case 208 :        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.NotCarry))
            {
                cpu.state.PC = (ushort)cpu.ram.ReadInt16(cpu.state.SP);
                cpu.state.SP += 2;
                //Console.WriteLine("Returning to 0x{0:X}", cpu.state.PC);
            }
break;        }
case 209 :        {
            SetRegister16BitAF(GBCZ80.Decoding.Opcode.RegisterAF.DE, (ushort)cpu.ram.ReadInt16(cpu.state.SP));
            cpu.state.SP += 2;
break;            //Console.WriteLine("Pop");
        }
case 210 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.NotCarry))
                cpu.state.PC = (ushort)paramValue;
        }
break;}case 212 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.NotCarry))
            {
                cpu.PushStackShort(cpu.state.PC);
                cpu.state.PC = (ushort)paramValue;
                //Console.WriteLine("Call Conditional 0x{0:X}", paramValue);
            }
        }
break;}case 213 :        {
            ushort value = GetRegister16BitAF(GBCZ80.Decoding.Opcode.RegisterAF.DE);
            cpu.PushStackShort(value);
break;            //Console.WriteLine("Push");
        }
case 214 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SUB, (byte)paramValue);
        }
break;}case 215 :        {
            cpu.PushStackShort(cpu.state.PC);
            switch (GBCZ80.Decoding.Opcode.ResetLocation.H10)
            {
                case Opcode.ResetLocation.H00: cpu.state.PC = 0; break;
                case Opcode.ResetLocation.H08: cpu.state.PC = 0x08; break;
                case Opcode.ResetLocation.H10: cpu.state.PC = 0x10; break;
                case Opcode.ResetLocation.H18: cpu.state.PC = 0x18; break;
                case Opcode.ResetLocation.H20: cpu.state.PC = 0x20; break;
                case Opcode.ResetLocation.H28: cpu.state.PC = 0x28; break;
                case Opcode.ResetLocation.H30: cpu.state.PC = 0x30; break;
                case Opcode.ResetLocation.H38: cpu.state.PC = 0x38; break;
            }
break;            //Console.WriteLine("Reset");
        }
case 216 :        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.Carry))
            {
                cpu.state.PC = (ushort)cpu.ram.ReadInt16(cpu.state.SP);
                cpu.state.SP += 2;
                //Console.WriteLine("Returning to 0x{0:X}", cpu.state.PC);
            }
break;        }
case 218 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.Carry))
                cpu.state.PC = (ushort)paramValue;
        }
break;}case 220 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            if (GetFlag(GBCZ80.Decoding.Opcode.Condition.Carry))
            {
                cpu.PushStackShort(cpu.state.PC);
                cpu.state.PC = (ushort)paramValue;
                //Console.WriteLine("Call Conditional 0x{0:X}", paramValue);
            }
        }
break;}case 222 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.SBC, (byte)paramValue);
        }
break;}case 223 :        {
            cpu.PushStackShort(cpu.state.PC);
            switch (GBCZ80.Decoding.Opcode.ResetLocation.H18)
            {
                case Opcode.ResetLocation.H00: cpu.state.PC = 0; break;
                case Opcode.ResetLocation.H08: cpu.state.PC = 0x08; break;
                case Opcode.ResetLocation.H10: cpu.state.PC = 0x10; break;
                case Opcode.ResetLocation.H18: cpu.state.PC = 0x18; break;
                case Opcode.ResetLocation.H20: cpu.state.PC = 0x20; break;
                case Opcode.ResetLocation.H28: cpu.state.PC = 0x28; break;
                case Opcode.ResetLocation.H30: cpu.state.PC = 0x30; break;
                case Opcode.ResetLocation.H38: cpu.state.PC = 0x38; break;
            }
break;            //Console.WriteLine("Reset");
        }
case 224 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            cpu.writeByte(0xFF00 + paramValue, cpu.state.A);
        }
break;}case 225 :        {
            SetRegister16BitAF(GBCZ80.Decoding.Opcode.RegisterAF.HL, (ushort)cpu.ram.ReadInt16(cpu.state.SP));
            cpu.state.SP += 2;
break;            //Console.WriteLine("Pop");
        }
case 226 :        {
            cpu.writeByte(0xFF00 + cpu.state.C, cpu.state.A);
break;        }
case 229 :        {
            ushort value = GetRegister16BitAF(GBCZ80.Decoding.Opcode.RegisterAF.HL);
            cpu.PushStackShort(value);
break;            //Console.WriteLine("Push");
        }
case 230 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.AND, (byte)paramValue);
        }
break;}case 231 :        {
            cpu.PushStackShort(cpu.state.PC);
            switch (GBCZ80.Decoding.Opcode.ResetLocation.H20)
            {
                case Opcode.ResetLocation.H00: cpu.state.PC = 0; break;
                case Opcode.ResetLocation.H08: cpu.state.PC = 0x08; break;
                case Opcode.ResetLocation.H10: cpu.state.PC = 0x10; break;
                case Opcode.ResetLocation.H18: cpu.state.PC = 0x18; break;
                case Opcode.ResetLocation.H20: cpu.state.PC = 0x20; break;
                case Opcode.ResetLocation.H28: cpu.state.PC = 0x28; break;
                case Opcode.ResetLocation.H30: cpu.state.PC = 0x30; break;
                case Opcode.ResetLocation.H38: cpu.state.PC = 0x38; break;
            }
break;            //Console.WriteLine("Reset");
        }
case 232 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            short offset = (short)((sbyte)paramValue);
            cpu.state.Carry = (0xFF - (cpu.state.SP & 0x00FF) < offset);
            cpu.state.HalCar = (0x0F - (cpu.state.SP & 0x000F) < (offset & 0x0f));
            cpu.state.SP = (ushort)(cpu.state.SP + offset);
            cpu.state.Zero = false;
            cpu.state.Op = false;
            //Console.WriteLine("SP Moved");
        }
break;}case 233 :        {
            cpu.state.PC = cpu.state.HL;
break;        }
case 234 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            cpu.writeByte(paramValue, cpu.state.A);
        }
break;}case 238 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.XOR, (byte)paramValue);
        }
break;}case 239 :        {
            cpu.PushStackShort(cpu.state.PC);
            switch (GBCZ80.Decoding.Opcode.ResetLocation.H28)
            {
                case Opcode.ResetLocation.H00: cpu.state.PC = 0; break;
                case Opcode.ResetLocation.H08: cpu.state.PC = 0x08; break;
                case Opcode.ResetLocation.H10: cpu.state.PC = 0x10; break;
                case Opcode.ResetLocation.H18: cpu.state.PC = 0x18; break;
                case Opcode.ResetLocation.H20: cpu.state.PC = 0x20; break;
                case Opcode.ResetLocation.H28: cpu.state.PC = 0x28; break;
                case Opcode.ResetLocation.H30: cpu.state.PC = 0x30; break;
                case Opcode.ResetLocation.H38: cpu.state.PC = 0x38; break;
            }
break;            //Console.WriteLine("Reset");
        }
case 240 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            cpu.state.A = cpu.ram.ReadByte(0xFF00 + paramValue);
        }
break;}case 241 :        {
            SetRegister16BitAF(GBCZ80.Decoding.Opcode.RegisterAF.AF, (ushort)cpu.ram.ReadInt16(cpu.state.SP));
            cpu.state.SP += 2;
break;            //Console.WriteLine("Pop");
        }
case 242 :        {
            cpu.state.A = cpu.ram.ReadByte(0xFF00 + cpu.state.C);
break;        }
case 243 :        {
            cpu.state.InterupEnabled = false;
break;        }
case 245 :        {
            ushort value = GetRegister16BitAF(GBCZ80.Decoding.Opcode.RegisterAF.AF);
            cpu.PushStackShort(value);
break;            //Console.WriteLine("Push");
        }
case 246 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.OR, (byte)paramValue);
        }
break;}case 247 :        {
            cpu.PushStackShort(cpu.state.PC);
            switch (GBCZ80.Decoding.Opcode.ResetLocation.H30)
            {
                case Opcode.ResetLocation.H00: cpu.state.PC = 0; break;
                case Opcode.ResetLocation.H08: cpu.state.PC = 0x08; break;
                case Opcode.ResetLocation.H10: cpu.state.PC = 0x10; break;
                case Opcode.ResetLocation.H18: cpu.state.PC = 0x18; break;
                case Opcode.ResetLocation.H20: cpu.state.PC = 0x20; break;
                case Opcode.ResetLocation.H28: cpu.state.PC = 0x28; break;
                case Opcode.ResetLocation.H30: cpu.state.PC = 0x30; break;
                case Opcode.ResetLocation.H38: cpu.state.PC = 0x38; break;
            }
break;            //Console.WriteLine("Reset");
        }
case 248 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            short offset = (short)((sbyte)paramValue);
            cpu.state.Carry = (0xFF - (cpu.state.SP & 0x00FF) < offset);
            cpu.state.HalCar = (0x0F - (cpu.state.SP & 0x000F) < (offset & 0x0f));
            cpu.state.HL = (ushort)(cpu.state.SP + offset);
            cpu.state.Zero = false;
            cpu.state.Op = false;
        }
break;}case 249 :        {
            cpu.state.SP = cpu.state.HL;
break;            //Console.WriteLine("SP <- HL");
        }
case 250 :{
                    cpu.state.PC += 2;
                    int paramValue = cpu.ram.ReadInt16(cpu.state.PC - 2) & 0xFFFF;
        {
            cpu.state.A = cpu.ram.ReadByte(paramValue);
        }
break;}case 251 :        {
            cpu.state.InterupEnabled = true;
            cpu.ram.WriteByte(GBRAM.IORegisters.IF, 0);
break;        }
case 254 :{
                    cpu.state.PC++;
                    int paramValue = cpu.ram.ReadByte(cpu.state.PC - 1) & 0xFF;
        {
            cpu.state.A = ALU8Bit(GBCZ80.Decoding.Opcode.Operation.CP, (byte)paramValue);
        }
break;}case 255 :        {
            cpu.PushStackShort(cpu.state.PC);
            switch (GBCZ80.Decoding.Opcode.ResetLocation.H38)
            {
                case Opcode.ResetLocation.H00: cpu.state.PC = 0; break;
                case Opcode.ResetLocation.H08: cpu.state.PC = 0x08; break;
                case Opcode.ResetLocation.H10: cpu.state.PC = 0x10; break;
                case Opcode.ResetLocation.H18: cpu.state.PC = 0x18; break;
                case Opcode.ResetLocation.H20: cpu.state.PC = 0x20; break;
                case Opcode.ResetLocation.H28: cpu.state.PC = 0x28; break;
                case Opcode.ResetLocation.H30: cpu.state.PC = 0x30; break;
                case Opcode.ResetLocation.H38: cpu.state.PC = 0x38; break;
            }
break;            //Console.WriteLine("Reset");
        }
case 203 :{switch(opCode2){case 0 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 1 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 2 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 3 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 4 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 5 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 6 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 7 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 8 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 9 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 10 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 11 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 12 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 13 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 14 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 15 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(cpu.state.Carry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(cpu.state.Carry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 16 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 17 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 18 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 19 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 20 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 21 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 22 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 23 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 24 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 25 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 26 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 27 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 28 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 29 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 30 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 31 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            byte src = GetRegister8Bit(dest);
            bool oldCarry = cpu.state.Carry;
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    src |= (byte)(oldCarry ? 0x01 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    src >>= 1;
                    src |= (byte)(oldCarry ? 0x80 : 0x00);
                    SetRegister8Bit(dest, src);
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
break;        }
case 32 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 33 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 34 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 35 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 36 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 37 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 38 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 39 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.LEFT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 40 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 41 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 42 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 43 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 44 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 45 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 46 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 47 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            byte src = GetRegister8Bit(dest);
            switch (GBCZ80.Decoding.Opcode.Direction.RIGHT)
            {
                case Opcode.Direction.LEFT:
                    cpu.state.Carry = (src & 0x80) != 0;
                    src <<= 1;
                    break;
                case Opcode.Direction.RIGHT:
                    cpu.state.Carry = (src & 0x01) != 0;
                    byte msb = (byte)(src & 0x80);
                    src >>= 1;
                    src |= msb;
                    break;
            }
            cpu.state.Zero = src == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            SetRegister8Bit(dest, src);
break;        }
case 48 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            byte val = GetRegister8Bit(dest);
            val = (byte)(((val & 0xF0) >> 4) | ((val & 0x0F) << 4));

            cpu.state.Zero = val == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = false;
            
            SetRegister8Bit(dest, val);
break;        }
case 49 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            byte val = GetRegister8Bit(dest);
            val = (byte)(((val & 0xF0) >> 4) | ((val & 0x0F) << 4));

            cpu.state.Zero = val == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = false;
            
            SetRegister8Bit(dest, val);
break;        }
case 50 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            byte val = GetRegister8Bit(dest);
            val = (byte)(((val & 0xF0) >> 4) | ((val & 0x0F) << 4));

            cpu.state.Zero = val == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = false;
            
            SetRegister8Bit(dest, val);
break;        }
case 51 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            byte val = GetRegister8Bit(dest);
            val = (byte)(((val & 0xF0) >> 4) | ((val & 0x0F) << 4));

            cpu.state.Zero = val == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = false;
            
            SetRegister8Bit(dest, val);
break;        }
case 52 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            byte val = GetRegister8Bit(dest);
            val = (byte)(((val & 0xF0) >> 4) | ((val & 0x0F) << 4));

            cpu.state.Zero = val == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = false;
            
            SetRegister8Bit(dest, val);
break;        }
case 53 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            byte val = GetRegister8Bit(dest);
            val = (byte)(((val & 0xF0) >> 4) | ((val & 0x0F) << 4));

            cpu.state.Zero = val == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = false;
            
            SetRegister8Bit(dest, val);
break;        }
case 54 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            byte val = GetRegister8Bit(dest);
            val = (byte)(((val & 0xF0) >> 4) | ((val & 0x0F) << 4));

            cpu.state.Zero = val == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = false;
            
            SetRegister8Bit(dest, val);
break;        }
case 55 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            byte val = GetRegister8Bit(dest);
            val = (byte)(((val & 0xF0) >> 4) | ((val & 0x0F) << 4));

            cpu.state.Zero = val == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = false;
            
            SetRegister8Bit(dest, val);
break;        }
case 56 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            byte val = GetRegister8Bit(dest);
            cpu.state.Zero = (val >> 1) == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = (val & 0x01) == 1;
            val >>= 1;
            SetRegister8Bit(dest, val);
break;        }
case 57 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            byte val = GetRegister8Bit(dest);
            cpu.state.Zero = (val >> 1) == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = (val & 0x01) == 1;
            val >>= 1;
            SetRegister8Bit(dest, val);
break;        }
case 58 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            byte val = GetRegister8Bit(dest);
            cpu.state.Zero = (val >> 1) == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = (val & 0x01) == 1;
            val >>= 1;
            SetRegister8Bit(dest, val);
break;        }
case 59 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            byte val = GetRegister8Bit(dest);
            cpu.state.Zero = (val >> 1) == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = (val & 0x01) == 1;
            val >>= 1;
            SetRegister8Bit(dest, val);
break;        }
case 60 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            byte val = GetRegister8Bit(dest);
            cpu.state.Zero = (val >> 1) == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = (val & 0x01) == 1;
            val >>= 1;
            SetRegister8Bit(dest, val);
break;        }
case 61 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            byte val = GetRegister8Bit(dest);
            cpu.state.Zero = (val >> 1) == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = (val & 0x01) == 1;
            val >>= 1;
            SetRegister8Bit(dest, val);
break;        }
case 62 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            byte val = GetRegister8Bit(dest);
            cpu.state.Zero = (val >> 1) == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = (val & 0x01) == 1;
            val >>= 1;
            SetRegister8Bit(dest, val);
break;        }
case 63 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            byte val = GetRegister8Bit(dest);
            cpu.state.Zero = (val >> 1) == 0;
            cpu.state.Op = false;
            cpu.state.HalCar = false;
            cpu.state.Carry = (val & 0x01) == 1;
            val >>= 1;
            SetRegister8Bit(dest, val);
break;        }
case 64 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 65 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 66 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 67 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 68 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 69 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 70 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 71 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 72 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 73 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 74 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 75 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 76 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 77 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 78 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 79 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 80 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 81 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 82 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 83 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 84 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 85 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 86 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 87 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 88 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 89 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 90 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 91 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 92 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 93 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 94 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 95 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 96 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 97 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 98 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 99 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 100 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 101 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 102 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 103 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 104 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 105 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 106 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 107 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 108 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 109 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 110 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 111 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 112 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 113 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 114 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 115 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 116 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 117 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 118 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 119 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 120 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 121 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 122 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 123 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 124 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 125 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 126 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 127 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            cpu.state.Op = false;
            cpu.state.HalCar = true;
            cpu.state.Zero = (val & (1 << (int)bit)) == 0;
break;        }
case 128 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 129 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 130 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 131 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 132 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 133 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 134 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 135 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 136 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 137 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 138 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 139 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 140 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 141 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 142 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 143 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 144 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 145 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 146 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 147 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 148 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 149 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 150 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 151 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 152 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 153 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 154 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 155 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 156 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 157 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 158 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 159 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 160 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 161 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 162 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 163 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 164 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 165 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 166 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 167 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 168 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 169 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 170 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 171 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 172 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 173 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 174 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 175 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 176 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 177 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 178 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 179 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 180 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 181 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 182 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 183 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 184 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 185 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 186 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 187 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 188 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 189 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 190 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 191 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val & (0xFF ^ (1 << ((int)bit))));
            SetRegister8Bit(dest, val);
break;        }
case 192 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 193 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 194 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 195 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 196 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 197 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 198 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 199 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_0;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 200 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 201 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 202 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 203 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 204 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 205 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 206 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 207 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_1;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 208 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 209 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 210 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 211 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 212 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 213 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 214 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 215 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_2;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 216 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 217 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 218 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 219 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 220 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 221 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 222 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 223 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_3;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 224 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 225 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 226 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 227 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 228 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 229 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 230 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 231 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_4;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 232 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 233 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 234 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 235 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 236 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 237 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 238 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 239 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_5;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 240 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 241 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 242 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 243 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 244 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 245 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 246 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 247 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_6;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 248 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.B;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 249 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.C;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 250 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.D;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 251 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.E;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 252 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.H;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 253 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.L;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 254 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.HL;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
case 255 :        {
            Opcode.Destination dest = GBCZ80.Decoding.Opcode.Destination.A;
            Opcode.RegisterBit bit = GBCZ80.Decoding.Opcode.RegisterBit.BIT_7;
            byte val = GetRegister8Bit(dest);
            val = (byte)(val | (1 << ((int)bit)));
            SetRegister8Bit(dest, val);
break;        }
}break;}}